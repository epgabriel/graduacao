#include <iostream>
using namespace std;
bool trocar(float&,float&);
int main() {
	int i,j,n;
	cin>>n;//n recebe a quantidade de linhas/colunas para declara��o da matriz que ser� usada
	float mat[n][n];
	bool devil;//devil, o Acusador.
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			cin>>mat[i][j];
		}
	}
	//ordena triangulo norte(v)
	do{
		devil=false;
		for(i=0;i<n/2;i++){
			for(j=i+1;j<n-2-i;j++){
				if(mat[i][j]>mat[i][j+1]){
					devil=trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}
		}
		for(i=0;i<(n/2)-1;i++){
			if(mat[i][(n-2-i)]>mat[i+1][i+2]){
				devil=trocar(mat[i][(n-2-i)],mat[i+1][i+2]);//ordena��o e atribui��o � devil para que haja repeti��o
			}
		}
	}while(devil);
	//ordena triangulo oeste(>)
	do{
		devil=false;	
		for(i=1;i<n-1;i++){//n-1 nao entra logo i vai at� n-1-1
			if(i<=n/2){//j cresce at� i=n/2 e regride quando i>n/2
				for(j=0;j<=i-2;j++){
					if(mat[i][j]>mat[i][j+1]){
						devil = trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
					}
				}
			}else{
				for(j=0;j<=(n-3)-i;j++){//n-1 nao entra, n-2 - i � o valor final de j, j tem de ser valor final-1 = n-3-i
					if(mat[i][j]>mat[i][j+1]){
						devil=trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
					}
				}
			}
		}	
		for(i=1;i<n-2;i++){//i vai at� n-2(i<n-1), mas para comparar � preciso que ele v� at� n-3(i<n-2)
			if(i<=n/2){
				if(mat[i][i-1]>mat[i+1][0]){
					devil=trocar(mat[i][i-1],mat[i+1][0]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}else{
				if(mat[i][(n-2)-i]>mat[i+1][0]){			
					devil=trocar(mat[i][(n-2)-i],mat[i+1][0]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}
		}
	}while(devil);
//	ordena triangulo leste(<)
	do{
		devil=false;
		//ordena linhas
		for(i=1;i<=(n/2);i++){
			for(j=n-i;j<n-1;j++){
				if(mat[i][j]>mat[i][j+1]){
					devil=trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}
		}
		for(i=(n/2)+1;i<n-1;i++){
			for(j=i+1;j<n-1;j++){
				if(mat[i][j]>mat[i][j+1]){
					devil=trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}
		}
		//ordena paralelos
		for(i=1;i<n/2;i++){
			if(mat[i][n-1]>mat[i+1][n-(i+1)]){
				devil=trocar(mat[i][n-1],mat[i+1][n-(i+1)]);//ordena��o e atribui��o � devil para que haja repeti��o
			}
		}	
		for(i=n/2;i<n-2;i++){
			if(mat[i][n-1]>mat[i+1][i+2]){
				devil=trocar(mat[i][n-1],mat[i+1][i+2]);//ordena��o e atribui��o � devil para que haja repeti��o
			}
		}
	}while(devil);
//	ordena triangulo sul(^)
	do{
		devil=false;
		for(i=(n/2)+1;i<n;i++){//ordena as linhas
			for(j=n-i;j<i-1;j++){
				if(mat[i][j]>mat[i][j+1]){
					devil=trocar(mat[i][j],mat[i][j+1]);//ordena��o e atribui��o � devil para que haja repeti��o
				}
			}
		}
		for(i=(n/2)+1;i<n-1;i++){//ordena paralelos
			if(mat[i][i-1]>mat[i+1][n-(i+1)]){
				devil=trocar(mat[i][i-1],mat[i+1][n-(i+1)]);//ordena��o e atribui��o � devil para que haja repeti��o
			}
		}
	}while(devil);
	//imprime diagonal primaria
	for(i=0;i<n;i++){
		cout<<mat[i][i]<<"\t";
	}cout<<endl;
	//imprime diagonal secundaria
	for(i=n-1;i>=0;i--){
		cout<<mat[i][n-i-1]<<"\t";
	}cout<<endl;
	//imprime matriz substituindo o que se deve substituir
	for(i=0;i<n;i++){cout<<endl;
		for(j=0;j<n;j++){
			if((i==j)||(i+j==(n-1)))cout<<mat[i][j]<<"\t";//se as coordenadas forem de uma diagonal, imprime o valor dela
			else cout<<(char)(64+(mat[i][j]*2))<<"\t";//sen�o, imprime a letra correspondente ao dobro do seu valor
		}
	}
   return 0;
}
bool trocar(float &A,float &B){//func�o que realiza as trocas e devolve true para que devil acuse essa troca
	float C=A;
	A=B;
	B=C;
	return true;
}
