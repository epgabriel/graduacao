#include <iostream>
#include <fstream>
#include <math.h>
using namespace std;

/*Das bibliotecas:
 * fstream para utilização dos arquivos
 * math.h para uso das funções "sqrt" e "pow"*/

//Registro que guardará as coordenadas 'x' e 'y' das cidades.
typedef struct{
	float x,y;
}Loc;//'Loc'alização.

//Protótipos das funções que serão utilizadas no decorrer do código:
void lerCoordenadas(Loc[],Loc[]);
void imprimir(Loc[],float);
void trocar(Loc&,Loc&);
float percurso(Loc,Loc);
void permutar(Loc[],Loc[],int,float&);
//fim dos protótipos.

//Função principal
int main(){
	Loc vet[10],A[10];/*vet é o vetor com as coordenadas que serão sempre manipuladas
						*/
	lerCoordenadas(vet,A);
	float dist=0;
	for(int i=0;i<9;i++){
		dist+=percurso(vet[i],vet[i+1]);
	}
	cout << dist << endl;
	permutar(vet,A,1,dist);
	imprimir(A,dist);
	return 0;
}


void permutar(Loc vet[], Loc A[], int start,float &menorDist){
	if(start==9){
		float dist=0;
		for(int i=0;i<9;i++){
			dist+=percurso(vet[i],vet[i+1]);
		}
		if(dist<menorDist){
			for(int i=1;i<9;i++){
				A[i]=vet[i];
			}
			menorDist=dist;
		}
	}else{
		for(int i=start;i<9;i++){
			trocar(vet[i],vet[start]);
			permutar(vet,A,start+1,menorDist);
			trocar(vet[start],vet[i]);
		}
	}
}
		
float percurso(Loc A,Loc B){
	return sqrt(pow((B.x-A.x),2)+pow((B.y-A.y),2));
}
void trocar(Loc &A, Loc &B){
	Loc C=A;
	A=B;
	B=C;
}
void imprimir(Loc vet[],float dist){
	ofstream w;
	w.open("saida.txt");
	for(int i=0;i<9;i++){
		w<<vet[i].x<<"\t"<<vet[i].y<<endl;
	}w<<dist<<endl;
	for(int i=9;i>0;i--){
		w<<endl<<vet[i].x<<"\t"<<vet[i].y;
	}w<<endl<<dist;
	w.close();
}
void lerCoordenadas(Loc vet[],Loc A[]){
	ifstream r;
	r.open("entrada.txt");
	for(int i=0;i<9;i++){
		r>>vet[i].x>>vet[i].y;
		A[i]=vet[i];
	}
	vet[9]=vet[0];
	A[9]=A[0];
	r.close();
}
