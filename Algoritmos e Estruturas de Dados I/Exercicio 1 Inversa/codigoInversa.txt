#include <iostream>
using namespace std;
void trocar(float&,float&);
int main(){
    int n;
    cin>>n;
    float mat[n][n],ident[n][n],pivo,aux;
    int i,j,k;
    bool devil;//variavel que acusa
    for(i=0;i<n;i++){//le a matriz
        for(j=0;j<n;j++){
            cin>>mat[i][j];
            if(i==j){//preenche a identidade
                ident[i][j]=1;
            }else{
                ident[i][j]=0;
            }
        }
    }
    //organizar a matriz de modo que nenhum pivo possa ser 0
    for(k=0;k<n;k++){
        pivo=mat[k][k];
        if(pivo==0){
            devil=false;
            i=k;
            do{
                i++;
                if(mat[i][k]!=0){
                    devil=true;//devil acusa se foi encontrado uma linha com pivo valido
                }
            }while(!devil&&i<n);//se devil nao achar entao o valor de i tem de ser o requisito de parada
            if(i==n){//se i igual a n entao n�o existe um pivo valido, logo nao existe matriz inversa
                return 0;
            }else{
                for(j=0;j<n;j++){
                    trocar(mat[k][j],mat[i][j]);//ordena matriz
                    trocar(ident[k][j],ident[i][j]);//ordena identidade em funcao da matriz
                }
            }
        }
    }
    //zera os elementos abaixo e acima do pivo principal
    for(i=0;i<n;i++){
        pivo=mat[i][i];
        if(pivo!=1){//se o pivo n�o for 1 entao deve-se dividir a linha por ele
            for(j=0;j<n;j++){
                mat[i][j]/=pivo;
                ident[i][j]/=pivo;
            }
        }
        for(k=i+1;k<n;k++){ //zera os elementos abaixo do pivo principal
            pivo=mat[k][i]*(-1);//variavel pivo aqui � usada como secundaria
            for(j=0;j<n;j++){
                aux=mat[i][j]*pivo;
                mat[k][j]+=aux;
                aux=ident[i][j]*pivo;
                ident[k][j]+=aux;
            }
        }
        if(i>0){//se i maior que 0 entao h� elementos acima do pivo principal
            for(k=i-1;k>=0;k--){//zera os elementos acima do pivo principal
                pivo=mat[k][i]*(-1);//variavel pivo agora trabalha como secundario
                for(j=n-1;j>=0;j--){
                    aux=mat[i][j]*pivo;
                    mat[k][j]+=aux;
                    aux=ident[i][j]*pivo;
                    ident[k][j]+=aux;
                }
            }
        }
    }
    for(i=0;i<n;i++){//imprimi a matriz inversa
        for(j=0;j<n;j++){
            cout<<ident[i][j]<<"\t";
        }cout<<endl;
    }
    return 0;
}
void trocar(float &A,float &B){//funcao de troca de elementos
	float C=A;
	A=B;
	B=C;
}
