#include <iostream>
using namespace std;
void trocar(float&,float&);
int main(){
    int n;
    cin>>n;
    float mat[n][n],ident[n][n],pivo,aux;
    int i,j,k;
    bool devil;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            cin>>mat[i][j];
            if(i==j){
                ident[i][j]=1;
            }else{
                ident[i][j]=0;
            }
        }
    }
    for(k=0;k<n;k++){
        pivo=mat[k][k];
        if(pivo==0){
            devil=false;
            i=k;
            do{
                i++;
                if(mat[i][k]!=0){
                    devil=true;
                }
            }while(!devil&&i<n);
            if(i==n){
                return 0;
            }else{
                for(j=0;j<n;j++){
                    trocar(mat[k][j],mat[i][j]);
                    trocar(ident[k][j],ident[i][j]);
                }
            }
        }
    }
    for(i=0;i<n;i++){
        pivo=mat[i][i];
        if(pivo!=1){
            for(j=0;j<n;j++){
                mat[i][j]/=pivo;
                ident[i][j]/=pivo;
            }
        }
        for(k=i+1;k<n;k++){
            pivo=mat[k][i]*(-1);
            for(j=0;j<n;j++){
                aux=mat[i][j]*pivo;
                mat[k][j]+=aux;
                aux=ident[i][j]*pivo;
                ident[k][j]+=aux;
            }
        }
        if(i>0){
            for(k=i-1;k>=0;k--){
                pivo=mat[k][i]*(-1);
                for(j=n-1;j>=0;j--){
                    aux=mat[i][j]*pivo;
                    mat[k][j]+=aux;
                    aux=ident[i][j]*pivo;
                    ident[k][j]+=aux;
                }
            }
        }
    }
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            cout<<ident[i][j]<<"\t";
        }cout<<endl;
    }
    return 0;
}
void trocar(float &A,float &B){
	float C=A;
	A=B;
	B=C;
}

