import java.util.Scanner; //biblioteca para leitura de dados

public class AnaliseNumeros {
	
    private int qnt;  //quantidade de elementos a serem inseridos
	private int vet[];//vetor onde os valores serão armazenados
	private float media; //variavel para cálculo da média
		
	public AnaliseNumeros(int qnt){ //função chamada na criação do objeto, inicializa os valores
		this.qnt = qnt;
		this.vet = new int[this.qnt];
		media = 0;
	}
	
	void preencherVetor(Scanner e){
	//Aqui o vetor é preenchido, ordenado e a média é calculada
		System.out.println("Digite os "+this.qnt+" elementos: ");
		for(byte i=0; i<qnt; i++){
			vet[i] = e.nextInt();
			media += vet[i];
		}
		
		//vetor preenchido, agora será ordenado.
		boolean a;
		int q = qnt;
		do{
			q--;
			a = false;
			for(byte i=0; i<q; i++){
				if(vet[i] > vet[i+1]){
					int aux = vet[i];
					vet[i] = vet[i+1];
					vet[i+1] = aux;
					a = true;
				}
			}
		}while(a);
		media /= qnt;
	}

	int getMenor(){
		return vet[0];
	}
	
	int getMaior(){
		return vet[qnt-1];
	}
	
	float getMedia(){
		return media;
	}
	
	void imprimirPositivos(){//Funcao que verifica se um numero é positivo e o imprime na tela se assim o for
		System.out.print("\nElemento(s) Positivo(s): ");
		boolean a = false; //booleana que verifica se há pelo menos um elemento positivo nesse arranjo
		int aux = vet[0]; //Variável auxiliar para que não sejam imprimidos valores repetidos
		if(aux > 0){
			System.out.print(aux+" ");
			a = true;
		}
		for(byte i=1; i<qnt; i++){
			if(vet[i] > 0 && vet[i] != aux){
				System.out.print(vet[i]+" ");
				aux = vet[i];
				a = true;
			}
		}
		if(!a) System.out.print("Não foram encontrados elementos positivos neste arranjo!");
	}
	
	void imprimirNegativos(){//Funcao que verifica se um numero é negativo e o imprime na tela se assim o for
		System.out.print("\nElemento(s) Negativo(s): ");
		boolean a = false; //booleana que verifica se há pelo menos um elemento negativo nesse arranjo
		int aux = vet[0]; //Variável auxiliar para que não sejam imprimidos valores repetidos
		if(aux < 0){
			System.out.print(aux+" ");
			a = true;
		}
		for(byte i=1; i<qnt; i++){
			if(vet[i] < 0 && vet[i] != aux){
				System.out.print(vet[i]+" ");
				aux = vet[i];
				a = true;
			}
		}
		if(!a) System.out.print("Não foram encontrados elementos negativos neste arranjo!");
	}
	
	void imprimirPrimos(){ //Funcao que verifica se um numero é primo e o imprime na tela se assim o for
		System.out.print("\nElemento(s) Primo(s): ");
		boolean primos = false; //booleana para saber se há um único primo no arranjo
		int aux = -1; //não há números primos negativos, então...
		for(byte i=0; i<qnt; i++){
			if(vet[i] > 0 && vet[i] != aux){
				boolean a = false; //booleana auxiliar para parar o laço se um numero nao for primo
				byte j = 2;
				while(j < vet[i] && !a){
					if(vet[i]%j == 0){
						a = true;
					}
					j++;
				}
				if(!a && vet[i] > 0){
					primos = true;
					aux = vet[i];
					System.out.print(vet[i]+" ");
				}
			}
		}
		if(!primos) System.out.print("Não foram encontrados elementos primos neste arranjo!");
	}
	
}
