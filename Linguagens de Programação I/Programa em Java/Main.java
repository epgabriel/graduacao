import java.io.IOException;
import java.util.Scanner;
import java.lang.String;

public class Main {
	
	public static void main(String[] args) throws IOException{
		Scanner e = new Scanner(System.in);//VARIÁVEL USADA PARA ENTRADA DE DADOS PELO USUÁRIO
		byte op = 0;//VARIÁVEL USADA PARA SELECIONAR OPÇÕES DO MENU
		boolean repetir = false; //CONTROLE DE REPETICAO PARA O MENU PRINCIPAL
		
		do{
			if(!repetir){
				op = lerOpcao(e); //USUÁRIO ESCOLHE ENTRE UMA DAS OPÇÃO VÁLIDAS
			}
			
			switch(op){
			case 1:
				analisarNumeros(e);
				break;
			case 2:
				analiseControle(e);
				break;
			case 3:
				analisarEquacao(e);
				break;
			}
			
			if(op != 0){ //SE O USUÁRIO NÃO DECIDIU ENCERRAR O PROGRAMA ENTÃO:
				byte controle = continuidade(e); //VERIFICA-SE SE O PROGRAMA DEVE CONTINUAR
				switch(controle){
				case 0: 
					op = 0; //PROGRAMA DEVE PARAR 
					break;	
				case 1: 
					repetir = true; //PROGRAMA DEVE REPETIR A ÚLTIMA OPÇÃO
					break;
				case 2: 
					repetir = false;//PROGRAMA DEVE RETORNAR AO MENU PRINCIPAL
					break;
				}
			}
		}while(op != 0);
		e.close(); //VARIAVEL DE ENTRADA DE DADOS É FINALIZADA
	}
	
	public static byte lerOpcao(Scanner e){
		//Funcao que obriga o usuario a digitar uma opcao valida e a retorna
		imprimirMenu();
		byte op = e.nextByte();
		while(op < 0 || op > 3){
			System.out.print("Erro!Digite uma opcao válida: ");
			op = e.nextByte();
		}
		return op;
	}

	public static void imprimirMenu(){
		System.out.print("\nMenu:\n"
				+ "1 - Análise de Números\n"
				+ "2 - Análise de Controle\n"
				+ "3 - Análise de Equação do Segundo Grau\n"
				+ "0 - Sair\n"
				+ "Digite uma opção: ");
	}

	public static byte continuidade(Scanner e){
		System.out.print("\nDigite '1' para repetir a última opção escolhida\n"
				+"Digite '2' para retornar ao menu principal\n"
				+"Digite '0' para sair\n"
				+"\tOpção: ");
		byte op = e.nextByte();
		while(op < 0 || op > 2){
			System.out.println("Erro! Digite uma opção válida: ");
			op=e.nextByte();
		}
		
		return op;
	}
	
	public static void analisarNumeros(Scanner e){
		AnaliseNumeros a = new AnaliseNumeros(definirQuantidade(e));
		/*definirQuantidade(e) retorna uma quantidade valida de elementos que são passados como
		 * parâmetro para o construtor AnaliseNumeros que instancia os elementos da classe
		 */
		a.preencherVetor(e);
		System.out.println("\nO menor: " + a.getMenor()
				       + "\nO maior: " + a.getMaior()
				       + "\nA media: " + a.getMedia());
		a.imprimirPositivos();
		a.imprimirNegativos();
		a.imprimirPrimos();
		System.out.println("\n\n");
	}
	public static int definirQuantidade(Scanner e){
		//FUNCAO QUE DETERMINA E RETORNA A QUANTIDADE DE ELEMENTOS A SEREM UTILIZADOS
		System.out.print("\nEntre com a quantidade de elementos a serem inseridos no arranjo: ");
		int qnt;
		while((qnt = e.nextInt()) <= 0){
			System.out.print("Erro! Digite uma quantidade válida: ");
		}
		return qnt;
	}

	public static void analiseControle(Scanner e){
		Turma t = new Turma();
		receberDisciplinas(e, t); //CADASTRA AS TRÊS DISCIPLINAS
		receberAlunos(e, t); //CADASTRA OS CINCO ALUNOS
		calcularMediaParaCurso(e, t);//CALCULA A MÉDIA DE CADA ALUNO PARA O CURSO E SE FOI APROVADO
		calcularMediaTurmas(e, t);//CALCULA A MÉDIA DA TURMA PARA CADA DISCIPLINA E GERAL 
		
	}
	public static void receberDisciplinas(Scanner e, Turma t){
		System.out.println("\nCadastrar disciplinas:\n");
		String disciplina = e.nextLine();//CONTROLE, ESSA LINHA SERVE SIMPLESMENTE PARA ACIONAR O nextLine()
		for(byte i=0; i<3; i++){
			System.out.print((i+1)+"ª disciplina: ");
			disciplina = e.nextLine();
			t.setDisciplina(disciplina, i);
		}
	}
	private static void receberAlunos(Scanner e, Turma t) {
		System.out.println("\nCadastrar alunos:\n");
		
		for(byte i=0; i<5; i++){ //CADASTRA CINCO ALUNOS
			System.out.print("\nNome completo: ");
			String nome = e.nextLine();
			t.aluno[i].setNome(nome);
			
			System.out.print("Matricula: ");
			t.aluno[i].setMatricula(e.nextInt());
			while(t.aluno[i].getMatricula() <= 0){
				System.out.print("Erro! Digite um número de matrícula válido: ");
				t.aluno[i].setMatricula(e.nextInt());
			}
			
			for(byte j=0; j<3; j++){//CADASTRA AS NOTAS DE CADA DISCIPLINA DE CADA ALUNO
				System.out.print("Nota em "+t.getDisciplina(j)+": ");
				t.aluno[i].setNota(e.nextFloat(), j);
				while(t.aluno[i].getNota(j) < 0 || t.aluno[i].getNota(j) > 100){
					System.out.print("Erro! Digite uma nota válida: ");
					t.aluno[i].setNota(e.nextFloat(), j);
				}
			}
			
			nome = e.nextLine(); //CONTROLE DO nextLine(), SEM ISSO DÁ RUIM!
		}
		
	}
	private static void calcularMediaParaCurso(Scanner e, Turma t) {
		System.out.println("\n\nMédia de cada aluno para o curso:\n");
		float media;
		for(byte i=0; i<5; i++){
			System.out.print(t.aluno[i].getNome()+": ");
			media = 0;
			
			for(byte j=0; j<3; j++){
				media += t.aluno[i].getNota(j);
			}
			media /= 3;
			char conceito = calcularConceito(media*0.1);
			System.out.println(media+" - "+conceito+" - "+situacao(conceito));
		}
		
	}
	private static char calcularConceito(double d){
		if(d >= 9.0){
			return 'A';
		}else if(d >= 6.0){
			return 'B';
		}else if(d >= 4.0){
			return 'C';
		}else return 'D';
	}
	private static String situacao(char conceito){
		if(conceito == 'A') return "Aprovado";
		else if(conceito == 'D') return "Reprovado";
		else return "Recuperação";
	}
	
	private static void calcularMediaTurmas(Scanner e, Turma t) {
		System.out.println("Média da turma nas disciplinas:\n");
		float mediaGeral = 0;
		for(byte i=0; i<3; i++){
			System.out.print(t.getDisciplina(i)+": ");
			float media = 0;
			for(byte j=0; j<5; j++){
				media += t.aluno[j].getNota(i);
			}
			media /= 5;
			mediaGeral += media;
			System.out.println(media);
		}
		
		System.out.println("\nMédia geral da turma: "+mediaGeral/3);
		
	}
	
	public static void analisarEquacao(Scanner e){
		Equacao a = new Equacao();
		System.out.print("\nPrimeiro Coeficiente(ax²): ");
		a.setCoeficienteA(e.nextInt());
		System.out.print("\n  Segundo Coeficiente(bx): ");
		a.setCoeficienteB(e.nextInt());
		System.out.print("\n  Terceiro Coeficiente(c): ");
		a.setCoeficienteC(e.nextInt());
		a.calculaRaizes();
	}
}
