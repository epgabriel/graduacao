public class Equacao {
	//declaração das variáveis que receberão os coeficientes
	private int a;
	private int b;
	private int c;
	
	public Equacao(){
		System.out.println("\nAnálise de Equação do Segundo Grau:");
	}
	
	//setters e getters
	void setCoeficienteA(int a){
		this.a = a;
	}
	
	void setCoeficienteB(int b){
		this.b = b;
	}
	
	void setCoeficienteC(int c){
		this.c = c;
	}
	//------------------------------
	
	void calculaRaizes(){ //funcao que calcula, se existirem, as raízes da equacao
		System.out.println("\n");
		float delta = (b*b) - (4*a*c); //calcula delta
		System.out.println("\nDelta: "+delta);
		if(delta < 0){
			System.out.println("Não há raízes possíveis com esses coeficientes!\n");
		}else if(delta == 0){
			System.out.printf("Raíz possível: %.2f",(float)(b*-1)/(2*a));
		}else{
			System.out.printf("Raízes possíveis: %.2f e %.2f\n", (float)raiz(b*(-1), Math.sqrt(delta), 2*a),
					(float)raiz(b*(-1), Math.sqrt(delta)*(-1), 2*a));
		}
	}
	
	double raiz(int b, double delta, int a){
		return (b + delta)/a;
	}

}
