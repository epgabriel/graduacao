import java.lang.String;

public class Turma{
	private String disciplina[] = new String[3];
	protected Aluno aluno[] = new Aluno[5];
	
	public Turma(){
		for(byte i=0; i<5; i++){
			this.aluno[i] = new Aluno();
		}
	}
	
	void setDisciplina(String disciplina, byte id){
		this.disciplina[id] = disciplina;
	}
	String getDisciplina(byte id){
		return this.disciplina[id];
	}
}
