import java.lang.String;

public class Aluno {
	private String nome;
	private String sobrenome;
	private int matricula;
	private float nota[] = new float[3];
	
	
	void setNome(String nome){
		this.nome = nome;
	}
	String getNome(){
		return this.nome;
	}
	
	void setSobrenome(String sobrenome){
		this.sobrenome = sobrenome;
	}
	String getSobrenome(){
		return this.sobrenome;
	}
	
	void setMatricula(int matricula){
		this.matricula = matricula;
	}
	int getMatricula(){
		return this.matricula;
	}
	
	void setNota(float nota, byte id){
		this.nota[id] = nota;
	}
	float getNota(byte id){
		return this.nota[id];
	}

}
