/*Biblioteca onde os métodos principais são desenvolvidos e chamam, cada
 * um, seus respectivos métodos referentes à opção escolhida, de forma 
 * mais organizada
 */ 

#include <stdio.h>
#include <math.h>
#include "analisarNumeros.h" //->Biblioteca para resolução do problema de analise de numeros
#include "controle.h" //->Biblioteca para resolução do problema de controle
#include "equacao.h" //->Biblioteca para resolução do problema da equacao de segundo grau

void imprimirMenu(){ //Imprime o Menu sempre que solicitado
	printf(" \n\nMenu:\n"
			"\t1 - Análise de Números\n"
			"\t2 - Análise de Controle\n"
			"\t3 - Análise de Equação de Segundo Grau\n"
			"\t0 - Sair\n\n");
}

void lerOpcao(int *op){ //Recebe uma opção válida digitada pelo usuário
	printf("\n\tDigite uma opcao: ");
	scanf("%d", op);
	while(*op < 0 || *op > 3){
		printf("\nOpcao Inválida! Digite uma opção válida: ");
		scanf("%d", op);
	}
}	

void analisarNumeros(){ //Opção 1, Realiza a análise de números
	int n; //Quantidade de Elementos
	lerQuantidadeNumeros(&n); //atribui à 'n' uma quantidade válida
	
	int vet[n];
	float media=0; //variável para o cálculo da média
	
	preencherVetor(vet, n, &media); //preenche o vetor e a atribui à 'soma' a soma de seus elementos
	ordenarVetor(vet,n); //ordena esse vetor
	imprimirAnalise(vet[0], vet[n-1], media);//Imprime menor, maior e media
	
	//Os nomes estão bem óbvios aqui e.e
	imprimirElementosPositivos(vet,n);
	imprimirElementosNegativos(vet,n);
	imprimirElementosPrimos(vet,n);
}

void analisarControle(){
	Aluno aluno[5];
	cadastrarNomes(aluno);
	cadastrarMatricula(aluno);
	cadastrarDisciplinas(aluno);
	cadastrarNotas(aluno);
	imprimirEstadoFinal(aluno);
	calcularMediaGeral(aluno);
}
	
void analisarEquacao(){
	printf("\nInsire um valor para os coeficientes abaixo:\n");
	float a, b, c;
	lerPrimeiroCoeficiente(&a);
	lerSegundoCoeficiente(&b);
	lerTerceiroCoeficiente(&c);
	float delta = ((b*b)-4*a*c);
	printf("\n\tDelta = %.2f", delta);
	
	if(delta<0){
		printf("\n\tNão há resultados possíveis\n");
	}else if(delta==0){
		printf("\n\tValor para 'x': %.2f", calcularX(0,b*(-1),a*2));
	}else{
		printf("\n\tValores para 'x': %.2f e %.2f\n", 
				calcularX(sqrt(delta),b*(-1),a*2),
				calcularX(sqrt(delta)*-1,b*(-1),a*2));
	}
}

void continua(int *controle){
	printf("\n\nOperação realizada com sucesso!\n"
			"Digite '1' para voltar ao menu principal\n"
			"Digite '2' para repetir a última operação realizada\n"
			"Digite '0' para sair\n"
			"\tOpção: "); 
	scanf("%d", controle);
	while(*controle<0 || *controle>2){
		printf("Opção INVÁLIDA!! Digite uma opção válida: ");
		scanf("%d", controle);
	}
	printf("\n");
}
	
