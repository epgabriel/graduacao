#include "funcoesControle.h"

void       cadastrarNomes(Aluno*);
void   cadastrarMatricula(Aluno*);
void cadastrarDisciplinas(Aluno*);
void       cadastrarNotas(Aluno*);
void  imprimirEstadoFinal(Aluno*);
void   calcularMediaGeral(Aluno*);
