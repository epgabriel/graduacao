#include <stdio.h>
#include <string.h>

typedef struct{
	char discp[30];
	float nota;
}Disciplina;

typedef struct{
	char nome[30];
	int matricula;
	float media;
	Disciplina disciplina[3];
}Aluno;

void cadastrarNomes(Aluno *aluno){
	printf("\nDigite o nome completo dos cinco aluno a serem cadastrados:\n");
	int i=0;
	for(; i<5; i++){
		printf("Nome do %d aluno: ", i+1);
		scanf("%*c%[^\n]", aluno[i].nome);
	}
}
void  cadastrarMatricula(Aluno *aluno){
	printf("\n\nDigite o número de matricula de cada um dos alunos cadastrados:\n");
	int i=0;
	for(; i<5; i++){
		printf("\nMatricula de %s: ", aluno[i].nome);
		scanf("%d", &aluno[i].matricula);
		while(aluno[i].matricula <= 0){
			printf("Erro! Digite um número de matricula válido: ");
			scanf("%d", &aluno[i].matricula);
		}
	}
			
}

void cadastrarDisciplinas(Aluno *aluno){
	printf("\n\nCadastre as três disciplinas:\n");
	printf("Primeira Disciplina: ");
	scanf("%*c%[^\n]", aluno[0].disciplina[0].discp);
	printf("Segunda Disciplina:  ");
	scanf("%*c%[^\n]", aluno[0].disciplina[1].discp);
	printf("Terceira Disciplina: ");
	scanf("%*c%[^\n]", aluno[0].disciplina[2].discp);
}

void cadastrarNotas(Aluno *aluno){
	printf("\nCadastrar notas para os alunos cadastrados:\n");
	int i,j;
	for(i=0; i<5; i++){
		printf("\n%s\n", aluno[i].nome);
		aluno[i].media = 0;
		for(j=0; j<3; j++){
			printf("\tNota em %s: ", aluno[0].disciplina[j].discp);
			scanf("%f", &aluno[i].disciplina[j].nota);
			while(aluno[i].disciplina[j].nota < 0 || aluno[i].disciplina[j].nota > 100){
				printf("\tErro! Forneça uma nota válida: ");
				scanf("%f", &aluno[i].disciplina[j].nota);
			}
			aluno[i].media += aluno[i].disciplina[j].nota;
		}
		aluno[i].media /= 3;
	}
}

char conceito(float media){
	if(media > 9.0) return 'A';
	else if(media > 6.0) return 'B';
	else if(media > 4.0) return 'C';
	else return 'D';
}
void estadoFinal(char c){
	if(c == 'A') printf(" - Aprovado!");
	else if(c == 'D') printf(" - Reprovado!");
	else printf(" - Recuperação!");
}
void imprimirEstadoFinal(Aluno *aluno){
	printf("\nEstado final de cada um dos alunos cadastrados:\n");
	int i;
	char _conceito;
	for(i=0; i<5; i++){
		_conceito = conceito(aluno[i].media*0.1);
		printf("\n%s: %.2f - %c", aluno[i].nome, aluno[i].media, _conceito);
		estadoFinal(_conceito);
	}
	printf("\n");
}
		

void calcularMediaGeral(Aluno *aluno){
	printf("\nMedia Da Turma em cada disciplina:\n");
	int i,j;
	float media;
	for(i=0; i<3; i++){
		media = 0;
		printf("\n%s: ", aluno[0].disciplina[i].discp);
		for(j=0; j<5; j++){
			media += aluno[j].disciplina[i].nota;
		}
		printf("%.2f", media/5);
	}
	
	float mediaFinal=0;
	for(i=0;i<5;i++){
		mediaFinal += aluno[i].media;
	}
	printf("\n\nMédia geral final: %.3f", mediaFinal/5);
		
}


