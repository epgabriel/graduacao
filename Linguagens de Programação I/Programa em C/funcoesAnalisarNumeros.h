//biblioteca com o desenvolvimento dos protótipos em 'analisarNumeros.h'

#include <stdio.h>

//Protótipos de 'sub-funções' a serem utilizadas nessa biblioteca
int trocarElementos(int*, int*);
void verificaPrimos(int,int*);
//--------------------------------

void lerQuantidadeNumeros(int *n){
	printf("\nDigite a quantidade de elementos a serem inseridos neste arranjo: ");
	scanf("%d", n);
	while(*n<0){
		printf("Entrada INVÁLIDA! Repita a operação: ");
		scanf("%d", n);
	}
}

void preencherVetor(int *vet, int n, float *media){
	printf("\nEntre com os elementos:\n");
	int i=0;
	for(; i<n; i++){ //Laço para ler elementos
		scanf("%d", &vet[i]);
		*media += vet[i];
	}
	*media /= n; //calculo da media
}

void ordenarVetor(int *vet, int n){
	int i=0;
	int a;
	int qnt = n;
	
	//método bolha
	do{
		a = 1;
		qnt--;
		for(i=0; i<qnt; i++){
			if(vet[i] > vet[i+1]){
				a = trocarElementos(&vet[i], &vet[i+1]);
			}
		}
	}while(a==0);
}
int trocarElementos(int *A, int *B){
	int C = *A;
	*A = *B;
	*B = C;
	return 0;
}

void imprimirAnalise(int menor, int maior, float media){
	printf("\n\n\t\t          O menor elemento: %d"
		   "\n\t\t          O maior elemento: %d"
		   "\n\t\tA média entre os elementos: %.2f"
		   ,menor,maior,media);
}

void imprimirElementosPositivos(int *vet, int n){
	//imprime os elementos positivos evitando repetições
	
	printf("\n\nElemento(s) Positivo(s) no Arranjo: ");
	int i=1, a=0; //'i' controla iteracao e 'a' verifica se há elementos
	int aux = vet[0];
	
	if(aux > 0){
		printf("%d ",aux);
		a++;
	}
	
	for(; i<n; i++){
		if(vet[i] > 0 && vet[i] != aux){
			printf("%d ",vet[i]);
			aux = vet[i];
			a++;
		}
	}
	
	if(a == 0) printf("Não há elementos positivos neste arranjo!");
}

void imprimirElementosNegativos(int *vet, int n){
	printf("\nElemento(s) Negativo(s) no Arranjo: ");
	int i=1, a=0; //'i' controla iteracao e 'a' verifica se há elementos
	int aux = vet[0];
	if(aux < 0){
		printf("%d ",aux);
		a++;
	}
	for(; i<n; i++){
		if(vet[i] < 0 && vet[i] != aux){
			printf("%d ",vet[i]);
			aux = vet[i];
			a++;
		}
	}
	
	if(a == 0) printf("Não há elementos negativos neste arranjo!");
}


void imprimirElementosPrimos(int *vet, int n){
	//imprime os elementos primos no arranjo evitando repetições
	printf("\nElemento(s) \"Primo(s)\" no Arranjo: ");
	int i=1;
	int existeElementos=0;
	int aux = vet[0];
	/* 'i' controla iteracao e 'existeElementos' verifica se há elementos
	 * 'aux' serve para ser feito um controle que evita comparar e
	 * imprimir elementos repetidos
	 */ 
	
	if(aux > 0){
		verificaPrimos(aux, &existeElementos);
	}
	
	for(; i<n; i++){
		if(vet[i] > 0 && vet[i] != aux){
			verificaPrimos(vet[i], &existeElementos);
			aux = vet[i];
		}
	}
	
	if(existeElementos == 0) printf(" Não há números primos neste arranjo!");
}
void verificaPrimos(int elemento, int *existeElementos){
	int a=0;
	int j=2;
	while(j<elemento && a == 0){
		if(elemento%j == 0){ //Se o elemento não é primo então...
			a++; //... deve-se parar o laço
		}
		j++; //incrementa a variavel de iteracao
	}
	if(a==0){
		printf(" %d", elemento);
		*existeElementos = 1;
	}
}
