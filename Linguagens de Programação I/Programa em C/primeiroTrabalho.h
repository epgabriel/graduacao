//Biblioteca principal chamada pelo método main

#include "funcoes.h" //-> Biblioteca com o desenvolvimento dos protótipos abaixo:

void     imprimirMenu(); //Imprime o menu
void     lerOpcao(int*); //Define uma opção válida

//-------Desenvolve os métodos de cada opção proposta pelo trabalho-----
void  analisarNumeros(); 
void analisarControle();
void  analisarEquacao();
//----------------------------------------------------------------------

void     continua(int*); //Verifica se o programa deve continuar, repetir, etc
