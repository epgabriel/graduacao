/* Cabeçalho da biblioteca 'funcoesAnalisarNumeros.h'
 * com os seus métodos principais
 */ 

#include "funcoesAnalisarNumeros.h"

void lerQuantidadeNumeros(int*);
void preencherVetor(int*,int,float*);
void ordenarVetor(int*,int);
void imprimirAnalise(int,int,float);
void imprimirElementosPositivos(int*,int);
void imprimirElementosNegativos(int*,int);
void imprimirElementosPrimos(int*,int);




