#include <stdio.h>

void lerPrimeiroCoeficiente(float *a){
	printf("\tCoeficiente 'a': ");
	scanf("%f",&*a);
}

void lerSegundoCoeficiente(float *b){
	printf("\tCoeficiente 'b': ");
	scanf("%f",&*b);
}

void lerTerceiroCoeficiente(float *c){
	printf("\tCoeficiente 'c': ");
	scanf("%f",&*c);
}
	 
float calcularX(float delta, float b, float a){
	return (b+delta)/a;
}
