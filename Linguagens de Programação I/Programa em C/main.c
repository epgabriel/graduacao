#include <stdio.h>
#include "primeiroTrabalho.h"

int main(){
	printf("\t================Bem Vindo================");
	int op; //variavel a ser usada para guardar a opcao do usuario
	int controle = 1; //variavel auxiliar para ajudar no controle de continuidade
	do{
		if(controle == 1){
			imprimirMenu(); //imprime na tela as opções do menu
			lerOpcao(&op); //le uma opcao valida digitada pelo usuario
		}
		
		switch(op){ //controle dessa opção lida
			case 1: //se for 1 entao analisar numeros
				analisarNumeros();
			break;
			
			case 2://se for 2 entao analisar controle
				analisarControle();
			break;
			
			case 3://se for 3 entao analisar equacao de grau 2
				analisarEquacao();
			break;
		}
		
		if(op != 0){ //Se não foi solicitado o encerramento do programa então...
			continua(&controle);  
			if(controle == 0){ //se controle igual à zero então o programa deve ser encerrado
				op = controle; //op passará a ser '0' e encerrará o laço que é o programa
				printf("Bye bye!");
			}
		}		
	}while(op!=0);
		
	return 0;
}
