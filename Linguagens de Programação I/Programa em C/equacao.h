#include "funcoesEquacao.h"

void lerPrimeiroCoeficiente(float*);

void lerSegundoCoeficiente(float*);

void lerTerceiroCoeficiente(float*);

float calcularX(float delta, float b, float a);

