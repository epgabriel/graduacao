package app;

public class Magica extends Carta{
	
	private String efeito;
	private boolean quickPlay;
	
	public Magica(String nomeCarta, String efeito, boolean quickPlay){
		super(nomeCarta, "Magica");
		this.efeito = efeito;
		this.quickPlay = quickPlay;
	}
	
	public void setEfeito(String efeito){
		this.efeito = efeito;
	}
	
	
	public String getEfeito(){
		return efeito;
	}
	
	public void setQuickPlay(boolean quickPlay){
		this.quickPlay = quickPlay;
	}
	
	public boolean getQuickPlay(){
		return quickPlay;
	}
        
        @Override
        public void setNome(String nome){
            super.setNome(nome);
        }
        
        @Override
        public String getNome(){
            return "Mágica: "+super.getNome();
        }

}
