package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Jogador {
	
	private String nome;
	static private int codGeral = 0;
	private int cod;
	private int vitorias;
	private int derrotas;
	private Deck deck;
        static public List<Jogador> jogadores = new ArrayList<Jogador>();
		
	
	public Jogador(String nome) {
		this.nome = nome;
		this.cod = ++codGeral;
		vitorias = 0;
		derrotas = 0;
		this.deck = new Deck();
                
	}
        
        public void setNome(String nome){
            this.nome = nome;
        }
	
	public String getNome(){
		return this.nome;
	}
	public void setVitoria(){
		vitorias++;
	}
	
	public void setDerrota(){
		derrotas++;
	}
	
	public boolean jogar(){
		Random r = new Random();
		
		if(r.nextBoolean()){
			this.vitorias++;
			return true;
		}
		else{
			this.derrotas++;
			return false;
		}
	}
	
	public String dadosJogador(){
		return "Nome: "+nome
				+ "\nCódigo: "+cod
				+ "\nVitórias: "+vitorias
				+ "\nDerrotas: "+derrotas
				+ "\nTotal de partidas: "+(vitorias+derrotas);
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}
	
	public void setCarta(Carta carta){
		this.deck.cartas.add(carta);
	}
	
	public int getCodigo(){
		return cod;
	}
	

}
