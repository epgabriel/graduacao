package app;


import java.util.ArrayList;
import java.util.List;

public class Deck {
	protected List<Carta> cartas;
	private static int limCartas = 42;
	
	Deck(){
		cartas = new ArrayList<Carta>();
	}
	
	public List<Carta> getCartas() {
		return cartas;
	}

	public void setCarta(Carta carta) {
		cartas.add(carta);
	}
	
	public int getQntCartas(){
		return cartas.size();
	}
	
	public int getLimiteCartas(){
		return limCartas;
	}
}
