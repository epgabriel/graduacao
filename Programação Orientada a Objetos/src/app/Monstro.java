package app;

public class Monstro extends Carta {
	
	private int hp;
	private int atq;
	private int def;
	private String efeito;
	
	public Monstro(String nomeCarta, String efeito, int hp, int atq, int def){
		super(nomeCarta, "Monstro");
		this.hp = hp;
		this.atq = atq;
		this.def = def;
		this.efeito = efeito;
	}

	public int getAtq() {
		return atq;
	}

	public void setAtq(int atq) {
		this.atq = atq;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}
	
	public String getEfeito(){
		return efeito;
	}
	
	public void setEfeito(String efeito){
		this.efeito = efeito;
	}
        
        @Override
        public String getNome(){
            return "Monstro: "+super.getNome();
        }
        
        @Override
        public void setNome(String nome){
            super.setNome(nome);
        }

}
