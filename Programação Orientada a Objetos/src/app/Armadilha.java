package app;

public class Armadilha extends Carta{

	private String efeito;

	public Armadilha(String nomeCarta, String efeito){
		super(nomeCarta, "Armadilha");
		this.efeito = efeito;
	}
	
	public void setEfeito(String efeito){
		this.efeito = efeito;
	}
	
	public String getEfeito(){
		return efeito;
	}
        
        @Override
        public String getNome(){
            return "Armadilha: "+super.getNome();
        }
        
        @Override
        public void setNome(String nome){
            super.setNome(nome);
        }



}
