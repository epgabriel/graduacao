package app;

public abstract class Carta {
	
	private String nomeCarta;
	private String tipo;
	
	public Carta(String nomeCarta, String tipo){
		this.nomeCarta = nomeCarta;
		this.tipo = tipo;
	}
        
        public String getNome(){
            return nomeCarta;
        }
        
        public void setNome(String nomeCarta){
            this.nomeCarta = nomeCarta;
        }
        
        public String getTipo(){
            return tipo;
        }

}
