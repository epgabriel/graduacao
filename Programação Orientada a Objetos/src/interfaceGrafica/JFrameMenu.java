/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGrafica;

/**
 *
 * @author Enio
 */
public class JFrameMenu extends javax.swing.JFrame {

    /**
     * Creates new form JFrameMenu
     */
    public JFrameMenu() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPanePrincipal = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemSimularDuelo = new javax.swing.JMenuItem();
        jMenuItemBuscarJogador = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItemMostrarDeck = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItemMonstro = new javax.swing.JMenuItem();
        jMenuItemMagica = new javax.swing.JMenuItem();
        jMenuItemArmadilha = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMinimumSize(new java.awt.Dimension(620, 415));
        setResizable(false);
        getContentPane().setLayout(null);

        jDesktopPanePrincipal.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                jDesktopPanePrincipalComponentShown(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaceGrafica/yugioh-netflix.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        jLabel1.setInheritsPopupMenu(false);
        jLabel1.setPreferredSize(new java.awt.Dimension(1023, 415));
        jDesktopPanePrincipal.add(jLabel1);
        jLabel1.setBounds(0, 0, 1023, 415);

        getContentPane().add(jDesktopPanePrincipal);
        jDesktopPanePrincipal.setBounds(0, 0, 680, 420);

        jMenu1.setText("Principal");

        jMenuItemSimularDuelo.setText("Simular Duelo");
        jMenuItemSimularDuelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSimularDueloActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemSimularDuelo);

        jMenuItemBuscarJogador.setText("Buscar/Excluir Jogador");
        jMenuItemBuscarJogador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemBuscarJogadorActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemBuscarJogador);

        jMenuItem5.setText("Cadastrar Jogador");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenu3.setText("Atualizar Deck");

        jMenuItem7.setText("Remover Carta");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem7);

        jMenuItemMostrarDeck.setText("Mostrar Deck");
        jMenuItemMostrarDeck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemMostrarDeckActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItemMostrarDeck);

        jMenu4.setText("Inserir Carta");

        jMenuItemMonstro.setText("Monstro");
        jMenuItemMonstro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemMonstroActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemMonstro);

        jMenuItemMagica.setText("Magica");
        jMenuItemMagica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemMagicaActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemMagica);

        jMenuItemArmadilha.setText("Armadilha");
        jMenuItemArmadilha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemArmadilhaActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItemArmadilha);

        jMenu3.add(jMenu4);

        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        JInternalFrameInserirJogador obj= new JInternalFrameInserirJogador();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItemBuscarJogadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemBuscarJogadorActionPerformed
        // TODO add your handling code here:
        JInternalFrameBuscarJogador obj= new JInternalFrameBuscarJogador();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemBuscarJogadorActionPerformed

    private void jMenuItemSimularDueloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSimularDueloActionPerformed
        // TODO add your handling code here:
        JInternalFrameDuelo obj= new JInternalFrameDuelo();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemSimularDueloActionPerformed

    private void jMenuItemMonstroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMonstroActionPerformed
        // TODO add your handling code here:
        JInternalFrameMonstro obj= new JInternalFrameMonstro();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemMonstroActionPerformed

    private void jMenuItemMostrarDeckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMostrarDeckActionPerformed
        // TODO add your handling code here:
        NewJInternalMostrarDeck obj= new NewJInternalMostrarDeck();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemMostrarDeckActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        JInternalRemoverCarta obj= new JInternalRemoverCarta();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItemMagicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemMagicaActionPerformed
        // TODO add your handling code here:
        JInternalMagica obj= new JInternalMagica();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemMagicaActionPerformed

    private void jMenuItemArmadilhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemArmadilhaActionPerformed
        // TODO add your handling code here:
        JInternalArmadilha obj= new JInternalArmadilha();
        jDesktopPanePrincipal.add(obj);
        obj.setVisible(true);
    }//GEN-LAST:event_jMenuItemArmadilhaActionPerformed

    private void jDesktopPanePrincipalComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jDesktopPanePrincipalComponentShown
        // TODO add your handling code here:
    }//GEN-LAST:event_jDesktopPanePrincipalComponentShown

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPanePrincipal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItemArmadilha;
    private javax.swing.JMenuItem jMenuItemBuscarJogador;
    private javax.swing.JMenuItem jMenuItemMagica;
    private javax.swing.JMenuItem jMenuItemMonstro;
    private javax.swing.JMenuItem jMenuItemMostrarDeck;
    private javax.swing.JMenuItem jMenuItemSimularDuelo;
    // End of variables declaration//GEN-END:variables
}
