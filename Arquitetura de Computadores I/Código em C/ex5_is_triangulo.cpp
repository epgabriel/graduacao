#include <iostream>

using namespace std;

int main() {
	
	int a, b, c;
	
	cout << "Entre com os lados do triângulo: ";
	cin >> a >> b >> c;
	
	// |b-c| < a < |b+c|
	// |a-c| < b < |a+c|
	// |a-b| < c < |a+b|
	
	int isT = 0;
	
	int aux = b-c;
	if(aux < 0) aux *= -1;
	if((b-c) < a && a < (b+c)) isT++;
	
	aux = a-c;
	if(aux < 0) aux *= -1;
	if((a-c) < b && b < (a+c)) isT++;
	
	aux = a-b;
	if(aux < 0) aux *= -1;
	if((a-b) < c && c < (a+b)) isT++;
	
	if(isT == 3) cout << "É triângulo!\n";
	else cout << "Não é triângulo!\n";
	
	return 0;
}
