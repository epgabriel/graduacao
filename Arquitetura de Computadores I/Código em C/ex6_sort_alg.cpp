#include <iostream>

using namespace std;

void swap(int v[], int k) {
	int temp;
	temp = v[k];
	v[k] = v[k+1];
	v[k+1] = temp;
}

void sort(int v[], int n) {
	int i, j;
	for(i = 0; i < n; i++) {
		for(j = i-1; j >= 0 && v[j] > v[j+1]; j--) {
			swap(v,j);
		}
	}
}

int main() {
	int n = 5;
	int v[n];
	
	cout << "Elementos: ";
	
	for(int i = 0; i < n; i++) {
		cin >> v[i];
	}
	
	sort(v, n);
	
	cout << "Elementos: ";
	
	for(int i = 0; i < n; i++) {
		cout << v[i] << " ";
	}
	
	return 0;
}
