#include <iostream>

using namespace std;

bool swap(int &A, int &B) {
	int C = A;
	A = B;
	B = C;
	return true;
}

void sort(int vet[], int n) {
	bool a = false;
	do{
		a = false;
		for(int i = 0; i < n-1; i++) {
			if(vet[i] < vet[i+1]) {
				a = swap(vet[i], vet[i+1]);
			}
		}
	}while(a);
}

int main() {
	int n;
	cout << "N: ";
	cin >> n;
	
	while (n <= 0) {
		cout << "Please, enter with a valid number of elements: ";
		cin >> n;
	}
	
	int vet[n];
	
	for(int i = 0; i < n; i++) {
		cout << i+1 << "o number: ";
		cin >> vet[i];
	}
	
	sort(vet, n);
	
	for(int i = 0; i < n; i++) {
		cout << vet[i] << " ";
	}
	cout << endl;
	
	return 0;
}
