# Title: Clear Algorithm		Filename: ex2_decres.asm
#Author: Gabriel Edmilson P.	Date: 11/14/2016
#Description: This program implements both the proposed Clear algorithm of exercise 7
#Input:  An option of which method will be used
#Output: Array clean

#Note: Was used an array (can be checked in field 'data'), it is possible to see the cleaning process by the data field on execution
#      The two forms are implemented is this program, that is the why of asking for the user choose a method

.data
	presentation:	.asciiz	"\n\t::Clear Array Program::\n"
	result_m1:	.asciiz "\nMethod 1 done, no errors.\n"
	result_m2:	.asciiz "\nMethod 2 done, no errors.\n"
	prompt_c:	.asciiz "\nPlease, which method do you want to use?\n1 - Cleaning using the array itself\n2 - Cleaning using a pointer to the array\n\t Option: "
	prompt_ce:	.asciiz	"Please! Enter with a valid option (1) (2): "
	str_space:	.asciiz " "
	array:		.word	23, 10, 5, 4, 109, 23, 41
	size:		.word	28
	
	
.text
.globl main

main:	li $v0, 4		#Prints a little presentation
	la $a0, presentation
	syscall
	
	li $v0, 4		#Asks the user to choose which method will be used
	la $a0, prompt_c
	syscall
	
L1:	la  $a0, array		#a0 = array
	la  $a1, size
	lw $a1, ($a1)		#a1 = size

	li $v0, 5		#Gets user's option
	syscall
	move $t0, $v0
	
	beq $t0, 1, method1	#if op == 1 then method1
	beq $t0, 2, method2	#else if op == 2 then method2
	
	li $v0, 4		#else prints an error message and reads another option
	la $a0, prompt_ce
	syscall
	
	j L1
	
method1:addi $t0, $zero, 0	#method1, cleaning by the array itself
	
loopm1:				#method1's lopp
	sw   $zero, array($t0)	#array[i] = 0
	addi $t0, $t0, 4	#i++
	blt  $t0, $a1, loopm1	#if i < size then next iteration
	
	li $v0, 4		#Prints a message telling that the method is done and without errors
	la $a0, result_m1
	syscall
	
	j print			#Prints the clean array
	
method2:add $t2, $zero, $a0	#p = &array[0]
	add $t1, $a0, $a1	#t1 = &array[size]
	

loopm2:	sw   $zero, ($t2)	#*p = 0
	addi $t2, $t2, 4	#p = p+1
	blt  $t2, $t1, loopm2	#if p < &array[size] then iterates again
	
	li $v0, 4		#Prints a message telling that the method is done and without errors
	la $a0, result_m2
	syscall
	
print:	addi $t0, $zero, 0	#print label, t0 = 0

P1:	lw $t1, array($t0)	#t1 = vet[t0]
	
	li $v0, 1		#prints vet[t0]
	move $a0, $t1
	syscall
	
	li $v0, 4		#prints a space
	la $a0, str_space
	syscall
	
	addi $t0, $t0, 4	#i++
	blt  $t0, $a1, P1	#if i < size then continue printing
	
	
EXIT:	li $v0, 10
	syscall
		
