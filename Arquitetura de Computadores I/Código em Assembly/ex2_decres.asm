# Title: Prints Decrescent	Filename: ex2_decres.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: Prints n readed numbers in decrescent order
#Input:  n numbers
#Output: n numbers in decrescent order

############### Data segment ###############
.data
	prompt_n: .asciiz "\nEnter the number of elements you want to print decremently: "
	prompt_en:.asciiz "\nPlease, enter with a valid number of elements (>0): "
	prompt_nn:.asciiz "º number: "
	prompt_r: .asciiz "\nYour elements in decrescent order: "
	str_space:.asciiz " "
	str_endl: .asciiz "\n"
	

############### Code segment ###############
.text

#t0 -> i 
#s0 -> v size
#s1 -> v[]
#s2 -> v size in bytes
.globl main

	main:	li $v0, 4	# Give information to the user, and gets the intended array size
		la $a0, prompt_n
		syscall
		
		li $v0, 5	#Gets the intended size
		syscall
		move $s0, $v0	#s0 = normal size, not the bytes

		bgtz $s0, allocate_array
	
	
  validate_n:	li $v0, 4	#A loop label to guarantee that the user sets a valid array size
		la $a0, prompt_en
		syscall
		
		li $v0, 5
		syscall
		move $s0, $v0
		
		blez $s0, validate_n
		
allocate_array:	mul $t1, $s0, 4		#t1 = number of bytes to allocate
		li $v0, 9		#9 indicates allocation
		move $a0, $t1		#a0 keeps the number of bytes allocated
		syscall			#v0[a0]
		
		move $s1, $v0		#s1 = address of allocated memory []
		move $s2, $t1		#s2 = array size in bytes
		
		addi $t0, $zero, 0	#t0 = 0 (its a incrementable variable)
			
read_elements:	addi $t1, $t0, 1	#t1 is an auxiliar for printing
		mul $t5, $t0, 4		#t5 = current id byte(s)

		li $v0, 1		#Prints the current number to read
		move $a0, $t1
		syscall
			
		li $v0, 4		#Prints the instruction
		la $a0, prompt_nn
		syscall
			
		li $v0, 5		#Reads the number
		syscall
		move $t2, $v0
			
		add $t3, $s1, $t5	#t3 = actual byte id
		sw  $t2, ($t3)
			
		addi $t0, $t0, 1	#i++
			
		blt $t0, $s0, read_elements
		
	
	sort:	addi $s3, $zero, 0	#a = false
		addi $t0, $zero, 0	#i = 0
	
	loop:	mul  $t1, $t0, 4	#t1 = i adress in byte for the array
		add  $t1, $t1, $s1	#t1 = vet[i]
		addi $t2, $t0, 1	#t2 = i+1 dec
		mul  $t2, $t2, 4	#t2 = i+1 adress in byte for the array
		add  $t2, $t2, $s1	#t2 = vet[i+1]

		lw  $t3, ($t1)		#t3 = vet[i]
		lw  $t4, ($t2)		#t4 = vet[2]

		bge $t3, $t4, L1	#if vet[t1] >= vet[t2] then next iteration
		
	swap:	addi $s3, $zero, 1	#a = true
		
		sw $t4, ($t1)		#vet[i] = t4
		sw $t3, ($t2)		#vet[2] = t3
		
	L1:	addi $t0, $t0, 1	#i++
		addi $t1, $s0, -1	#t1 = n-1
		
		blt  $t0, $t1, loop	#if i < n-1 then loop
		beqz $s3, L2		#else if i == n-1 and a == false then print_array
		j sort			#else sort
		
	L2:	addi $t0, $zero, 0	#t0 = i = 0
		add  $t1, $zero, $s1	#t1 = &vet[0]
		
		li $v0, 4		#Prints prompt_r
		la $a0, prompt_r
		syscall
		
print_array:	lw $t2, ($t1)		#t1 = vet[i]
		
		li $v0, 1		#prints vet[i]
		move $a0, $t2
		syscall
		
		li $v0, 4		#prints space
		la $a0, str_space
		syscall
		
		addi $t0, $t0, 1	#i++
		addi $t1, $t1, 4	#vet[i] => vet[i+1]
		blt  $t0, $s0, print_array
		
		li $v0, 4		#Prints str_endl
		la $a0, str_endl
		syscall
	
	exit:	li $v0, 10
		syscall
	