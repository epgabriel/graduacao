# Title: Productory of n odd numbers	Filename: ex3_prod_impares.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: Prints the result of the productory of the firsts odd numbers <= n
#Input:  Integer n
#Output: Productory of odd numbers less or equal to n

############### Data segment ###############
.data
	prompt_n: .asciiz "  Please, type the integer 'n': " 
	prompt_r: .asciiz "             The productory is: "
############### Code segment ###############
.text
#s0 -> n
#s1 -> prod = 1
#t0 -> i = 1
.globl main

	main:	#main program entry
	
		#Prints the prompt_n and reads an integer
		li $v0, 4
		la $a0, prompt_n
		syscall
		li $v0, 5
		syscall
		move $s0, $v0
		#----------------------------------------#
	
		addi $s1, $zero, 1	#s1 = prod = 1, auxiliar var
		addi $t0, $zero, 1	#s2 = i = interator auxiliar var
	
	for:	mul $s1, $s1, $t0	#s1 = s1*t0 -> prod *= i
		addi $t0, $t0, 2	#i += 2
		ble $t0, $s0, for	#if(i < n) for()
		
		#Prints the results
		li $v0, 4
		la $a0, prompt_r
		syscall
		li $v0, 1
		move $a0, $s1
		syscall
		#-----------------#
		
		#exit
		li $v0, 10
		syscall
	
