# Title: Triangular Number	Filename: ex4_triang_number.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: A number is said triangular if it is the product of three consectives natural numbers
#Input:  Integer n
#Output: If it is triangular so prints its three consecutive natural numbers, else prints an warning

############### Data segment ###############
.data
	prompt_n: .asciiz "Please, type the integer 'n': " 
	prompt_r: .asciiz "Your three consecutives numbers: "
	prompt_e: .asciiz "Sorry, but your number is not triangular\n"
	str_space:.asciiz " "
	str_endl: .asciiz "\n"
	
############### Code segment ###############
.text
#s0 -> n
#t0 -> i = 0 -> first number
#t1 -> second number
#t2 -> third number
.globl main

	main:	#main program entry
	
		#Prints the prompt_n and reads an integer
		li $v0, 4
		la $a0, prompt_n
		syscall
		li $v0, 5
		syscall
		move $s0, $v0
		#----------------------------------------#
		
		
		addi $t0, $zero, 0
		
	for:	addi $t0, $t0, 1	# i++
		addi $t1, $t0, 1	# i+1
		addi $t2, $t0, 2 	# i+2
		mul  $t3, $t1, $t2	# t3 = (i+1) * (i+2)
		mul  $t3, $t3, $t0	# t3 *= i
		blt  $t3, $s0, for	#if(i*(i+1)*(i+2) < n then for
		
		beq $t3, $s0, success	#if(t3 == n) then n is triangular, print it
		
		#else, prints error
		li $v0, 4
		la $a0, prompt_e
		syscall
		
		j exit
		
	success:#prints the numbers
		li $v0, 4
		la $a0, prompt_r
		syscall
		
		li $v0, 1
		move $a0, $t0
		syscall
		
		li $v0, 4
		la $a0, str_space
		syscall
		
		li $v0, 1
		move $a0, $t1
		syscall
		
		li $v0, 4
		la $a0, str_space
		syscall
		
		li $v0, 1
		move $a0, $t2
		syscall
		
	exit:	li $v0, 10
		syscall	