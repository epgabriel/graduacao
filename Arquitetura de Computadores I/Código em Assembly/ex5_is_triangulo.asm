# Title: "Is it a Triangulo?"	Filename: ex5_is_triangulo.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: Given three natural numbers, verify if it can be a triangulo
#Input:  Three natural numbers, 'a', 'b' e 'c'
#Output: Inform if the given values can form a triangulo

############### Data segment ###############
.data
	prompt_fs: .asciiz "Please, type the first side:  " 
	prompt_ss: .asciiz "Please, type the second side: " 
	prompt_ts: .asciiz "Please, type the third side:  " 
	prompt_r: .asciiz "\nIt is a triangulo!! Congratulations!\n"
	prompt_e: .asciiz "\nSorry, but it is not a triangulo.\n"
	str_space:.asciiz " "
	str_endl: .asciiz "\n"
	
############### Code segment ###############
.text
#s0 -> auxiliar, if it is 3 at the final so it is a triangulo
#s1 -> fist side
#s2 -> second side
#s3 -> third side
.globl main

		#main program entry
	main:	#Initial process, asks for the three sides	
		
		li $v0, 4		#Reads the first side
		la $a0, prompt_fs
		syscall
		li $v0, 5
		syscall
		move $s1, $v0
		
		li $v0, 4		#Reads the second side
		la $a0, prompt_ss
		syscall
		li $v0, 5
		syscall
		move $s2, $v0
		
		li $v0, 4		#Reads the third side
		la $a0, prompt_ts
		syscall
		li $v0, 5
		syscall
		move $s3, $v0
		
		addi $s0, $zero, 0	#Initializes s0 with 0
		
	verifyA:sub $t0, $s2, $s3	#t0 = b-c
		bltz $t0, invertA	#if b-c < 0 then invertA
		blt $t0, $s1, checkA	#if b-c < a then checkA
		j fail			#else exists
		
	invertA:add $t1, $zero, $s2	#t1 = b
		add $s2, $zero, $s3	#b = c
		add $s3, $zero, $t1	#c = t1
		j verifyA		#jumps to verifyA
		
	checkA: add $t1, $s2, $s3	#t1 = s2 + s3 -> b+c
		add $s0, $s0, 1		#increments s0
		bgt $t1, $s1, verifyB	#if s2+s3 > s1 then verify B
		j fail			#else exists
		
		
		
	verifyB:sub $t0, $s1, $s3	#t0 = a-c
		bltz $t0, invertB	#if a-c < 0 then invertB
		blt $t0, $s2, checkB	#if a-c < b then checkB
		j fail			#else exists
		
	invertB:add $t1, $zero, $s1	#t1 = a
		add $s1, $zero, $s3	#a = c
		add $s3, $zero, $t1	#c = t1
		j verifyB		#jumps to verifyB
		
	checkB: add $t1, $s1, $s3	#t1 = s1 + s3 -> a+c
		add $s0, $s0, 1		#increments s0
		bgt $t1, $s2, verifyC	#if s1+s3 > s2 then verifyC 
		j fail			#else exists
	
		
		
	verifyC:sub $t0, $s1, $s2	#t0 = a-b
		bltz $t0, invertC	#if a-b < 0 then invertC
		blt $t0, $s3, checkC	#if a-b < c then checkC
		j fail			#else exists
		
	invertC:add $t1, $zero, $s1	#t1 = a
		add $s1, $zero, $s2	#a = b
		add $s2, $zero, $t1	#b = t1
		j verifyC		#jumps to verifyC
		
	checkC: add $t1, $s1, $s2	#t1 = s1 + s2 -> a+b
		bgt $t1, $s3, success	#if s1+s2 > s3 then it is a triangulo! Jumps to success.
		j fail			#else exists
		
	fail:	li $v0, 4	#Prints fail message
		la $a0, prompt_e
		syscall
		j exit
		
	success:li $v0, 4	#Prints success message
		la $a0, prompt_r
		syscall
		
	exit:	li $v0, 10	#Exits
		syscall
	