# Title: Perfect Number		Filename: ex8_nperfect.asm
#Author: Gabriel Edmilson P.	Date: 11/14/2016
#Description: We say that a positive integer number is perfect if it is equal to the sum of your positive divisors different of n.
#Input:  Positive integer n
#Output: Whereas n is perfect or not

#Notes: If you look at the heap after(or at the) execution you will can see the divisors! That's amazing.

.data	
	intro:		.asciiz "\n\t::Perfect Number::\n"
	prompt_n:	.asciiz "\nEnter with the number: "
	prompt_ne:	.asciiz "Please, enter with a integer positive number: "
	is_perfect:	.asciiz "\n(True) Yeah, it is a perfect number! (True)\n"
	not_perfect:	.asciiz "\n(False) Oh, it is not a perfect number. (False)\n"
	
	
.text 
.globl main

main:		#Presents an introduction to the user
		li $v0, 4
		la $a0, intro
		syscall
		
		#Asks for the number and set it to $s0
		li $v0, 4
		la $a0, prompt_n
		syscall
read_n:		li $v0, 5
		syscall
		move $s0, $v0
		
		#Branches to 'experfect' to verify its "perfectness",
		# if the given number is greater than zero
		bgtz $s0, experfect
		
		#Else, we politely force the user to set a valid number!
		#Present a error message and request a positive integer:
		#After jumps to 'read_n' where the number is read.
		li $v0, 4
		la $a0, prompt_ne
		syscall
		j read_n
		
experfect:	#Verify if the number is perfect
		#It is needed to check all the numbers from 1 to n/2 to see if they are divisors,
		# so.. lets allocate such space in memory:
		
		div $t0, $s0, 2		#t0 = n/2 integer
		mul $t0, $t0, 4		#t0 = n/2 in bytes, which is the required space for allocation
		li $v0, 9		#9 indicates allocation
		move $a0, $t0		#a0 keeps the number of bytes allocated
		syscall			#v0[a0]
		
		move $s1, $v0		#s1 = address of allocated memory []
		move $s2, $t0		#s2 = array size in bytes
		
#Until now we have:
#	-> $s0 => n
#	-> $s1 => [] address of the vector of divisors
#	-> $s2 => array size in bytes
		
		addi $t0, $s1, 0	#t0 => array[0]
		addi $t1, $zero, 0	#t1 => i = 0
		
loop_mod:	#loop to identify the divisors
		addi $t1, $t1, 1	#i++
		beq $t1, $s0, sum	#if i == n then stop looping and go to the sum
		rem $t2, $s0, $t1	#else t2 = n mod i
		bnez $t2, loop_mod	#if n mod i != 0 then trys next
		sw $t1, ($t0)		#else array[id] = i
		addi $t0, $t0, 4	#id++
		j loop_mod		#try next iteration		
		
sum:		#Sums the divisors to see if them are equal to n
		add $t1, $zero, $s1	#t1 = address divisors array []
		add $t2, $t0, $zero 	#t2 = address divisors array used size
		addi $s3, $zero, 0	#s3 = sum, starts with zero

sum_loop:	lw $t3, ($t1)		#t3 = array[i]
		add $s3, $s3, $t3	#sum += array[i]
		addi $t1, $t1, 4	#t1 -> array[i+1]
		
		ble $t1, $t2, sum_loop	#if array[i] <= array[size] then continues the sum
		
		#else
		
		beq $s3, $s0, perfect	#if sum == n then it is perfect
		
notperfect:	li $v0, 4		#Prints that the number is not perfect and jumps to EXIT
		la $a0, not_perfect
		syscall
		
		j EXIT
		
perfect:	li $v0, 4
		la $a0, is_perfect
		syscall
		
EXIT:		li $v0, 10
		syscall	