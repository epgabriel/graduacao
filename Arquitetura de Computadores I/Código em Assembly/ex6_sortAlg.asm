# Title: Sort Procedure		Filename: ex6_sortAlg.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: Implement the requested algorithm
#Input:  Five integer numbers
#Output: Sorted elements

############### Data segment ###############
.data
	prompt_n:	.asciiz "Please, enter the five numbers:\n"
	prompt_r:	.asciiz "\nThe sorted elements: "
	str_space:	.asciiz " "
	str_endl:	.asciiz "\n"
	array:		.space 20
############### Code segment ###############
.text
.globl main


main:	addi $s0, $zero, 5 #s0 -> n

	li $v0, 4
	la $a0, prompt_n
	syscall
	
	addi $t0, $zero, 0
	
read:	mul $t1, $t0, 4		#reads the five elements
	
	li $v0, 5
	syscall
	move $t2, $v0
			
	sw $t2, array($t1)
	
	addi $t0, $t0, 1
	blt $t0, $s0, read
	
sort:	addi $t0, $zero, -1		# int i = -1
	addi $s1, $s0, -1
loop1:	beq  $t0, $s1, printing		#if i == n then print the elements
	addi $t0, $t0, 1		#i++
	addi $t1, $t0, -1		#j=i-1

	loop2:	bltz $t1, loop1		#if j < 0 then break
		mul  $t2, $t1, 4	#t2 = j in bytes
		addi $t3, $t1, 1	#t3 = j+1
		mul  $t3, $t3, 4	#t3 = j+1 in bytes
		
		lw $t4, array($t2)	#t4 = vet[j]
		lw $t5, array($t3)	#t5 = vet[j+1]
		
		bgt $t4, $t5, swap	#if vet[j] > vet[j+1] then swap
		
	L2:	addi $t1, $t1, -1	#j--
		j loop2			#next iteration of loop2


swap:	sw $t4, array($t3)		#&vet[j+1] = vet[j]
	sw $t5, array($t2)		#&vet[j] = vet[j+1]
	j L2				
	
printing:
	addi $t0, $zero, 0		#i = 0
P1:	mul $t1, $t0, 4			
	lw $t1, array($t1)
	
	li $v0, 1
	move $a0, $t1
	syscall
	
	li $v0, 4
	la $a0, str_space
	syscall
	
	addi $t0, $t0, 1
	
	blt $t0, $s0, P1
	
EXIT:	li $v0, 10
	syscall	
	