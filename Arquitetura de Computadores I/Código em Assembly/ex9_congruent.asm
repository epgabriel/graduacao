# Title: Congruent		Filename: ex9_congruent.asm
#Author: Gabriel Edmilson P.	Date: 11/14/2016
#Description: We say that a number 'i' is congruent 'mod m' to 'j' if 'i mod m' is equal to 'j mod m'
#Input:  Positive integer n, j, m
#Output: The firsts n numbers congruents to "j mod m"

.data
	intro:		.asciiz	"\n\t::Congruence::\n"
	prompt_n:	.asciiz "\nPlease, enter the positive integer n: "
	prompt_j:	.asciiz "\nPlease, enter the positive integer j: "
	prompt_m:	.asciiz "\nPlease, enter the positive integer m: "
	prompt_err:	.asciiz	"Please! Enter with a positive integer:  "
	result:		.asciiz "\nThe congruents: "
	e_result:	.asciiz "Ops, there are no congruent numbers\n"
	str_space:	.asciiz " "
	str_endl:	.asciiz	"\n"
	
.text
.globl main

main:		li $v0, 4		#Presents the program's title
		la $a0, intro	
		syscall
		
		li $v0, 4		#Presents the request for the n
		la $a0, prompt_n
		syscall
		
read_n:		li $v0, 5		#Reads the n
		syscall
		move $s0, $v0
		
		bgt $s0, 0, n_readed	#if n > 0 then n is readed
		
		li $v0, 4		#else then prints a message and read n again
		la $a0, prompt_err
		syscall
		j read_n

n_readed:	li $v0, 4		#n is now readed and well set, then it's time to read 'j'
		la $a0, prompt_j
		syscall
		
read_j:		li $v0, 5
		syscall
		move $s1, $v0
		
		bgt $s1, 0, j_readed	#if j > 0 then j is readed
		
		li $v0, 4		#else then prints a message and read n again
		la $a0, prompt_err
		syscall
		j read_j
		
j_readed:	li $v0, 4		#j is now readed and well set, then it's time to read 'm'
		la $a0, prompt_m
		syscall
		
read_m:		li $v0, 5
		syscall
		move $s2, $v0	
		
		bgt $s2, 0, m_readed	#if m > 0 then it is time to start the congruence process
		
		li $v0, 4
		la $a0, prompt_err
		syscall
		j read_m
		
m_readed:	#Until now we have:
#		  s0 -> n
#		  s1 -> j
#		  s2 -> m
#		  So, it is time to go ahead and start to check the numbers
		
		rem  $s3, $s1, $s2	#s3 = j mod m
		add  $s4, $zero, 0	#s4 = used bytes
		
		div  $t0, $s0, 2	#t0 -> n / 2
		mul  $t0, $t0, 4	#t0 -> n/2 in bytes
		
		li $v0, 9		#v0[]
		move $a0, $t0		#a0 -> allocate space (n/2 spaces) 
		syscall
		
		move $s5, $v0		#s5 -> congruent's array[]
		move $s6, $a0		#s6 -> max size in bytes
		
		addi $t0, $zero, 0	#t0 -> i, lets start it
		#div  $t3, $s0, 2	#t3 -> n/2
		
congruence:	addi $t0, $t0, 1		#t0++
		bgt  $t0, $s0, print_c		#if i == n then branches to the print stage

#		else
		rem  $t1, $t0, $s2		#t1 = i mod m
		bne  $t1, $s3, congruence	#if i mod m != j mod m then branches to the next iteration
		
#		else
		add $t1, $s5, $s4	#t1 = [] + used bytes
		sw $t0, ($t1)		#stores i in array_congruence[id]
		addi $s4, $s4, 4	#s4++ (there is one more element congruent)
		j congruence

#		Prints the congruent elements				

print_c:	move $t0, $s5		#t0 = congruent's array[]
		li $v0, 4		#prints message informing thtat the elements will be printed
		la $a0, result
		syscall
		
		add $t5, $s5, $s4	#t5 = [] + bytes used, which is the max size the loop must achieve	
		
P1:		lw $t1, ($t0)		#loop, t1 = array[i]
		li $v0, 1		#prints it
		move $a0, $t1
		syscall
		
		li $v0, 4		#prints a space " "
		la $a0, str_space
		syscall
		
		addi $t0, $t0, 4	#t0++ (next byte id)
		
		blt $t0, $t5, P1	#if id < max size then prints another
		

EXIT:		li $v0, 4		#Prints a line break and then exits
		la $a0, str_endl
		syscall

		li $v0, 10
		syscall
		
