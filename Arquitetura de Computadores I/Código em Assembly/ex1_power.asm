# Title: Potencia n m		Filename: ex1_power.asm
#Author: Gabriel Edmilson P.	Date: 11/13/2016
#Description: Calculates  m^n
#Input:  Integers 'm' and 'n'
#Output: Power of m raised to n Pow(m,n)

.data
	prompt_header:	.asciiz	"\n\t:::This program calculates m power n:::\n"
	prompt_m:	.asciiz	"Enter the m value: "
	prompt_n:	.asciiz	"Enter the n value: "
	prompt_result:	.asciiz "\n\n\tP(m,n): "
	
	
.text

#s0 -> m
#s1 -> n

.globl main

main:	li $v0, 4	#Prints what the program does
	la $a0, prompt_header
	syscall
	
	li $v0, 4	#Prints the request for m value
	la $a0, prompt_m
	syscall
	
	li $v0, 5	#Gets m
	syscall
	move $s0, $v0	#s0 = m

	li $v0, 4	#Prints the request for n value
	la $a0, prompt_n
	syscall
	
	li $v0, 5	#Gets n
	syscall
	move $s1, $v0	#s1 = n
	
	li $v0, 4		#Prints the result string
	la $a0, prompt_result
	syscall
	
	beq  $s1, 0, prints_1	#expoente (n) == 0,go to prints_1
	beq  $s1, 1, prints_m	#expoente (n) == 1,prints m
	
	add $s2, $zero, $s0	#s2 = m, will be the final result later

power:	mul $s2, $s2, $s0	#s2 *= m
	addi $s1, $s1, -1	#n--
	bgt $s1, 1, power
	
	li $v0, 1		#Prints the m power n
	move $a0, $s2
	syscall
	
	j EXIT
	
prints_1:		#Prints 1, n == 0
	li $v0, 1
	addi $a0, $zero, 1
	syscall
	
	j EXIT

prints_m:		#Prints m, n == 1
	li $v0, 1
	move $a0, $s0
	syscall
	
EXIT:	li $v0, 10
	syscall		
