#include <stdio.h>
#include <stdlib.h>

typedef struct Pessoa {
	int idade;
}Pessoa;

typedef struct Nodo {
	struct Pessoa pessoa;
	struct Nodo *pProx;
}Nodo;

struct Nodo *topo = NULL;

void push (struct Nodo *nodo ) {
	nodo->pProx = topo;
	topo = nodo;
}

struct Nodo* pop() {
	if(topo!= NULL){
		struct Nodo *aux;
		aux = topo;
		topo = topo->pProx;
		return aux; 
	}else {
		return NULL;
	}
}
//imprimir(pop());
void Imprimir(struct Nodo *nodo) {
	if(nodo!=NULL){
		printf("%d\n",nodo->pessoa.idade);
	}
	else{
		printf("Pilha vazia\n");
	}
	
	
}
