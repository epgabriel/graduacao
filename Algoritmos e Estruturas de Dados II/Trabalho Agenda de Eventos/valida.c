#include <stdio.h>
#include "valida.h"

void validaOpcao(int *op, int limite){
	while(*op<0 || *op>limite){
		printf("Erro! Digite uma opção válida: ");
		scanf("%d", op);
	}
}

void validaMes(int *mes){ //função que avalia se o mês é válido
	while(*mes < 1 || *mes > 12){
		printf("Erro! Informe um mês válido: ");
		scanf("%d", mes);
	}
	
}

void validaDia(struct Evento *pEvento){ //função que avalia se o dia é válido
	if(pEvento->dia > 28 || pEvento->dia < 1){ //Todo mês tem pelo menos vinte e oito dias
		
		int qntDias;
		
		if(pEvento->mes <= 7){ //Se o mês está entre Janeiro a Julho então:
			if(pEvento->mes == 2){ //Se for Fevereiro:
				qntDias = 28;
			}
			else if(pEvento->mes%2 != 0){ //Janeiro, Março, Maio, Julho
				qntDias = 31;
			}
			else{ //Abril, Junho
				qntDias = 30;
			}
		}else{ //Se o mês está entre Agosto a Dezembro
			if(pEvento->mes%2 != 0){ //Setembro e Novembro
				qntDias = 30;
			}
			else{ //Agosto, Outubro e Dezembro
				qntDias = 31;
			}
		}
		
		//Enquanto o dia for inferior a 1 ou superior ao limite de dias que o mês possui...
		while(pEvento->dia < 1 || pEvento->dia > qntDias){
			printf("Erro! Informe um dia entre 1 e %d: ",qntDias);
			scanf("%d",&pEvento->dia);
		}
	}	
}
