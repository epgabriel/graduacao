#include <stdlib.h>
#include "evento.h"
#include "valida.h"
#include "auxiliar.h"
#include "relatorio.h"
#include "estrutura.h"					

int main(){
	inicializaHash();
	
	int op;
	int qntEventos=0;
	do{
		verificarOpcao(&op);
		
		if(op == 1){
			struct Evento *pNodo = malloc(sizeof(struct Evento));
			preencherEvento(pNodo);
			inserirHash(pNodo);
			qntEventos++;
		}
		
		else if(op == 2){
			FILE *arq;
			if((arq=fopen("entrada.txt","r")) == NULL){
				printf("\nErro ao achar arquivo!\n\n");
			}else{
				while(!feof(arq)){
					struct Evento *pNodo = malloc(sizeof(struct Evento));
					preencherEventoArquivo(arq,pNodo);
					inserirHash(pNodo);
					qntEventos++;
				}
			}
			fclose(arq);
		}
		
		else if(op == 3){
			int mes;
			char cod[5];
			printf("\nMês do evento: ");
			scanf("%d",&mes);
			validaMes(&mes);
			if(pHash[mes-1] != NULL){
				printf("\nCódigo do evento: ");
				scanf("%s",cod);
				pesquisarEvento(&qntEventos,mes, cod);
			}	
		}
		
		else if(op == 4){
			imprimirAgenda();
		}		
	}while(op!=0);
	
	relatorioOrdenadoPorData();
	relatorioTarefasNaoRealizadas();
	relatorioEventosOrdenadosPorNome(qntEventos);
	apagarAgenda();
	
	return 0;
}

