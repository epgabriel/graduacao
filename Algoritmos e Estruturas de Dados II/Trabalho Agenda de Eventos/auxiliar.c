#include <stdio.h>
#include <stdlib.h>
#include "auxiliar.h"
#include "valida.h"
#include "evento.h"

void inicializaHash(){
	int i=0;
	for(;i<12;i++){
		pHash[i] = NULL;
	}
}

void verificarOpcao(int *op){
	/*verifica se o usuário deseja manipular eventos
	 *de forma manual ou automática (através de arquivos)
	 * ou se deseja encerrar o programa
	 */ 
	printf("\nMenu Principal"
	"\nDigite '1' para cadastrar manualmente os eventos"
	"\nDigite '2' para cadastrar os eventos usando um arquivo"
	"\nDigite '3' para pesquisar um evento específico"
	"\nDigite '4' para imprimir eventos"
	"\nDigite '0' para encerrar o programa"
	"\n\tOpção: ");
	scanf("%d", op);
	validaOpcao(op, 4);
}

void apagarAgenda(){
	printf("\n\nDesalocando eventos ... \n");
	int i=0;
	for(; i<12; i++){
		if(pHash[i] != NULL){
			struct Evento *pAux;
			while(pHash[i] != NULL){
				pAux = pHash[i];
				pHash[i] = pHash[i]->pProximo;
				free(pAux);
			}
		}
	}
	printf("\n\t... Operação realizada com sucesso!\n");
}

void imprimirAgenda(){
	printf("\n\n");
	struct Evento *pAux;
	int i=0;
	for(; i<12; i++){
		printf("\n_______________________________________________");
		printf("\n\nEventos do mês de %d:\n",i+1);
		if(pHash[i] == NULL){
			printf("Não há eventos nesse mês");
		}
		else{
			pAux = pHash[i];
			while(pAux != NULL){
				imprimirEvento(pAux);
				pAux = pAux->pProximo;
			}
		}
	}
	printf("\n\n");
}
