#include <stdio.h>
#include "estrutura.h"

#ifndef EVENTO_H
#define EVENTO_H

void preencherEvento(struct Evento*);
void preencherEventoArquivo(FILE*,struct Evento *);
void inserirHash(struct Evento*);
void imprimirEvento(struct Evento*);
void pesquisarEvento(int*,int,char*);

#endif
