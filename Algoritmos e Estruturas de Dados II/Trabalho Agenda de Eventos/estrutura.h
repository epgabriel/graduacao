#ifndef ESTRUTURA_H
#define ESTRUTURA_H


struct Fila{
	char task[200];
};

struct Evento{
	char nome[50];
	char codigo[5];
	
	int dia,mes;
	
	int filaInicio;
	int filaFinal;
	struct Fila fila[5];
	
	struct Evento *pProximo;
};

struct Evento *pHash[12];

#endif
