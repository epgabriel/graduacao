#include <string.h>
#include <stdlib.h>
#include "evento.h"
#include "valida.h"

void preencherEvento(struct Evento *pNodo){
	printf("\nNome do evento: ");
	scanf("%*c%[^\n]", pNodo->nome);
	
	printf("\nCódigo: ");
	scanf("%s", pNodo->codigo);
	
	int mes;
	printf("\nMês: ");
	scanf("%d", &mes);
	validaMes(&mes);
	pNodo->mes = mes;
	
	
	printf("\nDia: ");
	scanf("%d", &pNodo->dia);
	validaDia(pNodo);
	//Tarefas:
	printf("\nTarefa(s):\n\n");
	pNodo->filaInicio = 0;
	pNodo->filaFinal = 0; //primeira tarea (0) é lida obrigatoriamente
	
	int op;
	do{
		printf("%dª tarefa:\n\t", pNodo->filaFinal+1);
		scanf("%*c%[^\n]", pNodo->fila[pNodo->filaFinal].task);
		pNodo->filaFinal++;
		
		if(pNodo->filaFinal < 5){
			printf("\n\nDigite '1' para inserir outra tarefa\n"
			"Digite '0' para retornar ao menu principal\n"
			"Opção: ");
			scanf("%d",&op);
			validaOpcao(&op,1);
		}
	}while(op==1&&pNodo->filaFinal<5);
}

void preencherEventoArquivo(FILE *arq, struct Evento *pNodo){
	fscanf(arq,"%*c%[^\n]", pNodo->nome); //Nome do Evento
	fscanf(arq,"%s", pNodo->codigo); 	 //Codigo
	fscanf(arq,"%d", &pNodo->mes); 		//Mes
	fscanf(arq,"%d", &pNodo->dia); 	   //Dia
	
	//Tarefas:
	pNodo->filaInicio = 0;
	pNodo->filaFinal = 0; //primeira tarea (0) é lida obrigatoriamente

	int op;
	do{
		fscanf(arq,"%*c%[^\n]", pNodo->fila[pNodo->filaFinal].task);
		pNodo->filaFinal++;
		
		if(pNodo->filaFinal < 5){
			fscanf(arq,"%d",&op);
		}
	}while(op==1&&pNodo->filaFinal<5);
}

void inserirHash(struct Evento *pNodo){
	if(pHash[pNodo->mes-1] == NULL || pHash[pNodo->mes-1]->dia > pNodo->dia){ //Se a posicao no Hash[pNodo->mes-1] tiver vazia
		pNodo->pProximo = pHash[pNodo->mes-1];
		pHash[pNodo->mes-1] = pNodo;
	}
	else{
		struct Evento *pAnterior = pHash[pNodo->mes-1];
		struct Evento *pAtual = pHash[pNodo->mes-1]->pProximo;
		
		while(pAtual != NULL && pAtual->dia < pNodo->dia){
			pAnterior = pAtual;
			pAtual = pAtual->pProximo;
		}
		
		pAnterior->pProximo = pNodo;
		pNodo->pProximo = pAtual;
	}	
}

void imprimirEvento(struct Evento *pNodo){
	printf("\n\t--------------------------------------");
	printf("\n\tNome: %s"
	"\n\tCódigo: %s\tData: %d/%d/2015",
	pNodo->nome, pNodo->codigo, pNodo->dia, pNodo->mes);
	
	int i = pNodo->filaInicio;
	if(i < pNodo->filaFinal){
		printf("\n  Tarefas:\n\t%d tarefa(s) já concluída(s)\n", i);
		for(;i<pNodo->filaFinal;i++){
			printf("\n\t%dª\t%s",(i+1),pNodo->fila[i].task);
		}
	}	
	printf("\n\n");
}

void pesquisarEvento(int *qntEventos, int mes, char *cod){
	struct Evento *pAtual = pHash[mes-1];
	struct Evento *pAnterior = NULL;
	
	while(pAtual != NULL && strcmp(pAtual->codigo, cod) != 0){
		pAnterior = pAtual;
		pAtual = pAtual->pProximo;
	}
	
	if(pAtual != NULL){
		imprimirEvento(pAtual);
		
		int op;
		do{
			printf("\nDigite '1' para remover tarefa concluída"
			"\nDigite '0' para retornar ao Menu Principal"
			"\n\tOpcao: ");
			scanf("%d",&op);
			validaOpcao(&op,1);
			if(op == 1){
				pAtual->filaInicio++;
			}
			imprimirEvento(pAtual);
		}while(op != 0 && pAtual->filaInicio < pAtual->filaFinal);
		
		if(pAtual->filaInicio == pAtual->filaFinal){
			printf("\n\n\tTodas as tarefas foram concluídas...\n"
			"\t  ...iniciando processo de exclusão do evento...");
			if(pAnterior == NULL){
				pHash[mes-1] = pAtual->pProximo;
			}else{
				pAnterior->pProximo = pAtual->pProximo;
			}
			free(pAtual);
			printf("\n Evento excluido com sucesso!\n\n");
			*qntEventos -= 1;
		}
	}else{
		printf("\nEvento não encontrado!\n");
	}
}
	
	
