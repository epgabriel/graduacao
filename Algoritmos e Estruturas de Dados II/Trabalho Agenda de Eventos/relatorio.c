#include <stdio.h>
#include <string.h>
#include "valida.h"

void relatorioOrdenadoPorData(){
	printf("\n\nOrdenando eventos por data...\n ... gerando relatórios...");
	
	struct Evento *pAux;
	FILE *arq = fopen("RelatorioDataSort.txt","w+");
	fprintf(arq,"\n_____________________________________\n");
	fprintf(arq,"Relatório de Eventos Ordenados por Data");
	int i=0;
	for(; i<12; i++){
		fprintf(arq,"\n_______________________________________________");
		fprintf(arq,"\n\nEventos do mês de %d:\n",i+1);
		if(pHash[i] == NULL){
			fprintf(arq,"Não há eventos neste mês");
		}
		else{
			pAux = pHash[i];
			while(pAux != NULL){
				fprintf(arq,"\n\t--------------------------------------");
				fprintf(arq,"\n\tNome: %s"
				"\n\tCódigo: %s\tData: %d/%d/2015",
				pAux->nome, pAux->codigo, pAux->dia, pAux->mes);
				
				fprintf(arq,"\nTarefas:\n");
				fprintf(arq,"\tTarefas concluídas:\n");
				int j=0;
				for(;j<pAux->filaInicio;j++){
					fprintf(arq,"\n\t%d - \t%s",(j+1),pAux->fila[j].task);
				}
				j = pAux->filaInicio;
				fprintf(arq,"\n\n\tTarefas não concluídas:\n");
				for(;j<pAux->filaFinal;j++){
					fprintf(arq,"\n\t%d - \t%s",(j+1),pAux->fila[j].task);
				}
				
				pAux = pAux->pProximo;
			}
		}
	}

	fclose(arq);
	printf("\n\t...RELATÓRIO GERADO COM SUCESSO!!!\n");
}

void relatorioTarefasNaoRealizadas(){
	printf("\n\nCalculando tarefas não realizadas...\n ... gerando relatório...");
	struct Evento *pAux;
	FILE *arq = fopen("RelatorioTarefasPendentes.txt","w+");
	fprintf(arq,"\n_____________________________________\n");
	fprintf(arq,"Relatório de Tarefas não realizadas por Evento");
	int i=0;
	for(; i<12; i++){
		fprintf(arq,"\n_______________________________________________");
		fprintf(arq,"\n\nEventos do mês de %d:\n",i+1);
		if(pHash[i] == NULL){
			fprintf(arq,"Não há eventos neste mês");
		}
		else{
			pAux = pHash[i];
			while(pAux != NULL){
				fprintf(arq,"\n\n\t--------------------------------------");
				fprintf(arq,"\n\tNome: %s"
				"\n\tCódigo: %s\tData: %d/%d/2015\n",
				pAux->nome, pAux->codigo, pAux->dia, pAux->mes);
				
				int j = pAux->filaInicio;
				fprintf(arq,"\n\tTarefas não concluídas:\n");
				for(;j<pAux->filaFinal;j++){
					fprintf(arq,"\n\t%d - \t%s",(j+1),pAux->fila[j].task);
				}
				
				pAux = pAux->pProximo;
			}
		}
	}

	fclose(arq);
	printf("\n\t...RELATÓRIO GERADO COM SUCESSO!!!\n");
}

void relatorioEventosOrdenadosPorNome(int qnt){
	printf("\n\nOrdenando eventos por nome...\n ... gerando relatório...");
	struct Evento eventos[qnt];
	
	
	int i=0,j=0;
	for(;j<12;j++){
		if(pHash[j] != NULL){
			struct Evento *pAux=pHash[j];
			while(pAux != NULL){
				eventos[i++] = *pAux;
				pAux = pAux->pProximo;
			}
		}
	}	
	
	int a;
	do{
		a=0;
		
		for(i=0;i<qnt-2;i++){
			if((strcmp(eventos[i].nome,eventos[i+1].nome)) > 0){
				struct Evento aux = eventos[i];
				eventos[i] = eventos[i+1];
				eventos[i+1] = aux;
				a = 1;
			}
		}
	}while(a==1);
	
	FILE *arq = fopen("RelatorioNameSort.txt","w+");
	fprintf(arq,"\n\tRelatório dos Eventos em Ordem Alfabética\n");
	for(i=0; i<qnt-1; i++){
		fprintf(arq,"\n--------------------------------------------\n");
		fprintf(arq,"Evento: %s\nCódigo: %s\tData: %d/%d/2015\n",
		eventos[i].nome,eventos[i].codigo,eventos[i].dia,eventos[i].mes);
		fprintf(arq,"\nTarefas:\nTarefas Concluídas:\n");
		for(j=0;j<eventos[i].filaInicio;j++){
			fprintf(arq,"\n\t%d - %s",j+1,eventos[i].fila[j].task);
		}
		fprintf(arq,"\nTarefas:\nTarefas não concluídas:\n");
		for(j=eventos[i].filaInicio; j<eventos[i].filaFinal; j++){
			fprintf(arq,"\n\t%d - %s",j+1,eventos[i].fila[j].task);
		}
	}
	fclose(arq);
	printf("\n\t...RELATÓRIO GERADO COM SUCESSO!!!\n");
}
