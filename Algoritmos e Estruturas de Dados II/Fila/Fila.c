#include <stdio.h>
#include <stdlib.h>
#include "Fila.h"

struct Nodo* pInicio = NULL;
struct Nodo* pFinal = NULL;

int main(){
	
	int op;
	
	do{
		scanf("%d",&op);
		if(op==1) enqueue();
		else if(op==2) denqueue();
		else if(op==3) imprimir();
		else destruir();
		
	}while(op!=0);

	return 0;
}


void enqueue(){
	struct Nodo* pNodo = malloc(sizeof(struct Nodo));
	
	printf("\n Digite o nome :  ");
	scanf("%*c%[^\n]",pNodo->pessoa.nome);
	
	printf("\n Digite a idade :  ");
	scanf("%d",&pNodo->pessoa.idade);
	
	printf("\n Digite o codigo :  ");
	scanf("%d",&pNodo->pessoa.codigo);
	
	if(pInicio==NULL){
		pInicio=pNodo;
		pFinal=pNodo;
		pNodo->pProximo=NULL;
	}else{
		pFinal->pProximo=pNodo;
		pFinal=pNodo;
	}
}

void denqueue(){
	if(pInicio==NULL){
		printf("\nErro! Fila vazia!\n");
	}else{
		struct Nodo* pAux = pInicio;
		pInicio=pInicio->pProximo;
		free(pAux);
	}
}

void imprimir(){
	if(pInicio==NULL){
		printf("\nErro! Fila vazia!\n");
	}else{
		struct Nodo* pAux = pInicio;
		while(pAux!=NULL){
			printf("\n\n\t%s\n%d\n%d\n",pAux->pessoa.nome,pAux->pessoa.idade,
			pAux->pessoa.codigo);
			pAux=pAux->pProximo;
		}
	}
}

void destruir(){
	 struct Nodo* pAux = pInicio;
	 while(pInicio!=NULL){
		 pAux =pInicio;
		 pInicio=pInicio->pProximo;
		 free(pAux);
	 }
 }
		
		
	
			

		
		
	
