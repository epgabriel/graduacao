#include <stdio.h>
#include <stdlib.h>
#include "HeadPilha.h"

struct Nodo *topo = NULL;

int main(){
	printf("------------Pilha-----------\n\n");
	int op;
	do{
		lerOpcao(&op);
		
		switch(op){
		case 1:
			push();
			break;
			
		case 2:
			pop();
			break;
			
		case 3:
			imprimir();
			break;
		}
	}while(op != 0);
	destruir();
	return 0;
}

void lerOpcao(int *op){
	printf("\nMenu:\n"
		   "1 - Colocar elementos na Pilha\n"
		   "2 - Retirar elementos na Pilha\n"
		   "3 - Imprimir\n"
		   "0 - Sair\n"
		   "\tOpcao: ");
		   
	scanf("%d", op);
	
	while(*op<0 || *op>3){
		printf("Digite uma OPCAO VALIDA: ");
		scanf("%d", op);
	}
}
	
		

void push(){
	struct Nodo *nodo = malloc(sizeof(struct Nodo));
		
	preencherNodo(nodo);
	
	if(topo == NULL){
		topo = nodo;
		nodo->p = NULL;
	}else{
		nodo->p = topo;
		topo = nodo;
	}
		
}
	
void preencherNodo(struct Nodo *nodo){
	
	printf("\nNome: ");
	scanf("%*c%[^\n]", nodo->pessoa.nome);
	
	printf("Idade: ");
	scanf("%d%*c", &nodo->pessoa.idade);
	
	printf("Codigo: ");
	scanf("%d%*c", &nodo->pessoa.cod);
}

void pop(){
	if(topo == NULL){
		printf("\nERRO! PILHA VAZIA!!\n");
	}else{
		struct Nodo *aux = topo;
		topo = topo->p;			
		if(aux != NULL){
			free(aux);
		}
	}	
}

void imprimir(){
	if(topo == NULL){
		printf("\nERRO! PILHA VAZIA!!\n");
	}else{
		struct Nodo *aux = topo;
		while(aux!=NULL){
			printf("\n\n\tNome: %s\n"
				   "\tIdade: %d\n"
				   "\tCodigo: %d\n\n"
				   ,aux->pessoa.nome, aux->pessoa.idade
				   ,aux->pessoa.cod);
			aux = aux->p;
		}
	}
}

void destruir(){
	printf("\n\n\tBye bye =[\n");
	struct Nodo *aux = topo;
	while(aux != NULL){
		free(aux);
		aux = aux->p;
	}
}

