#include <stdio.h>
#include <stdlib.h>

struct Pessoa{
	char nome[30];
	int idade;
	char sexo;
};

struct Nodo{
	struct Pessoa pessoa;
	struct Nodo *pProximo;
};

struct Nodo *pInicio = NULL;

void push();
void pop();
void imprimir();


int main(){
	int cod = 0;
	do{
		printf("\nDigite '1' para inserir itens na Pilha"
			   "\nDigite '2' para remover itens na Pilha"
			   "\nDigite '3' para imprimir a Pilha"
			   "\nDigite '0' para sair"
			   "\nOpção: ");
		scanf("%d", &cod);
		while(cod < 0 || cod > 3){
			printf("Erro! Digite uma opção válida: ");
			scanf("%d", &cod);
		}
		
		switch(cod){
			case 1: push();
			break;
			case 2: pop();
			break;
			case 3: //imprimir();
			break;
		}
	}while(cod != 0);
	
	return 0;
}

void push(){
	struct Nodo *pNodo = malloc(sizeof(struct Nodo));
	printf("Digite o nome: ");
	scanf("%s%*c", pNodo->pessoa.nome);
	printf("Digite o sexo: ");
	scanf("%c%*c", &pNodo->pessoa.sexo);
	printf("Digite a idade: ");
	scanf("%d%*c", &pNodo->pessoa.idade);
	
	if(pInicio == NULL){
		pNodo->pProximo = pInicio;
		pInicio = pNodo;
	}else{
		
}

void pop(){
	if(pInicio == NULL){
		printf("A pilha está vazia\n");
	}else{
		struct Nodo *pAux = pInicio;
		pInicio = pAux->pProximo;
	}
}
