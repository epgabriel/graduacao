struct Pessoa{
	char nome[30];
	int idade;
	int cod;
};

struct Nodo{
	struct Pessoa pessoa;
	struct Nodo *p;
};


void lerOpcao(int*);

void push();
void preencherNodo(struct Nodo*);

void pop();

void imprimir();
void destruir();
