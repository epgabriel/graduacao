/*Programa que realiza uma soma simples
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char** argv) {

    int x, y ;
    printf("Digite o primeiro numero: ");
    scanf("%d",&x);
    printf("Digite o segundo numero:  ");
    scanf("%d",&y);
    
    int soma;
    
    soma=x+y;
    
    printf("\n%d + %d = %d",x, y, soma);
    
    
    return (0);
}

