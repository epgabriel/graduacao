/*Programa que verifica ocorrencia de bingo numa dada cartela
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>
#include <stdlib.h>
    

/*
 * 
 */
int main(int argc, char** argv) {
    //ler arquivo
    FILE *fp;
    int i=0, j=0, bingo[5][5], sorteio[10];
    fp=fopen("teste.txt", "r");
    for(i=0; i<5;i++){
        for(j=0;j<5;j++){
        fscanf(fp,"%d", &bingo[i][j]);
        }
    }
    for(i=0;i<10;i++){
        fscanf(fp,"%d", &sorteio[i]);
                
    }
    //conferir se foi bingo horizontalmente
    int cont=0, k=0,chec=0;
    for (i=0;i<5;i++){
        for(j=0;j<5;j++){
            for(k=0;k<10;k++){
                if(sorteio[k]==bingo[i][j])cont++;
                }
            }
            if(cont==5){
                printf("bingo");
                chec++;
            }else{
                cont=0;
            }
        }
    //verticalmente
    for (i=0;i<5;i++){
        for(j=0;j<5;j++){
            for(k=0;k<10;k++){
                if(sorteio[k]==bingo[j][i])cont++;
                }
            }
            if(cont==5){
                printf("bingo");
                chec++;
            }else{
                cont=0;
            }
        }
    //diagonalmente
    for(i=0;i<5;i++){
        for(k=0;k<10;k++){
            if(sorteio[k]==bingo[i][i])cont++;
        }
    }
    if(cont==4){
        printf("bingo");
        chec++;
    }else{
        cont=0;
    }
    for(i=0;i<5;i++){
        for(j=4;j>0;j--){
            for(k=0;k<10;k++){
                if(sorteio[k]==bingo[i][j]) cont++;
            }
        }
    }
    if(cont==4){
        printf("bingo");
        chec++;
    }else{
        cont=0;
     }
    if(chec==0)printf("-1");
    return (EXIT_SUCCESS);
}
