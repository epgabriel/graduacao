/*Programa que imprime um vetor ao contrário
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>

int main(){
	int n;
	printf("Digite o tamanho do vetor: ");
	scanf("%d", &n);
	while(n<1){
		printf("Tamanho invalido, digite um valor valido: ");
		scanf("%d", &n);
	}
	int vet[n], i;
	
	for(i=0;i<n;i++){
		printf("Digite o %do elemento: ", i+1);
		scanf("%d", &vet[i]);
	}
	
	printf("\nVetor ao contrario: ");
	for(i=n-1; i>=0; i--){
		printf("%d ", vet[i]);
	}
	
	return 0;
}
