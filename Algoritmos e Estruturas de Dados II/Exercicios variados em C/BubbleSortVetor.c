/*Programa que ordena um vetor dado seu tamanho e seus valores
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>
#include <stdlib.h>

int main() {

    int x,i;
    printf("Digite o tamanho do vetor: ");
    scanf("%d",&x);
    while(x<1){
		printf("Tamanho inválido! Digite um tamanho válido: ");
		scanf("%d",&x);
	}
    int vet[x];
    
    
    for(i=0;i<x;i++){
		printf("Digite o %do elemento do vetor: ",i+1);
		scanf("%d",&vet[i]);
    }
    
    int b=1,aux; 
    
    do{
        b=1;
        
        for(i=0;i<x-1;i++)
        {
            if(vet[i]>vet[i+1])
            {
                aux=vet[i];
                vet[i]=vet[i+1];
                vet[i+1]=aux;
                        
                b=0; 
            }
        }
      }while(b!=1);
    
    
    printf("\nVetor ordenado:\n");
     for(i=0;i<x;i++){
		printf("%d ",vet[i]);
     }
    
    return (EXIT_SUCCESS);
}

