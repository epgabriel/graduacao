/*Programa que ordena uma matriz pelo metodo selection sort
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    
    int i, j, aux, n, menor;
    scanf("%d\n", &n);
    int vet[n];

    for(i = 0; i < n; i++){
        scanf("%d", &vet[i]);
    }

    for(i = 0; i < n; i++){
        menor = vet[i];
        for(j = i+1; j < n; j++){
            if(vet[j] < menor){
                menor = vet[j];
                aux = vet[i];
                vet[i] = vet[j];
                vet[j] = aux;
            }
          
        }
    }

    for(i = 0; i < n; i++){
        printf("%d ", vet[i]);
    }
    return (EXIT_SUCCESS);
}

