/*Programa que recebe o salario de um funcionario e o percentual
 * de aumento, calcule e mostre o valor do aumento e o novo salario
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>

int main(){
	float sal_f, perc_aum;
	float aum_valor, novo_sal;
	
	printf("Salario: ");
	scanf("%f", &sal_f);
	
	printf("Porcentagem de aumento: ");
	scanf("%f", &perc_aum);
	
	aum_valor = sal_f * perc_aum / 100;
	novo_sal = sal_f + aum_valor;
	
	printf("O valor do aumento foi de: %.2f\n"
	"O novo salario é: %.2f\n", aum_valor, novo_sal);
	
	 return 0;
 }
