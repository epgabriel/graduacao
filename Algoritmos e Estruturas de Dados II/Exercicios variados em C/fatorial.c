/*Codigo calcula fatoriais
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>

void calcularFatorial(int*);

int main(){
	printf("\t---Fatorial---\n\n");
	int num;
	printf("Calcular o fatorial de: ");
	scanf("%d", &num);
	
	printf("\nO fatorial de %d é...",num);
	calcularFatorial(&num);
	printf(" %d", num);
	return 0;
}

void calcularFatorial(int *f){
	int i;
	for(i=*f-1; i>0; i--){
		*f*=i;
	}
} 
