/*Programa que verifica ocorrencia de bingo numa dada cartela
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
 
 #include <stdio.h>
 
 int trocar(int*, int*);
 
 int main(){
	int n;
	printf("A matriz é quadrada, por favor digite o numero de linhas~colunas: ");
	scanf("%d", &n);
	while(n<1){
		printf("Erro! Digite um valor valido: ");
		scanf("%d", &n);
	}
	
	int mat[n][n], l, c;
	
	for(l=0; l<n; l++){
		for(c=0; c<n; c++){
			scanf("%d", &mat[l][c]);
		}
	}
	
	int troc;
	
	do {
		troc = 1;//false
		
		for(l=0; l<n-1; l++){
			if (mat[0][l] > mat[0][l+1]){
				troc = trocar(&mat[0][l], &mat[0][l+1]);
			}
			
			if (mat[n-1][l] > mat[n-1][l+1]){
				troc = trocar(&mat[n-1][l], &mat[n-1][l+1]);
			}
			
			if (mat[l][n-l-1] > mat[l+1][n-l-2]){
				troc = trocar(&mat[l][n-l-1], &mat[l+1][n-l-2]);
			}
		}
		
	}while (troc == 0); //true
	
	printf("\nMatriz Ordenada:\n");
	for (l=0; l<n; l++){
		for (c=0; c<n; c++){
			printf("%d ", mat[l][c]);
		}
		printf("\n");
	}
	
	return 0;
}

int trocar(int *A, int *B){
	int C = *A;
	*A = *B;
	*B = C;
	return 0;
}

