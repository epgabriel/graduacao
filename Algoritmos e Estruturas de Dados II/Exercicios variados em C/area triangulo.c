/*Programa que calcula a area de um triangulo
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
 #include <stdio.h>
 
 int main(){
	float base, altura;
	
	printf("Digite a base do triangulo:   ");
	scanf("%f", &base);
	
	printf("Digite a altura do triangulo: ");
	scanf("%f", &altura);
	
	printf("\nEsse triangulo tem base igual a: %.2f", (base*altura)/2);
	
	return 0;
 }
