/*Programa que realiza uma busca binaria
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */


#include <stdio.h>
int main(){

int n;
printf(" Digite um vetor para ser ordenado " );
scanf("%d",&n);

int vet[n],i;

for( i =0;i<n;i++){
	scanf(" %d", &vet[i]);
}
int cont = 0;
int aux ;
	do{
		cont = 0;
		
		for( i =0;i<n-1;i++){
			
				if(vet[i]>vet[i+1]){
					aux=vet[i];
					vet[i]=vet[i+1];
					vet[i+1]=aux;
					cont = 1;
				}
			}
		
	}while (cont );
	int busca ;
	
	for( i =0;i<n;i++){
	printf(" %d \n", vet[i]);
	}
	
	printf(" Digite o numero para ser buscado " );
	scanf("%d", &busca);
	
	int inicio = 0;
	int fim = n -1;
	int meio ;
	int pos = -1 ;
	
	do {
		meio = ( inicio + fim ) /2;
		
		if ( busca == vet[meio]){
			pos = meio;
		} else	if ( busca > meio ){
			inicio = meio + 1 ;
		}else if ( busca < meio ){
			fim = meio -1 ;
		}
	}while ( pos<0&&inicio<=fim );
	
	
	printf(" %d ", pos );	
		
			
		
		
	return 0;
}


