/* Programa que calcula a area de um circulo
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
 
 #include <stdio.h>
 #define PI 3.1415
 
 int main(){
	 float raio;
	 printf("Digite o raio do circulo: ");
	 scanf("%f", &raio);
	 printf("Esse circulo tem area de: %.4f", PI*raio*raio);
	 return 0;
 }
	 
