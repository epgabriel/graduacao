/*Programa que ordena uma matriz atraves do metodo bubble sort
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    
    int i, j, cont, aux, n;
    scanf("%d\n", &n);
    int mat[n][n];

    for(i = 0; i < n; i++){
        for(j = 0; j < n; j++){
            scanf("%d", &mat[i][j]);
        }
    }

    do{
        cont = 0;
        for(i = 0; i < n; i++){
            for(j = 0; j < n; j++){
                if(mat[i][j] > mat[i][j+1]){
                    aux = mat[i][j];
                    mat[i][j] = mat[i][j+1];
                    mat[i][j+1] = aux;
                    cont = 1;
                }
                if(mat[i][n-1] > mat[i+1][0]){
                    aux = mat[i][n-1];
                    mat[i][n-1] = mat[i+1][0];
                    mat[i+1][0] = aux;
                    cont = 1;
                }
            }
        }
    } while(cont);
            

    for(i = 0; i < n; i++){
        printf("\n");
        for(j = 0; j < n; j++){
            printf("%d ", mat[i][j]);
        }
    }
    return (EXIT_SUCCESS);
}

