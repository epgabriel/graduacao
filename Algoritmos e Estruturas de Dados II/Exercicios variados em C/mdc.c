/*Programa que calcula o mdc
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>
#include <stdlib.h>
    

/*
 * 
 */
int main(int argc, char** argv) {
    int a, b, maior , menor, aux;
    scanf("%d%d", &a, &b);
    if(a>b){
        maior=a;
        menor=b;
    }else{
        menor=a;
        maior=b;
    }
    while (menor!=0){
        aux=menor;
        menor=maior%menor;
        maior=aux;
    }
    printf("%d", maior);
    return (EXIT_SUCCESS);
}

