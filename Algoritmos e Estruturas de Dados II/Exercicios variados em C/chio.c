/*Programa que calcula o determinante de uma matriz com o metódo de chió
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
 
 #include <stdio.h>

int main(){
    printf("\t\t--------Metodo de Chio--------\n\n");
    int n;
    int i,j,k=0;
    int devil=0;
    double pivo,det=1;
    
    printf("Digite a quantidade de linhas/colunas da matriz: ");
    scanf("%d", &n);
    while(n<1){
		printf("Erro! Digite um valor válido: ");
		scanf("%d", &n);
	}
    
    double mat[n][n], aux;
    printf("Entre com os valores da matriz:\n");
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            scanf("%lf", &mat[i][j]);
        }
    }
    
    while(k<n-1&&devil==0){
        pivo=mat[k][k];
        if(pivo==0){            
            j=k;
            do{
                j++;
                if(mat[k][j]!=0){
                    devil=1;
                }
            }while(devil==0&&j<n);
            devil=0;
            if(j<n){
                for(i=0;i<n;i++){
                    aux = mat[i][k];
                    mat[i][k] = mat[i][j];
                    mat[i][j] = aux;
                }
                det*=(-1);
            }else if(j==n) devil=1;
        }
        if(devil == 0){
	        pivo=mat[k][k];
	        if (pivo!=1){ 
	            for(j=0;j<n;j++){
	                mat[k][j]/=pivo;
	            }
	            det*=pivo;
	        }
	        for(i=k+1;i<n;i++){
	            for(j=k+1;j<n;j++){
	                mat[i][j]-=(mat[k][j]*mat[i][k]);                                     
	            }
	        }
	         k++;
	    }else det*=0;
       
    }
    det*=mat[n-1][n-1];
    printf("\n\nDeterminante: %.2lf\n", det);
    return 0;
}
