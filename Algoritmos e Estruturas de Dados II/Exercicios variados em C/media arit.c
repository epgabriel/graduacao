/* Programa que calcula media aritmetica
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
#include <stdio.h>

int main(){
	int n,i;
	printf("Número de entradas pretendidas: ");
	scanf("%d", &n);
	float vet[n], media=0;
	for(i=0;i<n;i++){
		printf("Digite o %dº elemento: ", i+1);
		scanf("%f", &vet[i]);
		media += vet[i];
	}
	printf("A média dos elementos ");
	for(i=0;i<n-1;i++){
		printf("%.2f, ", vet[i]);
	}
	printf("e %.2f é: %.2f", vet[n-1], media/n);
	return 0;
}
