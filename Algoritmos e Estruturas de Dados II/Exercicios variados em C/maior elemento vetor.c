/*Codigo que imprime o maior valor de um vetor. Usa-se registros
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>

struct Info{
	int id;
	int maiorElemento;
};

int main(){
	struct Info info;
	int n;
	
	printf("\tDigite o tamanho do vetor: ");
	scanf("%d", &n);
	while(n<1){
		printf("\tErro! Digite um tamanho valido: ");
		scanf("%d", &n);
	}
	
	int i, vet[n];
	
	for(i=0; i<n; i++){
		printf("\t");
		scanf("%d", &vet[i]);
		if(i==0){
			info.maiorElemento = vet[i];
			info.id=i+1;
		}
		else if(vet[i]>info.maiorElemento){
			info.maiorElemento = vet[i];
			info.id=i+1;
		}
	}
	
	printf("\n\tO maior elemento deste vetor de tamanho %d é: %d",n,info.maiorElemento);
	printf("\n\tLocalizado na %da posicao.",info.id);
	
	return 0;
}
