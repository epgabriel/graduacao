/*Programa que realiza busca e ordenacao usando registro
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */
#include <stdio.h>

struct Aluno{
	int id;
	int matricula;
};

void lerVetor(struct Aluno[],int);
void lerIdsParaBusca(int[]);
void ordenarVetor(struct Aluno[],int);
int trocar(struct Aluno*, struct Aluno*);
void imprimirIdsOrdenados(struct Aluno[],int);
void buscarIds(struct Aluno[],int,int[]);


int main(){
	printf("Programa que recebe dados id e matricula de uma quantidade definida\n"
		"pelo usuario, e em seguida ordena os ids e le quatro ids a serem buscados\n"
		"se os ids são encontrados são imprimidas as suas respectivas matriculas,\n"
		"caso contrario imprimi-se 0\n\n");
	int n;
	printf("Digite o tamanho do vetor: ");
	scanf("%d", &n);
	while(n<1){
		printf("Tamanho invalido, digite um tamanho adequado (maior que 1): ");
		scanf("%d", &n);
	}
	
	struct Aluno alunos[n];
	int idsParaBusca[4];
	
	lerVetor(alunos,n);
	lerIdsParaBusca(idsParaBusca);
	ordenarVetor(alunos,n);
	imprimirIdsOrdenados(alunos,n);
	buscarIds(alunos,n,idsParaBusca);
	return 0;
	
	return 0;
}

void lerVetor(struct Aluno A[],int n){
	int i;
	for(i=0;i<n;i++){
		printf("Digite o id e a matricula do %dº aluno: ",i+1);
		scanf("%d %d", &A[i].id, &A[i].matricula);
	}
}

void lerIdsParaBusca(int ids[]){
	int i;
	for(i=0;i<4;i++){
		printf("Digite os ids a serem buscados: ");
		scanf("%d",&ids[i]);
	}
}

void ordenarVetor(struct Aluno A[],int n){
	int a;
	int i;
	do{
		a=0; //false
		for(i=0;i<n-1;i++){
			if(A[i].id > A[i+1].id){
				a = trocar(&A[i], &A[i+1]);
			}
		}
	}while(a==1);
}
int trocar(struct Aluno *A,struct Aluno *B){
	struct Aluno C = *A;
	*A = *B;
	*B = C;
	return 1;
}
void imprimirIdsOrdenados(struct Aluno A[],int n){
	int i;
	printf("\n\nIds ordenados:\n");
	for(i=0;i<n;i++){
		printf("%d ",A[i].id);
	}printf("\n\n");
}
void buscarIds(struct Aluno A[],int n,int ids[]){
	int i;
	for(i=0;i<4;i++){
		int meio,inicio=0,fim=n-1;
		int a = 0; //false
		do{
			meio=(inicio+fim)/2;
			if(ids[i]==A[meio].id) a=1;
			else{
				if(ids[i]<A[meio].id) fim=meio-1;
				else inicio=meio+1;
			}
		}while(a == 0 && inicio <= fim);
		if(a==1) printf("%d\n",A[meio].matricula);
		else printf("0\n");
	}
}
