/*Codigo para calculo de um matriz inversa.
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>

int main(){
	int n;
	printf("Digite a quantidade de linhas/colunas da matriz: ");
    scanf("%d", &n);
    while(n<1){
		printf("Erro! Digite uma quantidade valida: ");
		scanf("%d", &n);
	}
    float mat[n][n],ident[n][n],pivo,aux, trocar;
    int i,j,k;
	int devil;
	
	printf("Entre com os valores da matriz: ");
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            scanf("%f", &mat[i][j]);
            if(i==j){
                ident[i][j]=1;
            }else{
                ident[i][j]=0;
            }
        }
    }
    for(k=0;k<n;k++){
        pivo=mat[k][k];
        if(pivo==0){
            devil=0;
            i=k;
            do{
                i++;
                if(mat[i][k]!=0){
                    devil=1;
                }
            }while(devil==0 && i<n);
            if(i==n){
                return 0;
            }else{
                for(j=0;j<n;j++){
                    trocar = mat[k][j];
                    mat[k][j] = mat[i][j];
                    mat[i][j] = trocar;
                    
                    trocar = ident[k][j];
                    ident[k][j] = ident[i][j];
                    ident[i][j] = trocar;
                }
            }
        }
    }
    for(i=0;i<n;i++){
        pivo=mat[i][i];
        if(pivo!=1){
            for(j=0;j<n;j++){
                mat[i][j]/=pivo;
                ident[i][j]/=pivo;
            }
        }
        for(k=i+1;k<n;k++){
            pivo=mat[k][i]*(-1);
            for(j=0;j<n;j++){
                aux=mat[i][j]*pivo;
                mat[k][j]+=aux;
                aux=ident[i][j]*pivo;
                ident[k][j]+=aux;
            }
        }
        if(i>0){
            for(k=i-1;k>=0;k--){
                pivo=mat[k][i]*(-1);
                for(j=n-1;j>=0;j--){
                    aux=mat[i][j]*pivo;
                    mat[k][j]+=aux;
                    aux=ident[i][j]*pivo;
                    ident[k][j]+=aux;
                }
            }
        }
    }
    printf("\nMatriz Inversa:\n");
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("%.3f\t",ident[i][j]);
        }printf("\n");
    }
    return 0;
}
