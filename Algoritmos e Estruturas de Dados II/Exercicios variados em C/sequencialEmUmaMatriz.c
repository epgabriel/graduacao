/*Programa que realiza uma busca sequencial em uma matriz
 * Nomes:
 * Lucas Pereira Ferreira
 * Caio Paiva
 * Gabriel Edmilson
 * Jorge Sassaki
 * Rodrigo Rabelo
 * Obede Carvalho
 */

#include <stdio.h>
int main(){
	
	int n;
	scanf("%d",&n);
	
	int mat[n][n];
	
	int i, j ;
	for( j =0 ; j< n ; j++)
		for( i =0; i<n; i++){
			scanf("%d",&mat[j][i]);
		}
	
	int busca;
	scanf("%d",&busca);
	for( j = 0 ; j<n ; j++){		
		for(i = 0;i<n;i++){
			if(busca == mat[j][i])
			{
				printf("a posicao é %d,%d",j, i );
			}
		}
	}
	return 0;
}
