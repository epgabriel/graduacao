#include <stdio.h>
#include <stdlib.h>

struct Tipo_Lista{
	int num;
	struct Tipo_Lista *Next;
};

struct Tipo_Lista *First;
struct Tipo_Lista *Last;

void criarListaVazia();
void insere(int);
void imprime();
void pesquisa(int busca);

int main(){
	criarListaVazia();
	insere(1);
	imprime();
	insere(2);
	insere(3);
	insere(4);
	imprime();
	insere(5);
	insere(6);
	insere(7);
	imprime();
	
	return 0;
}


void criarListaVazia(){
	struct Tipo_Lista *aux;
	aux = (struct Tipo_Lista*)malloc(sizeof(struct Tipo_Lista));
	First = aux;
	Last = First;
}
	
void insere(int x){
	struct Tipo_Lista *aux;
	aux = (struct Tipo_Lista*)malloc(sizeof(struct Tipo_Lista));
	aux->num = x;
	Last->Next = aux;
	Last = Last->Next;
	aux->Next = NULL;
}

void imprime(){
	struct Tipo_Lista *aux;
	aux = First->Next;
	
	while(aux != NULL){
		printf("\nItem = %d\n", aux->num);
		aux = aux->Next;
	}
}

void pesquisa(int busca){
	struct Tipo_Lista *aux;
	aux = First->Next;
	
	int flag = 0;
	
	while(aux != NULL){
		if(aux->num == busca){
			printf("\nAchou item %d\n",busca);
			aux = NULL;
			flag = 1;
		}
		else{
			aux = aux->Next;
		}
	}
	
	if(!flag){
		printf("\nItem nao encontrado!\n");
	}
	
}
