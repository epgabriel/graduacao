#include <stdio.h>
#include <stdlib.h>

struct Pilha{
	int num;
	struct Pilha *proximo;
};

struct Pilha bottom;
struct Pilha top;

void insere(int);

int main(){
	int num;
	struct Pilha *pilha;
	do{
		pilha = (struct Pilha*)malloc(sizeof(struct Pilha));
		scanf("%d", &num);
		pilha->num = num;
		insere(num);
	}while(num != 0);
	
	imprimir();
	return 0;
}

void insere(int n){
	top.num = n;
	top.proximo = NULL;
}
