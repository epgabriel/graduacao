#include <stdio.h>
#include <stdlib.h>
#include "seqSet.h"


int main(){
	system("sh -c clear");
	FILE *arq = fopen("Sistema.gjl", "w+b");
	
	if(arq != NULL){
		
		int op = 0;
		do{
			printf("========================Menu========================\n\n");
			printf("1 - Inserção\n");
			printf("2 - Remoção\n");
			printf("3 - Buscar\n");
			printf("4 - Imprimir\n");
			printf("0 - Sair\n");
			printf("\tOpção: ");
			
			while(scanf("%d", &op) < 0 || op > 5){
				printf("Erro! Digite uma opção válida: ");
			}
			
			switch(op){
				case 1: inserir(arq); break;
				case 2: remover(arq); break;
				case 3: buscar(arq);  break;
				case 4: imprimir(arq); break;
			}
		}while(op != 0);
	}
	
	else printf("\nErro! Não foi possível abrir o sistema. Operação foi abortada.\n");
	fclose(arq);
	
	return 0;
}
