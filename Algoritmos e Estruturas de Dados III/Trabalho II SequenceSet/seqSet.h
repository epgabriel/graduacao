#ifndef ESTRUTURA_H
#define ESTRUTURA_H

#include <stdio.h>

#define TAM_BLOCO_SEQUENCE_SET 4

struct Objeto{
	char Nome[50];
	int Codigo;
	//Outros Campos
};

struct BlocoSS{
	struct Objeto O[TAM_BLOCO_SEQUENCE_SET]; //Objetos no bloco
	int numObjetos; //Número de objetos no bloco
	int prox; //RRN do próximo bloco
};

void inserir(FILE *);
void remover(FILE *);
void buscar(FILE *);
void imprimir(FILE*);

#endif
