#include <stdio.h>
#include <stdlib.h>
#include "seqSet.h"

int num_blocos_ativos_no_arquivo = 0;
int num_blocos_totais_no_arquivo = 0;

void imprimirBloco(struct BlocoSS *Bloco){
	int i ;
	for(i = 0; i < Bloco->numObjetos; i++){
		printf("\nObjeto: %s\nCódigo: %d\n", Bloco->O[i].Nome, Bloco->O[i].Codigo);
	}
	printf("\n\n");
}

void imprimir(FILE *arq){
	
	if(num_blocos_ativos_no_arquivo > 0){
	
		if(arq != NULL){
			struct BlocoSS Bloco;
			Bloco.prox = 0;
			
			
			int i = 0;
			
			while(i < num_blocos_ativos_no_arquivo){
				fseek(arq, Bloco.prox, SEEK_SET);
				fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
				imprimirBloco(&Bloco);
				i++;
			}
		}
	}
}

void insereNoBloco(struct BlocoSS *Bloco, struct Objeto *objeto){
	int i = 0, j;
	while(objeto->Codigo > Bloco->O[i].Codigo && i < Bloco->numObjetos) i++;
	for(j = Bloco->numObjetos; j > i; j--) Bloco->O[j] = Bloco->O[j-1];
	Bloco->O[i] = *objeto;
	Bloco->numObjetos++;		
}		



int existe(int cod, FILE *arq){
	int encontrou = 0;
	if(num_blocos_ativos_no_arquivo != 0){
		rewind(arq);
		struct BlocoSS aux;
		int i = 0;
		do{
			fseek(arq, i*sizeof(struct BlocoSS), SEEK_SET);
			fread(&aux, sizeof(struct BlocoSS), 1, arq);
			int j = 0;
			for(; j < aux.numObjetos && encontrou == 0; j++){
				if(aux.O[j].Codigo == cod) encontrou = 1;
			}
			i++;
		}while(i < num_blocos_ativos_no_arquivo);
	}
	return encontrou;
}

void inserir(FILE *arq){
	system("sh -c clear");
	
	struct Objeto objeto;
	
	printf("\nNome do Objeto: ");
	scanf("%*c%[^\n]", objeto.Nome);
	
	printf("Código do Objeto: ");
	scanf("%d",&objeto.Codigo);
	
	while(existe(objeto.Codigo, arq) == 1){
		printf("Código já existente, informe um código válido: ");
		scanf("%d",&objeto.Codigo);
	}
	
	
	
	if(num_blocos_ativos_no_arquivo == 0){
		
		
		struct BlocoSS Bloco;
		Bloco.O[0] = objeto;
		Bloco.numObjetos = 1;
		Bloco.prox = sizeof(struct BlocoSS);
		
		fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
		
		
		num_blocos_ativos_no_arquivo++;
		num_blocos_totais_no_arquivo++;
	}
	
	else{

		if(arq != NULL){
			struct BlocoSS Bloco;
			struct BlocoSS Anterior;
			Anterior.prox = 0;
			
			rewind(arq);
			fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
			
			int i=1, flag = 0;
			
			while(i < num_blocos_ativos_no_arquivo && flag == 0){
				Anterior = Bloco;
				fseek(arq, Bloco.prox, SEEK_SET);
				fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
				
				
				if(objeto.Codigo < Bloco.O[Bloco.numObjetos-1].Codigo){
					flag = 1;
				}
				i++;
			}
		
			//Sabemos o Bloco onde deve ser inserido o objeto
		
			if(Bloco.numObjetos < TAM_BLOCO_SEQUENCE_SET){
				insereNoBloco(&Bloco, &objeto);
				fseek(arq, Anterior.prox, SEEK_SET);
				fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
			}
			
			else if(Bloco.numObjetos == TAM_BLOCO_SEQUENCE_SET){				
				
				struct BlocoSS NovoBloco;
				NovoBloco.numObjetos = 0;
				
				
				struct Objeto maior;
				
				if(Bloco.O[Bloco.numObjetos-1].Codigo > objeto.Codigo){
					maior = Bloco.O[Bloco.numObjetos-1];
					int i = 0, j;
					while(objeto.Codigo > Bloco.O[i].Codigo && i < Bloco.numObjetos) i++;
					for(j = Bloco.numObjetos-1; j > i; j--) Bloco.O[j] = Bloco.O[j-1];
					Bloco.O[i] = objeto;
				}
				else maior = objeto;
				
				
				int lim;
				
				if(TAM_BLOCO_SEQUENCE_SET%2 == 0) lim = (TAM_BLOCO_SEQUENCE_SET/2)-1;
				else lim = TAM_BLOCO_SEQUENCE_SET/2;
				
				for(i = 0; i < lim; i++){
					NovoBloco.O[i] = Bloco.O[1+i+(TAM_BLOCO_SEQUENCE_SET/2)];
					NovoBloco.numObjetos++;
					Bloco.numObjetos--;
				}
				NovoBloco.O[NovoBloco.numObjetos] = maior;
				NovoBloco.numObjetos++;
				
				NovoBloco.prox = Bloco.prox;
				Bloco.prox = num_blocos_totais_no_arquivo*sizeof(struct BlocoSS);
				
				fseek(arq, Anterior.prox, SEEK_SET);
				fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
				
				fseek(arq, Bloco.prox, SEEK_SET);
				fwrite(&NovoBloco, sizeof(struct BlocoSS), 1, arq);
				
				num_blocos_ativos_no_arquivo++;
				num_blocos_totais_no_arquivo++;
			}
				
		}
			
	}
}

void remover(FILE *arq){
	
	system("sh -c clear");
	
	if(num_blocos_ativos_no_arquivo == 0) {
		printf("\nESTRUTURA VAZIA!!!\n");
	}
	
	else{
		
		int e;
		printf("\nCódigo do objeto a ser removido: ");
		scanf("%d",&e);
		
		int encontrouBloc = 0;
		int remotPos = -1;
		
		struct BlocoSS Bloco;
		
		fseek(arq, 0, SEEK_SET);
		fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
		
		struct BlocoSS Anterior;
		Anterior.prox = 0;
		
		struct BlocoSS AnteAnterior;
		AnteAnterior.prox = 0;
		
		int i = 0;
		while(i < num_blocos_ativos_no_arquivo && encontrouBloc == 0){
			if(Bloco.O[Bloco.numObjetos-1].Codigo < e){
				AnteAnterior = Anterior;
				Anterior = Bloco;
				fseek(arq, Bloco.prox, SEEK_SET);
				fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
			}
			else{
				encontrouBloc = 1;
			}
			i++;
		}
		
		i = 0;
		while(i < Bloco.numObjetos && remotPos == -1){
			if(Bloco.O[i].Codigo == e){
				remotPos = i;
			}
			i++;
		}
		
		if(remotPos != -1){
			/*Se O bloco for raíz OU o Bloco possui objetos suficientes*/
			if(Anterior.prox == 0 || Bloco.numObjetos > TAM_BLOCO_SEQUENCE_SET/2){
				
				/*'Apaga o objeto' vulgo joga por cima*/
				for(i = remotPos; i < Bloco.numObjetos-1; i++){
					Bloco.O[i] = Bloco.O[i+1];
				}
				Bloco.numObjetos--; //numero de elementos diminui
				
				fseek(arq, Anterior.prox, SEEK_SET);
				fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
				
				/*Se foi removido o ultimo objeto do bloco E ele é raíz E há mais blocos*/
				if(Bloco.numObjetos == 0 && Anterior.prox == 0 && num_blocos_ativos_no_arquivo > 1){
					
					struct BlocoSS Proximo;
					fseek(arq, Bloco.prox, SEEK_SET);
					fread(&Proximo, sizeof(struct BlocoSS), 1, arq);
					
					for(i = 0; i < Proximo.numObjetos; i++){
						Bloco.O[i] = Proximo.O[i];
					}
					
				
					Bloco.numObjetos = Proximo.numObjetos;
					Bloco.prox = Proximo.prox;
					
					num_blocos_ativos_no_arquivo--;
					fseek(arq, Anterior.prox, SEEK_SET);
					fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
					
				}
				
			}
			
			else{ /*Bloco nao é raiz e o seu numero de objetos é inferior ao minimo apos a remocao*/
				
				for(i = 0; i < Bloco.numObjetos && Bloco.O[i].Codigo != e ;i++);
				
				if(i != Bloco.numObjetos){
					
					int j;	
					if(Anterior.numObjetos > TAM_BLOCO_SEQUENCE_SET/2 && Bloco.numObjetos-1 > 0){
						
						
						for(j = i; j > 0; j--){ /*remove o objeto jogando tudo por cima*/
							Bloco.O[j] = Bloco.O[j-1];
						}
						Bloco.O[0] = Anterior.O[Anterior.numObjetos-1];
						Anterior.numObjetos--;
						
						fseek(arq, AnteAnterior.prox, SEEK_SET);
						fwrite(&Anterior, sizeof(struct BlocoSS), 1, arq);
						
						fseek(arq, Anterior.prox, SEEK_SET);
						fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
					}
						
					else {
						i=0;
						j = Anterior.numObjetos ;
						while(i<Bloco.numObjetos){
							if(Bloco.O[i].Codigo != e){
								Anterior.O[j++] = Bloco.O[i];
							}
							i++;
						}
						Anterior.numObjetos += i-1;
						Anterior.prox = Bloco.prox;
						
						fseek(arq, AnteAnterior.prox, SEEK_SET);
						fwrite(&Anterior, sizeof(struct BlocoSS), 1, arq);
						
						num_blocos_ativos_no_arquivo--;
						
						fseek(arq, AnteAnterior.prox, SEEK_SET);
						fwrite(&Anterior, sizeof(struct BlocoSS), 1, arq);
						
						fseek(arq, Anterior.prox, SEEK_SET);
						fwrite(&Bloco, sizeof(struct BlocoSS), 1, arq);
					}
					
				}
			}
		}
	}
}	

void buscar(FILE *arq){
	system("sh -c clear");
	
	if(num_blocos_ativos_no_arquivo == 0) {
		printf("\nESTRUTURA VAZIA!!!\n");
	}
	
	else{
		
		int e;
		printf("\nCódigo do objeto a ser buscado: ");
		scanf("%d",&e);
		
		int encontrouBloc = 0;
		int remotPos = -1;
		
		struct BlocoSS Bloco;
		
		fseek(arq, 0, SEEK_SET);
		fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
		
		struct BlocoSS Anterior;
		Anterior.prox = 0;
		
		int i = 0;
		while(i < num_blocos_ativos_no_arquivo && encontrouBloc == 0){
			if(Bloco.O[Bloco.numObjetos-1].Codigo < e){
				Anterior = Bloco;
				fseek(arq, Bloco.prox, SEEK_SET);
				fread(&Bloco, sizeof(struct BlocoSS), 1, arq);
			}
			else{
				encontrouBloc = 1;
			}
			i++;
		}

		i = 0;
		while(i < Bloco.numObjetos && remotPos == -1){
			if(Bloco.O[i].Codigo == e){
				remotPos = i;
			}
			i++;
		}	
		
		if(remotPos != -1){
			printf("O objeto se encontra na posição %d do disco. Retornando ao Menu Principal.\n", Anterior.prox);
		}
		else{
			printf("O objeto não foi encontrado no sistema. Abortando operação. Retornando ao Menu Principal.\n");
		}
	}
}
