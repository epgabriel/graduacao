#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

void alocarMatriz(struct Arvore *P){
	
	if(P->matAmz != NULL) desalocarMatriz(P);
	
	//alocar as linhas da matriz
	P->matAmz = calloc(P->tam_matriz+10, sizeof(int*)); //um vetor de n ponteiros para int
	
	if(P->matAmz != NULL){
		P->tam_matriz += 10;
		int i=0;
		while(i < P->tam_matriz && P->matAmz != NULL){
			P->matAmz[i] = calloc(P->tam_matriz, sizeof(int)); //n vetores de n ints
			if(P->matAmz[i] == NULL) P->matAmz = NULL;
			else i++;
		}
	}
}

void desalocarMatriz(struct Arvore *P){
	if(P->matAmz != NULL){
		int i;
		for(i = 0; i < P->tam_matriz; i++){
			free(P->matAmz[i]);
		}
		free(P->matAmz);
	}
	P->matAmz = NULL;
}

void menuLerOpcao(int *op){
	/*Função que apresenta o menu de opções; armazena a opção escolhida
	 * pelo usuário e certifica-se de que será uma opção válida
	 */ 
	printf("\n================== MENU ==================\n"
			"\n1 - Inserção"
			"\n2 - Remover"
			"\n3 - Busca"
			"\n4 - Cadastrar amizade"
			"\n5 - Remover amizade"
			"\n6 - Consultar amizade"
			"\n7 - Caminho de amizade"
			"\n0 - Exit"
			"\n\tDigite uma opção: ");
	scanf("%d", op);
	while(*op < 0 || *op >7){
		printf("Digite uma opção válida: ");
		scanf("%d", op);
	}
}


struct Pessoa* posID(struct Pessoa *P, int id){
	
	struct Pessoa *aux = P;
		
	while(aux != NULL && aux->raiz->id != id){
			
		if(id < aux->raiz->id){ //Se o id de pessoa for menor que o do nó verificado
			aux = aux->esquerda; //então aux desloca-se para a esquerda
		}
				
		else{ //Caso contrário, desloca-se para a direita
			aux = aux->direita;
		}
	}
	
	return aux;
}




void realocarMatriz(struct Arvore *P){
	int tam = P->tam_matriz;
	int matAux[tam][tam];
	
	int i, j;
	for(i=0; i<tam; i++){
		for(j=0; j<tam; j++){
			matAux[i][j] = P->matAmz[i][j];
		}
	}
	
	alocarMatriz(P);
	
	for(i=0; i<tam; i++){
		for(j=0; j<tam; j++){
			P->matAmz[i][j] = matAux[i][j];
		}
	}
}

void addMatriz(struct Arvore *P, int id){
	if(P->cadastros > P->tam_matriz-1){
		realocarMatriz(P);
	}
	
	int i=1;
	int flag = 0;
	while(i<P->tam_matriz && !flag){
		if(P->matAmz[i][0] <= 0){
			P->matAmz[i][0] = id;
			P->matAmz[0][i] = id;
			P->matAmz[i][i] = 1;
			flag = 1;
		}
		i++;
	}
	
}

int posMatriz(struct Arvore *P, int id){
	int i = 1;
	while(P->matAmz[i][0] != id) i++;
	return i;
}

void addAmz(struct Arvore *P, int id1, int id2){
	id1 = posMatriz(P, id1);
	id2 = posMatriz(P, id2);
	
	P->matAmz[id1][id2] = 1;
	P->matAmz[id2][id1] = 1;
}

void remAmz(struct Arvore *P, int id1, int id2){
	id1 = posMatriz(P, id1);
	id2 = posMatriz(P, id2);
	
	P->matAmz[id1][id2] = 0;
	P->matAmz[id2][id1] = 0;
}

void remMatriz(struct Arvore *P, int id){
	id = posMatriz(P, id);
	
	P->matAmz[id][0] = -1;
	P->matAmz[0][id] = -1;
	
	int i;	
	for(i=1; i<P->tam_matriz; i++){
		P->matAmz[id][i] = 0;
		P->matAmz[i][id] = 0;
	}
}
	
	


void imprimir(struct Pessoa *P){
	if(P != NULL){
		printf("\nNome: %s\tID: %d", P->nome, P->raiz->id);
		imprimir(P->esquerda);
		imprimir(P->direita);
	}
}

void desalocarIDs(struct Nodo *raiz){
	if(raiz != NULL){
		desalocarIDs(raiz->esquerda);
		desalocarIDs(raiz->direita);
		free(raiz);
	}
}
void desalocar(struct Pessoa *P){
	if(P != NULL){
		desalocar(P->esquerda);
		desalocar(P->direita);
		desalocarIDs(P->raiz);
		free(P);
	}
}
	
