#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore.h"

void removeCasoFolha(struct Arvore *P, struct Pessoa *anterior, struct Pessoa *atual){
	//Função invocada quando se deseja remover um nó que não possui filhos
	
	if(anterior == NULL){ //Se o nó folha for a raiz da arvore
		P->pessoa = NULL;
	}
	
	else if(atual->raiz->id < anterior->raiz->id){
		/*Se o ID a ser removido for menor que o seu pai então o pai 
		 * (anterior) deve apontar sua esquerda para NULL
		 */
		anterior->esquerda = NULL;
	}
	
	else if(atual->raiz->id > anterior->raiz->id){
		/*Senão então ele está à direita pois é maior, então o pai
		 * (anterior) deve apontar sua direita para NULL
		 */
		 anterior->direita = NULL;
	}
	
	
	//Agora podemos desalocar a memória usada pelo nó que estamos removendo 
	free(atual);
}

void removeCasoUmFilho(struct Arvore *P, struct Pessoa *anterior, struct Pessoa *atual){
	//Funcao invocada quando se deseja remover um nó que possui somente um filho
	
	
	if(anterior == NULL){
		if(atual->esquerda != NULL){
			P->pessoa = atual->esquerda;
		}
		else if(atual->direita != NULL){
			P->pessoa = atual->direita;
		}
	}
	
	else if(atual->raiz->id < anterior->raiz->id){
		//Se o nó a ser removido está à esquerda do seu pai:
		
		if(atual->esquerda != NULL){
		//Se o nó a ser removido possui filhos somente à esquerda, então:
			anterior->esquerda = atual->esquerda;
		}

		else if(atual->direita != NULL){
			//Senão o nó a ser removido possui filhos somente à direita, então:
			anterior->esquerda = atual->direita;
		}
		
	}
	
	else if(atual->raiz->id > anterior->raiz->id){
		//Senão, ele está à direita de seu pai
		
		if(atual->esquerda != NULL){
		//Se o nó a ser removido possui filhos somente à esquerda, então:
			anterior->direita = atual->esquerda;
		}

		else if(atual->direita != NULL){
			//Senão o nó a ser removido possui filhos somente à direita, então:
			anterior->direita = atual->direita;
		}
	}
	//desaloca a memória usada pelo nó que removemos
	free(atual);
}

void removeCasoDoisFilhos(struct Arvore *P, struct Pessoa *anterior, struct Pessoa *atual){
	//Função invocada quando se deseja remover um nó que possui dois filhos
	anterior = atual;
	struct Pessoa *maior = atual->esquerda;
	//maior receberá o maior elemento a direita do primeiro filho a esquerda
	
	while(maior->direita != NULL){
		//Enquanto houver elementos à direita de maior:
		anterior = maior;
		maior = maior->direita;
	}
	
	struct Pessoa aux;
	
	strcpy(aux.nome, maior->nome);
	aux.raiz = maior->raiz;
	
    verificaCasoDeRemocao(P,anterior,maior);
    
    strcpy(atual->nome, aux.nome);
    atual->raiz = aux.raiz;
    
    
}

void verificaCasoDeRemocao(struct Arvore *P, struct Pessoa *anterior, struct Pessoa *atual){
	/*Função invocada somente se um ID a ser removido foi encontrado na
	 * nossa arvore principal, essa função se encarrega de indentificar
	 * o tipo de remoção que cada situação demanda e invoca as suas
	 * respectivas funções
	 */ 
	
	//       P => Apontador para a raiz da arvore principal
	//anterior => nó antecessor ao nó a ser removido
	//   atual => nó a ser removido
		
	if(atual->esquerda == NULL && atual->direita == NULL){
		/*Se o nó atual não possui filhos nem a esquerda nem a direita
		 * então o nó que queremos remover é um nó folha
		 */ 
		
		removeCasoFolha(P, anterior, atual);
	}
	
	else if( (atual->esquerda != NULL && atual->direita == NULL) || (atual->esquerda == NULL && atual->direita != NULL) ){
		/*Senão, se o nó atual possuir filhos ou à direita ou à esquerda
		 * então queremos remover um nó que possui um filho
		 */ 
		
		removeCasoUmFilho(P, anterior, atual);
	}
	else if(atual->esquerda != NULL && atual->direita != NULL){
		/*Senão, se o nó atual possui filhos à esquerda E filhos à 
		 * direita...
		 */ 
		
		removeCasoDoisFilhos(P, anterior, atual);
	}
}

void remocao(struct Arvore *P){
	if(P->pessoa == NULL){ 
		/* Se a árvore principal está vazia então não há nada que possa
		 * ser feito, logo imprimimos que não é possível realizar
		 * nenhuma ação e o programa retorna ao menu principal
		 */ 
		printf("\nNenhum usuário cadastrado!\n");
	}
	
	else{
		//Caso contrário uma série de procedimentos é demandada
		
		int id;
			
		printf("\nInsira o ID a ser removido: ");
		scanf("%d", &id); //le-se o id a ser removido
		
		struct Pessoa *anterior = NULL; //anterior recebe NULL e guarda o nó já comparado
		struct Pessoa *atual = P->pessoa; //atual recebe a raiz da arvore principal
		
		int flag = 0; //flag para parar o loop while abaixo:
		
		while(atual != NULL && flag == 0){ //Enquanto atual existir e flag for falsa
			
			//enquanto o nó atual existir e ele não possuir o ID procurado para remoção fazer:
			
			if(atual->raiz->id == id){ //Se for achado o id a ser removido flag fica verdadeira
				flag = 1;
			}
			
			else{ // Caso contrário:
				
				anterior = atual; //anterior recebe esse nó atual
				
				//se o ID procurado é menor que o ID que se encontra no atual nó então nos deslocamos para a esquerda
				if(id < atual->raiz->id){
					atual = atual->esquerda;
				}
				
				//caso contrário nos deslocamos para a direita
				else if(id > atual->raiz->id){
					atual = atual->direita;
				}
			}
			
		}
		
		if(flag == 1){ //Se flag for verdadeira:
			removerTodosAmigos(P, atual, atual->raiz->esquerda); //Remove o id atual de todos os seus amigos à esquerda
			removerTodosAmigos(P, atual, atual->raiz->direita); //Remove o id atual de todos os seus amigos à direita
			remMatriz(P, atual->raiz->id); //remove o id da matriz
			verificaCasoDeRemocao(P, anterior, atual); //verifica o caso de remoção apropriado para esse nó
			P->cadastros--; //Diminui em um o número de pessoas cadastradas na árvore principal
			printf("\nOperação realizada com sucesso!\n\tRetornando ao menu principal...\n");
		}
		else{ //Caso contrário o id não foi encontrado na nossa árvore
			printf("\nID %d nao encontrado!\n", id);
		}
	}
}
