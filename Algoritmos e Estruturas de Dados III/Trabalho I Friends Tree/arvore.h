#ifndef ARVORE_H
#define ARVORE_H

struct Nodo{
	int id;
	struct Nodo *esquerda;
	struct Nodo *direita;
};

struct Pessoa{
	char nome[30];
	struct Nodo *raiz;
	struct Pessoa *esquerda;
	struct Pessoa *direita;
};

struct Arvore{
	struct Pessoa *pessoa;
	int cadastros;
	int tam_matriz;
	int **matAmz;
};

//============================Comuns============================
void menuLerOpcao(int*);

void alocarMatriz(struct Arvore*);
void desalocarMatriz(struct Arvore*);
void realocarMatriz(struct Arvore*);
void addMatriz(struct Arvore*, int);
 int posMatriz(struct Arvore*, int);
void addAmz(struct Arvore*, int, int);
void remAmz(struct Arvore*, int, int);
void remMatriz(struct Arvore*, int);

struct Pessoa* posID(struct Pessoa*, int);
void imprimir(struct Pessoa*);
void desalocarIDs(struct Nodo*);
void desalocar(struct Pessoa*);

//============================INSERCAO============================
void preencherPessoa(struct Arvore*, struct Pessoa*);
void inserir(struct Arvore*);

//============================Remocao============================
void removeCasoFolha(struct Arvore*,struct Pessoa*, struct Pessoa*);
void removeCasoDoisFilhos(struct Arvore*, struct Pessoa*, struct Pessoa*);
void removeCasoUmFilho(struct Arvore*, struct Pessoa*, struct Pessoa*);
void verificaCasoDeRemocao(struct Arvore*, struct Pessoa*, struct Pessoa*);
void remocao(struct Arvore*);


//============================Amigos============================
 int saoAmigos(struct Nodo*, int);
void consultarAmizade(struct Arvore*);
void cadastrarAmizade(struct Arvore*);
void buscar(struct Arvore*);
void inserirAmizade(struct Nodo*, int);
void imprimeAmizades(struct Nodo*);

void lerIDsParaRemocao(struct Arvore*);
void removerAmizades(struct Arvore*, struct Pessoa*, struct Pessoa*);
void removeAmizade(struct Pessoa*, int);
void verificarCasoDeRemocaoAmigos(struct Pessoa*, struct Nodo*, struct Nodo*);
void removeCasoFolhaAmigos(struct Nodo*, struct Nodo*);
void removeCasoUmFilhoAmigos(struct Nodo*, struct Nodo*);
void removeCasoDoisFilhosAmigos(struct Pessoa*,struct Nodo*, struct Nodo*);

void removerTodosAmigos(struct Arvore*, struct Pessoa*, struct Nodo*);

//============================Bonus=============================
void imprimirMatriz(struct Arvore*);
 int idComparado(int*, int, int);
 int ultimaPosValida(int*, int);
void percurso(struct Arvore*, int*, int, int, int*);
void caminhoDeAmizades(struct Arvore*);



#endif
