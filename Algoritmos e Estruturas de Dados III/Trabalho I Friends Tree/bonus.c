#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

void imprimirMatriz(struct Arvore *P){
	int i, j;
	for(i=0; i< P->tam_matriz; i++){
		for(j=0; j < P->tam_matriz; j++){
			printf("%d ", P->matAmz[i][j]);
		}
		printf("\n");
		
	}
	printf("\n\n");
}

int idComparado(int *vet, int tam, int id){
	int i=0;
	int flag = 0;
	
	while(i < tam && flag == 0){
		if(vet[i] == id){
			 flag = 1;
		 }
		 i++;
	}
	
	return flag;
}

int ultimaPosValida(int *vet, int tam){
	int i = 0;
	int flag = 0;
	
	while(i < tam && flag == 0){
		if(vet[i] == -1) flag = 1;
		else i++;
	}
	
	return i;
}

void percurso(struct Arvore *P, int *vet, int id1, int id2, int *flag){
	int posId1 = posMatriz(P, id1);
	
	
	if(P->matAmz[posMatriz(P, id2)][posId1] == 1){
		*flag = 1;
	}
	
	else{
	
		int i=1;
	
		while(i < P->tam_matriz && *flag == 0){
	
			if(idComparado(vet, P->tam_matriz, P->matAmz[i][0]) == 0){
	
				if(P->matAmz[i][posId1] > 0){
	
					vet[ultimaPosValida(vet, P->tam_matriz)] = P->matAmz[i][0];
	
					percurso(P, vet, P->matAmz[i][0], id2, flag);
				}
			}
			i++;
		}
	}
	
	if(*flag == 1){
		printf("%d ", id1);
	}
	
}


void caminhoDeAmizades(struct Arvore *P){
	
	if(P->pessoa == NULL || (P->pessoa->esquerda == NULL && P->pessoa->direita == NULL) ){
		printf("\nNão há pessoas cadastradas suficientes para prosseguir com a "
		"operação!\n\tRetornando ao menu principal...\n");
	}
	
	else{
		printf("\nDigite os IDs para calcular um \"possível\" caminho de amizade(s):\n\n");
		
		int id1, id2, flag=0;
		
		printf("\tPrimeiro ID: ");
		scanf("%d",&id1);
			
		printf("\t Segundo ID: ");
		scanf("%d",&id2);
		
		struct Pessoa *p1 = posID(P->pessoa, id1);
		struct Pessoa *p2 = posID(P->pessoa, id2);
			
		if(p1 != NULL && p2 != NULL){
			int vet[P->tam_matriz];
			vet[0] = id2;
			int i;
			for(i=1; i<P->tam_matriz; i++){
				vet[i] = -1;
			}
			printf("Calculando caminho começando em %d... ", id1);
			percurso(P, vet, id2, id1, &flag);
			
			if(flag){
				printf("\nOperação realizada com sucesso, retornando ao menu principal\n");
			}else{
				printf("Não foi encontrado nenhum caminho!\n\tRetornando ao menu principal...\n");
			}
		}
		
		else{
			printf("\n");
			if(p1 == NULL){
				printf("ID %d não cadastrado no sistema!\n", id1);
			}
			if(p2 == NULL){
				printf("ID %d não cadastrado no sistema!\n", id2);
			}
			printf("\tRetornando ao menu principal...\n");
		}
	}
}
