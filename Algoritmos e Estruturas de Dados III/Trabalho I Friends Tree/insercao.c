#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"


void preencherPessoa(struct Arvore *P, struct Pessoa *pessoa){
	//Função responsável pelo preenchimento dos campos do registro Pessoa
	printf("\t\tCADASTRAMENTO\n");
	printf("\nPreencha os campos abaixo:\n"); 
	printf("\nNome: ");
	scanf("%*c%[^\n]", pessoa->nome);
	
	pessoa->raiz = malloc(sizeof(struct Nodo)); //alocando memoria para o tipo Nodo
	printf("ID: ");
	scanf("%d", &pessoa->raiz->id); //lê o ID
	
	while(posID(P->pessoa, pessoa->raiz->id) != NULL){
		//Enquanto houver uma pessoa com o mesmo ID inserido, deve-se inserir outro ID
		printf("ID já existente! Insira um ID válido: ");
		scanf("%d", &pessoa->raiz->id);
	}
	
	//seta as extremidades para NULL
	pessoa->esquerda = NULL;
	pessoa->direita  = NULL;
	
	pessoa->raiz->esquerda = NULL;
	pessoa->raiz->direita = NULL;
}

void inserir(struct Arvore *P){
	struct Pessoa *pessoa = malloc(sizeof(struct Pessoa));
	
	preencherPessoa(P, pessoa); //pessoa é preenchido
	
	if(P->pessoa == NULL){ //Se nenhuma pessoa estiver cadastrada
		P->pessoa = pessoa; //a raíz da árvore principal aponta pra pessoa
	}
	else{ //Se já houver pessoa(s) cadastrada(s)
		struct Pessoa *anterior = NULL; //ponteiro para armazenamento do nó antecessor
		struct Pessoa *atual = P->pessoa; //ponteiro para armazenamento do nó atual, começando na raíz
		
		int flag = 0; //uma flag auxiliar de parada começando com false
		
		while(atual != NULL && !flag){ //enquanto houver pessoas (será executado pelo menos uma vez)
			
			anterior = atual; //Nó verificado passa a ser antecessor
			
			if(pessoa->raiz->id < atual->raiz->id){ //Se o id de pessoa for menor que o do nó verificado
				atual = atual->esquerda; //então atual desloca-se para a esquerda
				
				if(atual == NULL){
					anterior->esquerda = pessoa;
				}
			}
			
			else{ //Caso contrário, desloca-se para a direita
				atual = atual->direita;
				
				if(atual == NULL){
					anterior->direita = pessoa;
				}
			}
		}
	}
	
	P->cadastros++;
	addMatriz(P, pessoa->raiz->id);
	printf("\nCadastro realizado com sucesso! Voltando ao menu...\n");
}

