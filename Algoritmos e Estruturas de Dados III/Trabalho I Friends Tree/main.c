#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

int main(){
	
	system("sh -c clear");
	
	struct Arvore *P = malloc(sizeof(struct Arvore));
	P->cadastros = 0;
	P->tam_matriz = 0;
	alocarMatriz(P);
	
	int op = 0; //variável para armazenar a opção digitada pelo usuário	
	
	do{
		menuLerOpcao(&op);
		
		system("sh -c clear");
		
		switch(op){
			case 1: inserir(P);
			break;
			
			case 2: remocao(P); 
			break;
		
			case 3: buscar(P); 
			break;
			
			case 4: cadastrarAmizade(P);
			break;
			
			case 5: lerIDsParaRemocao(P);
			break;
			
			case 6: consultarAmizade(P);
			break;
			
			case 7: caminhoDeAmizades(P);
			break;
		}
		
		//imprimir(P->pessoa);
		
	}while(op != 0);
	
	//imprimir(P->pessoa);
	printf("\n");
	desalocarMatriz(P);
	desalocar(P->pessoa);
	
	return 0;
}
