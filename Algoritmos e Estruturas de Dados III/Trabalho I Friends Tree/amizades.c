#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

int saoAmigos(struct Nodo *raiz, int id){
	if(raiz->direita == NULL && raiz->esquerda == NULL){
		return 0;
	}
	else{
		struct Nodo *atual = raiz;
		
		while(atual != NULL && atual->id != id){
			if(id < atual->id) atual = atual->esquerda;
			else atual = atual->direita;
		}
		
		if(atual == NULL) return 0;
		else return 1;
	}
}

void buscar(struct Arvore *P){
	if(P->pessoa == NULL){
		printf("\nNenhum cadastro realizado! Impossível prosseguir com a operação!\n"
		"\tRetornando ao menu principal...\n");
	}
	
	else{
		int id;
		printf("\nID a ser buscado: ");
		scanf("%d",&id);
		
		struct Pessoa *aux = posID(P->pessoa, id);
		
		if(aux != NULL){
			printf("\nNome: %s\tID: %d\n", aux->nome, aux->raiz->id);
			printf("Amigos: ");
			if(aux->raiz->esquerda == NULL && aux->raiz->direita == NULL){
				printf("Nenhum amigo cadastrado\n");
			}
			
			else{
				imprimeAmizades(aux->raiz->esquerda);
				imprimeAmizades(aux->raiz->direita);
				printf("\n");
			}
		}
		
		else{
			printf("\nID não existente, voltando ao menu principal...\n");
		}
	}
}
void imprimeAmizades(struct Nodo *raiz){
	if(raiz != NULL){
		printf(" %d", raiz->id);
		imprimeAmizades(raiz->esquerda);
		imprimeAmizades(raiz->direita);
	}
}

void consultarAmizade(struct Arvore *P){
	if(P->pessoa == NULL || (P->pessoa->esquerda == NULL && P->pessoa->direita == NULL) ){
		printf("\nNão há pessoas cadastradas suficientes para prosseguir com a "
		"operação!\n\tRetornando ao menu principal...\n");
	}
	
	else{
		int id1, id2;
		printf("\nDigite os IDs para a consuta:\n\n");
		
		printf("\tPrimeiro ID: ");
		scanf("%d",&id1);
		
		printf("\t Segundo ID: ");
		scanf("%d",&id2);
		
		struct Pessoa *p1 = posID(P->pessoa, id1);
		struct Pessoa *p2 = posID(P->pessoa, id2);
		
		if(p1 != NULL && p2 != NULL){
			
			if(!saoAmigos(p1->raiz, id2)){					
				printf("\n\t:::NÃO SÃO AMIGOS:::\n");
			}else{
				printf("\n\t:::SÃO AMIGOS:::\n");
			}
		}
		else{
			if(p1 == NULL){
				printf("\nID %d não existente! Não é possível haver"
				" amizade!\n\tRetornando ao menu principal...", id1);
			}
			if(p2 == NULL);{
				printf("\nID %d não existente! Não é possível haver"
				" amizade!\n\tRetornando ao menu principal...", id2);
			}
			printf("\n");
		}
	}
}

void cadastrarAmizade(struct Arvore *P){
	
	if(P->pessoa == NULL || (P->pessoa->esquerda == NULL && P->pessoa->direita == NULL) ){
		printf("\nNão há pessoas cadastradas suficientes para prosseguir com a "
		"operação!\n\tRetornando ao menu principal...\n");
	}
	
	else{
		int id1, id2;
		printf("\nDigite os IDs a serem amigos:\n\n");
		
		printf("\tPrimeiro ID: ");
		scanf("%d",&id1);
		
		printf("\t Segundo ID: ");
		scanf("%d",&id2);
		
		struct Pessoa *p1 = posID(P->pessoa, id1);
		struct Pessoa *p2 = posID(P->pessoa, id2);
		
		if(p1 != NULL && p2 != NULL){
			
			if(!saoAmigos(p1->raiz, id2)){					
				inserirAmizade(p1->raiz, id2);
				inserirAmizade(p2->raiz, id1);
				addAmz(P, id1, id2);
				printf("\nOperação realizada com sucesso, retornando ao menu principal\n");
			}
			else{
				printf("\nIDs %d e %d já eram amigos, voltando ao menu principal...\n", id1, id2);
			}
		}

		else{
			printf("\n");
			if(p1 == NULL){
				printf("ID %d não cadastrado no sistema!\n", id1);
			}
			if(p2 == NULL){
				printf("ID %d não cadastrado no sistema!\n", id2);
			}
			printf("\tRetornando ao menu principal...\n");
		}
	}
}

void inserirAmizade(struct Nodo *raiz, int id){	
	
	struct Nodo *pNodo = malloc(sizeof(struct Nodo));
	pNodo->id = id;
	pNodo->esquerda = NULL;
	pNodo->direita = NULL;
	
	struct Nodo *anterior = NULL;
	struct Nodo *atual = raiz;
		
	while(atual != NULL){
		anterior = atual;
		if(id < atual->id) atual = atual->esquerda;
		else atual = atual->direita;
	}
		
	if(id < anterior->id) anterior->esquerda = pNodo;
	else anterior->direita = pNodo;
}

void lerIDsParaRemocao(struct Arvore *P){
	
	if(P->pessoa == NULL || (P->pessoa->esquerda == NULL && P->pessoa->direita == NULL) ){
		printf("\nNão há pessoas cadastradas suficientes para prosseguir com a "
		"operação!\n\tRetornando ao menu principal...\n");
	}
	
	else{
		int id1, id2;
		printf("\nDigite os IDs para remoção de amizade:\n\n");
		
		printf("\tPrimeiro ID: ");
		scanf("%d",&id1);
		
		printf("\t Segundo ID: ");
		scanf("%d",&id2);
		
		struct Pessoa *p1 = posID(P->pessoa, id1);
		struct Pessoa *p2 = posID(P->pessoa, id2);	
		
		printf("\n");
		
		removerAmizades(P, p1, p2);
	}
}

void removerAmizades(struct Arvore *P, struct Pessoa *p1, struct Pessoa *p2){
	
	if(p1 != NULL && p2 != NULL){
		
		if(saoAmigos(p1->raiz, p2->raiz->id) == 0){
			printf("\nIDs não são amigos!");
		}
		
		else{
			removeAmizade(p1, p2->raiz->id);
			removeAmizade(p2, p1->raiz->id);
			remAmz(P, p1->raiz->id, p2->raiz->id);
			printf("Amizade entre %d e %d foi removida com sucesso\n", p1->raiz->id, p2->raiz->id);
		}
		
	}else{
		
		printf("\nErro! ID(s) não encontrado(s)!\n");
		printf("\tRetornando ao menu principal...\n");
	}
		
}

void removeAmizade(struct Pessoa *P, int id){
	
	struct Nodo *anterior = NULL;
	struct Nodo *atual = P->raiz;
			
	while(atual != NULL && atual->id != id){
		//enquanto o nó atual existir e ele não possuir o ID procurado para remoção fazer:
		anterior = atual; //anterior recebe esse nó atual
		
		//se o ID procurado é menor que o ID que se encontra no atual nó então nos deslocamos para a esquerda
		if(id < atual->id){
			atual = atual->esquerda;
		}
			
		else{
			atual = atual->direita;
		}
			
	}
	
	verificarCasoDeRemocaoAmigos(P, anterior, atual); //atual guarda a posicao do id a ser removido dentro da raiz
	
}

void verificarCasoDeRemocaoAmigos(struct Pessoa *P, struct Nodo *anterior, struct Nodo *atual){
		
	if(atual->esquerda == NULL && atual->direita == NULL){
		
		removeCasoFolhaAmigos(anterior, atual);
	}
	
	else if( (atual->esquerda != NULL && atual->direita == NULL) || (atual->esquerda == NULL && atual->direita != NULL) ){
		/*Senão, se o nó atual possuir filhos ou à direita ou à esquerda
		 * então queremos remover um nó que possui um filho
		 */ 
		removeCasoUmFilhoAmigos(anterior, atual);
	}
	else if(atual->esquerda != NULL && atual->direita != NULL){
		/*Senão, se o nó atual possui filhos à esquerda E filhos à 
		 * direita...
		 */ 
		
		removeCasoDoisFilhosAmigos(P, anterior, atual);
	}
}

void removeCasoFolhaAmigos(struct Nodo *anterior, struct Nodo *atual){
	//Função invocada quando se deseja remover um nó que não possui filhos
	
	if(atual->id < anterior->id){
		/*Se o ID a ser removido for menor que o seu pai então o pai 
		 * (anterior) deve apontar sua esquerda para NULL
		 */
		anterior->esquerda = NULL;
		
	}
	
	else if(atual->id > anterior->id){
		/*Senão então ele está à direita pois é maior, então o pai
		 * (anterior) deve apontar sua direita para NULL
		 */
		 anterior->direita = NULL;
		 
	}
	
	
	//Agora podemos desalocar a memória usada pelo nó que estamos removendo 
	free(atual);
}

void removeCasoUmFilhoAmigos(struct Nodo *anterior, struct Nodo *atual){
	//Funcao invocada quando se deseja remover um nó que possui somente um filho
	
	if(atual->id < anterior->id){
		//Se o nó a ser removido está à esquerda do seu pai:
		
		if(atual->esquerda != NULL){
		//Se o nó a ser removido possui filhos somente à esquerda, então:
			anterior->esquerda = atual->esquerda;
		}

		else{
			//Senão o nó a ser removido possui filhos somente à direita, então:
			anterior->esquerda = atual->direita;
		}
		
	}
	
	else if(atual->id > anterior->id){
		//Senão, ele está à direita de seu pai
		
		if(atual->esquerda != NULL){
		//Se o nó a ser removido possui filhos somente à esquerda, então:
			anterior->direita = atual->esquerda;
		}

		else{
			//Senão o nó a ser removido possui filhos somente à direita, então:
			anterior->direita = atual->direita;
		}
	}
	//desaloca a memória usada pelo nó que removemos
	free(atual);
}

void removeCasoDoisFilhosAmigos(struct Pessoa *P, struct Nodo *anterior, struct Nodo *atual){
	//Função invocada quando se deseja remover um nó que possui dois filhos
	anterior = atual;
	struct Nodo *maior = atual->esquerda;
	//maior receberá o maior elemento a direita do primeiro filho a esquerda
	
	while(maior->direita != NULL){
		//Enquanto houver elementos à direita de maior:
		anterior = maior;
		maior = maior->direita;
	}
	
	int id = maior->id; //Variável que armazena o ID de onde maior parou
	
	verificarCasoDeRemocaoAmigos(P, anterior, maior);
    
    atual->id = id;
}	

void removerTodosAmigos(struct Arvore *P, struct Pessoa *nodo_em_remocao, struct Nodo *amigo){
	if(amigo != NULL){
		removerTodosAmigos(P, nodo_em_remocao, amigo->esquerda);
		removerTodosAmigos(P, nodo_em_remocao, amigo->direita);
		removerAmizades(P, nodo_em_remocao, posID(P->pessoa, amigo->id));
	}
}
