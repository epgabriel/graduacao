#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

void inserir(struct Arvore *pRaiz, struct Arvore *pNodo){
	
	printf("Oi eu fui chamado, estou inserindo o %d\n", pNodo->elemento);
	
	if(pRaiz == NULL){
		printf("A raiz está null... vou entrar aqui...\n");
		pRaiz = pNodo;
		pRaiz->pEsquerda = NULL;
		pRaiz->pDireita = NULL;
	}
	
	else{
		printf("Ahh.. a raiz nao é nula T-T, ela é %d\n", pRaiz->elemento);
		if(pNodo->elemento < pRaiz->elemento){
			printf("Como sou menor vou entrar na esquerda\n");
			inserir(pRaiz->pEsquerda, pNodo);
		}
		else{
			printf("Como sou maior vou entrar na direita\n");
			inserir(pRaiz->pDireita, pNodo);
		}
	}
}

void imprimir(struct Arvore *pRaiz){
	if(pRaiz == NULL) return;
	printf(" %d", pRaiz->elemento);
	imprimir(pRaiz->pEsquerda);
	imprimir(pRaiz->pDireita);
}


void desalocar(struct Arvore *pRaiz){
	
	if(pRaiz == NULL) return;
	
	desalocar(pRaiz->pEsquerda);
	desalocar(pRaiz->pDireita);
	
	free(pRaiz);
}

