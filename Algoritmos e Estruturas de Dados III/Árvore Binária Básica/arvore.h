#ifndef ARVORE_H
#define ARVORE_H

struct Arvore{
	int elemento;
	struct Arvore *pEsquerda;
	struct Arvore *pDireita;
};

void inserir(struct Arvore*, struct Arvore*);

void remover(struct Arvore*);

void imprimir(struct Arvore*);

int buscar(struct Arvore*);

void desalocar(struct Arvore*);

#endif
