#include <stdio.h>
#include "interface.h"
#include "arvore.h"

void menu(){
	printf("\n\n\t1 - Inserir Elementos na Árvore;\n"
			"\t2 - Buscar Elemento;\n"
			"\t3 - Remover Elemento;\n"
			"\t4 - Imprimir Arvore;\n"
			"\t0 - Sair\n"
			"Opcao: ");
}

void validar(int *valor, int limite){
	while(*valor < 0 || *valor > limite){
		printf("Erro! Digite um valor válido: ");
		scanf("%d",valor);
	}
	printf("\n");
}

void preencherNodo(struct Arvore *pNodo){
	printf("\nElemento a ser inserido: ");
	scanf("%d", &pNodo->elemento);
}
