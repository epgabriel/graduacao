#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"
#include "interface.h"

int main(){
	
	printf("\n==================== MENU ====================\n\n");
	
	int opcao;
	struct Arvore *pRaiz = NULL;
	
	do{		
		menu();
		scanf("%d",&opcao);
		validar(&opcao, 4); //3 é o limite de opcoes diferentes de 0
		
		if(opcao == 1){ //inserir
			struct Arvore *pNodo = malloc(sizeof(struct Arvore));
			preencherNodo(pNodo);
			inserir(pRaiz, pNodo);
		}
		
		else if(opcao == 4){
			printf("\n\nArvore: ");
			imprimir(pRaiz);
		}
	}while(opcao != 0);
	
	return 0;
}
