"""
Tabela de Símbolos.
Classe Singleton.
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.0'
__email__   = 'arcgabrieled@gmail.com'

class TabelaSimbolos:

    class __TabelaSimbolos:
        def __init__(self):
            self.__tabela = {}  # Tabela do tipo dicionario
            self.__tam    = 0   # Tamanho da Tabela
        
        def idElemento(self, elemento):
            indice = -1
            for e in self.__tabela:
                if self.__tabela[e] == elemento:
                    indice = e
                    break
            return indice
        
        def addElemento(self, elemento):
            id_e = self.idElemento(elemento)
            if id_e == -1:
                self.__tabela[self.__tam] = elemento
                self.__tam += 1
                return self.__tam - 1
            else: 
                return id_e
        
        # Imprime a tabela de símbolos
        def __str__(self):
            s = "\n\n\t:::::::::::: Tabela de Símbolos ::::::::::::\n\n"
            for i in range(self.__tam): s += "\t\t{}\t{}\n".format(i, self.__tabela[i])
            return s
                
                   

    __instance = None

    def __init__(self):
        print("\nErro.. Desculpe, mas essa classe não é instanciável, ela é estática!")
        print("Encerrando o programa...\n\n...Programa Encerrado. \n")
        exit();

    @staticmethod
    def getInstance():
        if TabelaSimbolos.__instance == None:
            TabelaSimbolos.__instance = TabelaSimbolos.__TabelaSimbolos()
        return TabelaSimbolos.__instance
