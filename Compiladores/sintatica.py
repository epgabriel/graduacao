"""
Arquivo responsável pela Análise Sintática.
Linguagem LL I
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.1'
__email__   = 'arcgabrieled@gmail.com'

from tp_token import Terminais
from tp_token import TipoToken
from tp_token import Token


class Noh:
    def __init__(self, tipo, filhos=[]):
        """Um nó é uma célula das quais é composta a árvore sintática.
        É constituído de um tipo e de nós filhos.
        
        :param1 tipo: tipo do noh
        :param2 filhos: nós filhos

        :type tipo: TipoToken
        :type filhos: list
        """
        self.__tipo   = tipo
        self.__filhos = filhos
        self.__nivel  = 0 
        self.__final  = False
        self.__valor  = None

    def setIsFinal(self, final=True):
        self.__final = final
    
    def isFinal(self):
        return self.__final
    
    def setValor(self, valor=None):
        self.__valor = valor
    
    def getValor(self):
        return self.__valor

    def add(self, noh):
        if(noh != None):
            if not isinstance(noh, Noh):
                n = Noh(TipoToken.FINAL)
                n.setIsFinal()
                n.setNivel(self.__nivel)
                n.setValor(noh)
                self.__filhos.append(n)    
            else:                    
                noh.setNivel(self.__nivel + 1) 
                self.__filhos.append(noh)
            

    def getTipo(self):
        return self.__tipo

    def getFilhos(self):
        return self.__filhos

    def getNoh(self, tipo):
        if self.__tipo == tipo: return self
        
        for noh in self.__filhos:
            if noh.getNoh(tipo) != None:
                return noh
        return None
    
    def setNivel(self, nivel=0):
        self.__nivel = nivel




    def toString(self, tab=""):
        out = "{}\nNó do tipo {}".format(tab, self.__tipo)
        
        # Para cada noh filho:
        for noh in self.__filhos: 
            out += "\t" + str(noh)
        #for noh in self.__filhos: out += str(noh)
        
        return out;

    def __calcTabs(self):
        out = ""
        for i in range(self.__nivel - 1):
            out += "|  "
        out += "\__"
        return out

    def __str__(self):
        if isinstance(self.__tipo, TipoToken):
            if self.__tipo == TipoToken.FINAL:
                return "  -->  {}".format(self.__valor)
            return "\n{}Nó do tipo {}".format(self.__calcTabs(), self.__tipo.name)
        return "\nErro\n"
        

class ArvoreSintatica:

    def __init__(self):
        """Inicializa uma árvore sintática. Para isso inicializa um nó raiz
        com o tipo token Programa."""
        self.__raiz = Noh(TipoToken.PROGRAMA)
    
    
    def __str__(self):
        out = "\n\n\t\t::Árvore Sintática::\n"
        s = str(self.__raiz.toString())
        if s != None: out += s
        return out

    
    def getRaiz(self):
        return self.__raiz







class AnaliseSintatica:

    def __init__(self, tokens):
        self.__tokens           = tokens
        self.__token_id         = 0
        self.__tabs             = ""
        self.__erros_sintaticos = []

        self.__arvore = ArvoreSintatica()
        
        

        print("\n\n:: Iniciando Análise Sintática...")
        print("\n\t\tFluxo de Tokens")
        for t in self.__tokens: print(t)

        self.PROG()

        print(self.__arvore)
    

    def match(self, lexema): 
        """
        Testa se o símbolo atual casa com o token especificado.
        Se sim, avança a leitura.
        Se não, acusa erro.
        """
        
        if self.__tokens[self.__token_id].getValor() == lexema :
            self.__token_id += 1
            print("{} {} - {} consumido".format(self.__tabs, self.__token_id, self.__tokens[self.__token_id-1]))

        else:
            self.erro("match errado -> '{}' X {}".format(lexema, self.__tokens[self.__token_id]))
            self.__token_id += 1 
        
        self.__tabs = self.__tabs[2:]
        print()
            
    

    def __prox(self):
        """
        Retorna o próximo token, mas sem avançar a leitura.
        Ou seja, dá apenas uma "olhadinha" à frente.

        Retorna None se o id de leitura for superior ao tamanho da lista de tokens
        """
        
        if self.__token_id == len(self.__tokens):
            return None

        return self.__tokens[ self.__token_id ]

    
    
    def existemTokens(self):
        pass

    def erro(self, descricao):
        print("ERRRRRROOOOOOOOOOO")
        self.__token_id += 1;
        self.__erros_sintaticos.append(descricao)

    def imprimeErros(self):
        print("\n\n\t\t:::::: Erros Sintáticos ::::::\n")
        for e in self.__erros_sintaticos : print(e)

    
    def __coordTokenAtual(self) :
        return self.__tokens[self.__token_id].getCoord()



    # ============================================================================================
    """
    PROGRAMA É UMA LISTA DE DECLARAÇÕES.
    ESSAS DECLARACAÇÕES PODEM SER DECLARAÇÕES DE VARIÁVEIS OU DE FUNÇÕES.
    
    VARIÁVEIS SÃO DECLARADAS NAS FORMAS:
        TIPO-ESP IDENT ;
        TIPO-ESP IDENT A-COLCHETE INTEIRO F-COLCHETE {A-COLCHETE INTEIRO F-COLCHETE} ;

        EXEMPLOS:        
            int op;
            float var1[3][3];

    FUNÇÕES SÃO DECLARADAS NA FORMA:
        TIPO-ESP IDENT A-PARENTESE [PARAMS] F-PARENTESE COMPOSTO-DECL

        EXEMPLOS:
            int main(){
                int op;
                float var1[3][3];
            }

            int main(int argv, char**args) {
                int op;
                float var1[3][3];
            }
    """


    def PROG (self):
        """
        Inicia a análise sintática através do fluxo de tokens definido na análise léxica.
        """
        print()
        # Programa não pode ser vazio
        if self.__prox() is None:
            self.erro("Erro Sintático! Programa vazio!")

        # reserva a raiz da arvore
        noh = self.__arvore.getRaiz()

        print(self.__tabs + "TIPO(PROG)")
        noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR) 
        noh.add(noh_tipo)       
        self.TIPO(noh_tipo) # Chama TIPO passando o pai
            

        print(self.__tabs + "IDENT(PROG)")
        noh_ident = Noh(TipoToken.IDENT)
        noh.add(noh_ident)        
        self.IDENT(noh_ident)

        print(self.__tabs + "DECL-COM(PROG)") 
        noh_declc = Noh(TipoToken.DECL_COM)
        noh.add(noh_declc)
        self.DECL_COM(noh_declc)

       
        if self.__prox() != None :
            self.__tabs = ""
            print(self.__tabs + "DECL-LIST(PROG)") 
            noh_decll = Noh(TipoToken.DECL_LIST)
            noh.add(noh_decll)
            self.DECL_LIST(noh_decll)
        


    def DECL_LIST (self, noh):
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.TIPO_ESPECIFICADOR:
            print(self.__tabs + "TIPO(DECL-LIST)")            
            noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR)
            noh.add(noh_tipo)            
            self.TIPO(noh_tipo)

            print(self.__tabs + "IDENT(DECL-LIST)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            print(self.__tabs + "DECL-COM(DECL-LIST)")
            noh_declc = Noh(TipoToken.DECL_COM)
            noh.add(noh_declc)
            self.DECL_COM(noh_declc)

            if self.__prox() != None :
                self.__tabs = "";
                print(self.__tabs + "DECL-LIST(DECL-LIST)")
                noh_decll = Noh(TipoToken.DECL_LIST)
                noh.add(noh_decll)
                self.DECL_LIST(noh_decll)
        
        elif self.__prox() != None :
            self.erro("1Erro Sintático! Espera-se um tipo especificador em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
        else:
            self.imprimeErros()
    
    def DECL_COM (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.ABRE_PARENTESE:
            noh.add(Noh(TipoToken.ABRE_PARENTESE))
            self.match("(")

            print(self.__tabs + "PARAMS(DECL-COM)")
            noh_params = Noh(TipoToken.PARAMS)
            noh.add(noh_params)
            self.PARAMS(noh_params)

            noh.add(Noh(TipoToken.FECHA_PARENTESE))
            self.match(")")

            noh.add(Noh(TipoToken.ABRE_CHAVE))
            self.match("{")

            print(self.__tabs + "LOCAL-DECL(DECL-COM)")
            noh_locald = Noh(TipoToken.LOCAL_DECLARACOES)
            noh.add(noh_locald)
            self.LOCAL_DECL(noh_locald)

            print(self.__tabs + "COM-LIST(DECL-COM)")
            noh_coml = Noh(TipoToken.COMANDO_LISTA)
            noh.add(noh_coml)
            self.COM_LIST(noh_coml)

            noh.add(Noh(TipoToken.FECHA_CHAVE))
            self.match("}")
        
        else:
            print(self.__tabs + "VET-ATR(DECL-COM)")
            noh_veta = Noh(TipoToken.VET_ATR)
            noh.add(noh_veta)
            self.VET_ATR(noh_veta)

            noh.add(Noh(TipoToken.PONTO_VIRGULA))
            self.match(";")

    
    def VAR_ATR (self, noh):
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.ABRE_COLCHETE :
            
            noh.add(Noh(TipoToken.ABRE_COLCHETE))
            self.match("[")
            
            print(self.__tabs + "EXP(VAR-ATR)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)
            
            noh.add(Noh(TipoToken.FECH))
            self.match("]")

            print(self.__tabs + "VAR-ATR(VAR-ATR)")
            noh_varatr = Noh(TipoToken.VAR_ATR)
            noh.add(noh_varatr)
            self.VAR_ATR(noh_varatr)
        
    
    def VET_ATR (self, noh) :
        self.__tabs += "  "
        if self.__prox() == TipoToken.ABRE_COLCHETE :
            noh.add(Noh(TipoToken.ABRE_COLCHETE))
            self.match('[')

            print(self.__tabs + "NUM-INT(VET-ATR)")
            noh_numint = Noh(TipoToken.NUM_INT)
            noh.add(noh_numint)
            self.NUM_INT(noh_numint)

            noh.add(Noh(TipoToken.FECHA_COLCHETE))
            self.match(']')

            print(self.__tabs + "VET-ATR(VET-ATR)")
            noh_vetatr = Noh(TipoToken.VET_ATR)
            noh.add(noh_vetatr)
            self.VET_ATR(noh_vetatr)
    
        
    def PARAMS (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.TIPO_ESPECIFICADOR :
            print(self.__tabs + "TIPO(PARAMS)")
            noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR)
            noh.add(noh_tipo)
            self.TIPO(noh_tipo)

            print(self.__tabs + "IDENT(PARAMS)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            print(self.__tabs + "PARAM-VET(PARAMS)")
            noh_paramvet = Noh(TipoToken.PARAM_VET)
            noh.add(noh_paramvet)
            self.PARAM_VET(noh_paramvet)

            print(self.__tabs + "PARAM-LIST(PARAMS)")
            noh_paraml = Noh(TipoToken.PARAM_LIST)
            noh.add(noh_paraml)
            self.PARAM_LIST(noh_paraml)
       
    
    def PARAM_LIST (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.VIRGULA :
            noh.add(Noh(TipoToken.VIRGULA))
            self.match(',')

            print(self.__tabs + "TIPO(PARAM-LIST)")
            noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR)
            noh.add(noh_tipo)
            self.TIPO(noh_tipo)

            print(self.__tabs + "IDENT(PARAM-LIST)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            print(self.__tabs + "PARAM-VET(PARAM-LIST)")
            noh_paramvet = Noh(TipoToken.PARAM_VET)
            noh.add(noh_paramvet)
            self.PARAM_VET(noh_paramvet)

            print(self.__tabs + "PARAM-LIST(PARAM-LIST)")
            noh_paraml = Noh(TipoToken.PARAM_LIST)
            noh.add(noh_paraml)
            self.PARAM_LIST(noh_paraml)
    
    def PARAM_VET (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.ABRE_COLCHETE :
            noh.add(Noh(TipoToken.ABRE_COLCHETE))
            self.match('[')
            noh.add(Noh(TipoToken.FECHA_COLCHETE))
            self.match(']')
    
    

    def TIPO (self, noh) :
        self.__tabs += "  "
        
        if   self.__prox().getValor() == "int"    : 
            noh.add(Noh(TipoToken.INT))
            self.match("int")
        
        elif self.__prox().getValor() == "float"  :
            noh.add(Noh(TipoToken.FLOAT))
            self.match("float")
        
        elif self.__prox().getValor() == "char"   : 
            noh.add(Noh(TipoToken.CHAR))
            self.match("char")
        
        elif self.__prox().getValor() == "void"   : 
            noh.add(Noh(TipoToken.VOID))
            self.match("void")
       
        elif self.__prox().getValor() == "struct" :
            noh.add(Noh(TipoToken.STRUCT))
            self.match("struct")
            
            print(self.__tabs + "IDENT(TIPO)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)
            
            noh.add(Noh(TipoToken.ABRE_CHAVE))
            self.match("{")
            
            print(self.__tabs + "TIPO(TIPO)")
            noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR)
            noh.add(noh_tipo)
            self.TIPO(noh_tipo)

            print(self.__tabs + "IDENT(TIPO)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            print(self.__tabs + "VET-ATR(TIPO)")
            noh_vetatr = Noh(TipoToken.VET_ATR)
            noh.add(noh_vetatr)
            self.VET_ATR(noh_vetatr)

            noh.add(Noh(TipoToken.PONTO_VIRGULA))
            self.match(";")
            
            print(self.__tabs + "LOCAL-DECL(TIPO)")
            noh_locald = Noh(TipoToken.LOCAL_DECLARACOES)
            noh.add(noh_locald)
            self.LOCAL_DECL(noh_locald)

            noh.add(Noh(TipoToken.FECHA_CHAVE))
            self.match("}")
        
        else:
            self.erro("2Erro Sintático! Espera-se um tipo especificador em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
  
    def LOCAL_DECL (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.TIPO_ESPECIFICADOR :
            print(self.__tabs + "TIPO(TIPO)")
            noh_tipo = Noh(TipoToken.TIPO_ESPECIFICADOR)
            noh.add(noh_tipo)
            self.TIPO(noh_tipo)

            print(self.__tabs + "IDENT(TIPO)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            print(self.__tabs + "VET-ATR(TIPO)")
            noh_vetatr = Noh(TipoToken.VET_ATR)
            noh.add(noh_vetatr)
            self.VET_ATR(noh_vetatr)

            noh.add(Noh(TipoToken.PONTO_VIRGULA))
            self.match(";")
            
            print(self.__tabs + "LOCAL-DECL(TIPO)")
            noh_locald = Noh(TipoToken.LOCAL_DECLARACOES)
            noh.add(noh_locald)
            self.LOCAL_DECL(noh_locald)
    
    def COM_LIST (self, noh) :
        self.__tabs += "  "
        if (self.__prox().getTipo() == TipoToken.PALAVRA_RESERVADA 
            or self.__prox().getTipo() == TipoToken.ABRE_PARENTESE 
            or self.__prox().getTipo() == TipoToken.ABRE_CHAVE
            or self.__prox().getTipo() == TipoToken.NUM_INT 
            or self.__prox().getTipo() == TipoToken.IDENT
            or self.__prox().getTipo() == TipoToken.NUM) :
            print(self.__tabs + "COMANDO(COM-LIST)")
            noh_comando = Noh(TipoToken.COMANDO)
            noh.add(noh_comando)
            self.COMANDO(noh_comando)

            print(self.__tabs + "COM-LIST(COM-LIST)")
            noh_coml = Noh(TipoToken.COMANDO_LISTA)
            noh.add(noh_coml)
            self.COM_LIST(noh_coml)



    def COMANDO (self, noh) : 
        self.__tabs += "  "       
        if self.__prox().getTipo () == TipoToken.ABRE_CHAVE :
            noh.add(Noh(TipoToken.ABRE_CHAVE)) 
            self.match("{")

            print(self.__tabs + "LOCAL-DECL(LOCAL-DECL)")
            noh_locald = Noh(TipoToken.LOCAL_DECLARACOES)
            noh.add(noh_locald)
            self.LOCAL_DECL(noh_locald)

            print(self.__tabs + "COM-LIST(LOCAL-DECL)")
            noh_coml = Noh(TipoToken.COMANDO_LISTA)
            noh.add(noh_coml)
            self.COM_LIST(noh_coml)

            noh.add(Noh(TipoToken.FECHA_CHAVE))
            self.match("}")

        elif self.__prox().getValor() == "if"     : 
            noh.add(Noh(TipoToken.SE))
            self.match("if")

            noh.add(Noh(TipoToken.ABRE_PARENTESE))
            self.match("(")

            print(self.__tabs + "EXP(COMANDO)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)

            noh.add(Noh(TipoToken.FECHA_PARENTESE))
            self.match(")")
            
            print(self.__tabs + "COMANDO(COMANDO)")
            noh_comando = Noh(TipoToken.COMANDO)
            noh.add(noh_comando)
            self.COMANDO(noh_comando)

            print(self.__tabs + "SENAO-DECL(COMANDO)")
            noh_senald = Noh(TipoToken.SENAO_DECL)
            noh.add(noh_senald)
            self.SENAO_DECL(noh_senald)

        elif self.__prox().getValor() == "while"  : 
            noh.add(Noh(TipoToken.ENQUANTO))
            self.match("while")
            
            noh.add(Noh(TipoToken.ABRE_PARENTESE))
            self.match("(")
            
            print(self.__tabs + "EXP(COMANDO)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)

            noh.add(Noh(TipoToken.FECHA_PARENTESE))
            self.match(")")
            
            print(self.__tabs + "COMANDO(COMANDO)")
            noh_comando = Noh(TipoToken.COMANDO)
            noh.add(noh_comando)
            self.COMANDO(noh_comando)

        elif self.__prox().getValor() == "return" :
            noh.add(Noh(TipoToken.RETORNO))
            self.match("return")

            print(self.__tabs + "EXP-ATR(COMANDO)")
            noh_expa = Noh(TipoToken.EXP_ATR)
            noh.add(noh_expa)
            self.EXP_ATR(noh_expa)

            noh.add(Noh(TipoToken.PONTO_VIRGULA))
            self.match(";")

        elif self.__prox().getTipo () in [TipoToken.IDENT, TipoToken.ABRE_PARENTESE, TipoToken.NUM, TipoToken.NUM_INT] : 
            print(self.__tabs + "EXP-ATR(COMANDO)")
            noh_expa = Noh(TipoToken.EXP_ATR)
            noh.add(noh_expa)
            self.EXP_ATR(noh_expa)

            noh.add(Noh(TipoToken.PONTO_VIRGULA))
            self.match(";")

        else :
            self.erro("3Erro Sintático! Espera-se um comando em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
        
    
   
    def SENAO_DECL (self, noh) :
        self.__tabs += "  "
        if self.__prox().getValor() == "else":
            noh.add(Noh(TipoToken.SENAO))
            self.match("else")

            print(self.__tabs + "COMANDO(SENAO-DECL)")
            noh_comando = Noh(TipoToken.COMANDO)
            noh.add(noh_comando)
            self.COMANDO(noh_comando)
    


    def EXP (self, noh) :
        self.__tabs += "  "
       
        if self.__prox().getTipo() == TipoToken.IDENT:
            
            print(self.__tabs + "IDENT(EXP)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)
            
            if self.__prox().getTipo() == TipoToken.ABRE_COLCHETE:

                print(self.__tabs + "VAR-ATR(EXP)")
                noh_vata = Noh(TipoToken.VAR_ATR)
                noh.add(noh_vata)
                self.VAR_ATR(noh_vata)

                if self.__prox().getTipo() == TipoToken.ATRIBUICAO:
                    noh.add(Noh(TipoToken.RECEBE))
                    self.match("=")

                    print(self.__tabs + "EXP(EXP)")
                    noh_exp = Noh(TipoToken.EXPRESSAO)
                    noh.add(noh_exp)
                    self.EXP(noh_exp)

                else :

                    print(self.__tabs + "EXP-SOMA-L(EXP)")
                    noh_somal = Noh(TipoToken.EXP_SOMA_L)
                    noh.add(noh_somal)
                    self.EXP_SOMA_L(noh_somal)

                    print(self.__tabs + "EXP-SOMA-C(EXP)")
                    noh_exps = Noh(TipoToken.EXP_SIMP_C)
                    noh.add(noh_exps)
                    self.EXP_SIMP_C(noh_exps)
            
            elif self.__prox().getTipo() == TipoToken.ATRIBUICAO :
                noh.add(Noh(TipoToken.RECEBE))
                self.match("=")

                print(self.__tabs + "EXP(EXP)")
                noh_exp = Noh(TipoToken.EXPRESSAO)
                noh.add(noh_exp)
                self.EXP(noh_exp)

            elif self.__prox().getTipo() == TipoToken.ABRE_PARENTESE:
                noh.add(Noh(TipoToken.ABRE_PARENTESE))
                self.match("(")

                print(self.__tabs + "ARGS(EXP)")
                noh_args = Noh(TipoToken.ARGS)
                noh.add(noh_args)
                self.ARGS(noh_args)

                noh.add(Noh(TipoToken.FECHA_PARENTESE))
                self.match(")")

                print(self.__tabs + "EXP-SOMA-L(EXP)")
                noh_somal = Noh(TipoToken.EXP_SOMA_L)
                noh.add(noh_somal)
                self.EXP_SOMA_L(noh_somal)

                print(self.__tabs + "EXP-SOMA-C(EXP)")
                noh_exps = Noh(TipoToken.EXP_SIMP_C)
                noh.add(noh_exps)
                self.EXP_SIMP_C(noh_exps)
            
            else :
                print(self.__tabs + "EXP-SOMA-L(EXP)")
                noh_somal = Noh(TipoToken.EXP_SOMA_L)
                noh.add(noh_somal)
                self.EXP_SOMA_L(noh_somal)

                print(self.__tabs + "EXP-SOMA-C(EXP)")
                noh_exps = Noh(TipoToken.EXP_SIMP_C)
                noh.add(noh_exps)
                self.EXP_SIMP_C(noh_exps)

        
        elif self.__prox().getTipo() == TipoToken.ABRE_PARENTESE :
            noh.add(Noh(TipoToken.ABRE_PARENTESE))
            self.match("(")
            
            print(self.__tabs + "EXP(EXP)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)

            noh.add(Noh(TipoToken.FECHA_PARENTESE))
            self.match(")")
            
            print(self.__tabs + "EXP-SOMA-L(EXP)")
            noh_somal = Noh(TipoToken.EXP_SOMA_L)
            noh.add(noh_somal)
            self.EXP_SOMA_L(noh_somal)

            print(self.__tabs + "EXP-SOMA-C(EXP)")
            noh_exps = Noh(TipoToken.EXP_SIMP_C)
            noh.add(noh_exps)
            self.EXP_SIMP_C(noh_exps)
    
        elif self.__prox().getTipo() == TipoToken.NUM :

            print(self.__tabs + "NUM(EXP)")
            noh_num = Noh(TipoToken.NUM)
            noh.add(noh_num)
            self.NUM(noh_num)

            print(self.__tabs + "EXP-SOMA-L(EXP)")
            noh_somal = Noh(TipoToken.EXP_SOMA_L)
            noh.add(noh_somal)
            self.EXP_SOMA_L(noh_somal)

            print(self.__tabs + "EXP-SOMA-C(EXP)")
            noh_exps = Noh(TipoToken.EXP_SIMP_C)
            noh.add(noh_exps)
            self.EXP_SIMP_C(noh_exps)
        
        elif self.__prox().getTipo() == TipoToken.NUM_INT :

            print(self.__tabs + "NUM-INT(EXP)")
            noh_nint = Noh(TipoToken.NUM_INT)
            noh.add(noh_nint)
            self.NUM_INT(noh_nint)

            print(self.__tabs + "EXP-SOMA-L(EXP)")
            noh_somal = Noh(TipoToken.EXP_SOMA_L)
            noh.add(noh_somal)
            self.EXP_SOMA_L(noh_somal)

            print(self.__tabs + "EXP-SOMA-C(EXP)")
            noh_exps = Noh(TipoToken.EXP_SIMP_C)
            noh.add(noh_exps)
            self.EXP_SIMP_C(noh_exps)

        else:
            self.erro("4Erro Sintático! Espera-se uma expressão em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))

    def EXP_ATR (self, noh):
        self.__tabs += "  "
        if self.__prox().getTipo() in [TipoToken.ABRE_PARENTESE, TipoToken.IDENT, TipoToken.NUM, TipoToken.NUM_INT] :
            print(self.__tabs + "EXP(EXP-ATR)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)
    

    def EXP_SIMP_C (self, noh) :
        self.__tabs += "  "
        
        if self.__prox().getTipo() == TipoToken.RELACIONAL :
            
            print(self.__tabs + "REL(EXP-SOMA-C)")
            noh_rel = Noh(TipoToken.REL)
            noh.add(noh_rel)
            self.REL(noh_rel)

            print(self.__tabs + "TERMO(EXP-SOMA-C)")
            noh_termo = Noh(TipoToken.TERMO)
            noh.add(noh_termo)
            self.TERMO(noh_termo)

            print(self.__tabs + "EXP-SOMA-L(EXP)")
            noh_somal = Noh(TipoToken.EXP_SOMA_L)
            noh.add(noh_somal)
            self.EXP_SOMA_L(noh_somal)
    
    def EXP_SOMA_L (self, noh) :
        self.__tabs += "  "

        if self.__prox().getTipo() == TipoToken.SOMA :
            print(self.__tabs + "SOMA(EXP-SOMA-L)")
            noh_soma = Noh(TipoToken.SOMA)
            noh.add(noh_soma)
            self.SOMA(noh_soma)

            print(self.__tabs + "TERMO(EXP-SOMA-L)")
            noh_termo = Noh(TipoToken.TERMO)
            noh.add(noh_termo)
            self.TERMO(noh_termo)

            print(self.__tabs + "EXP-SOMA-L(EXP)")
            noh_somal = Noh(TipoToken.EXP_SOMA_L)
            noh.add(noh_somal)
            self.EXP_SOMA_L(noh_somal)
       
       # elif self.__prox().getTipo() == TipoToken.MULT:
       #     print(self.__tabs + "SOMA(EXP-SOMA-L)")
       #    noh_mult = Noh(TipoToken.MULT)
       #     noh.add(noh_mult)
       #     self.MULT(noh_mult)

       #     print(self.__tabs + "TERMO(EXP-SOMA-L)")
       #     noh_termo = Noh(TipoToken.TERMO)
       #     noh.add(noh_termo)
       #     self.TERMO(noh_termo)

       #     print(self.__tabs + "EXP-MULT-L(EXP)")
       #     noh_multl = Noh(TipoToken.EXP_MULT_L)
       #     noh.add(noh_multl)
       #     self.EXP_MULT_L(noh_multl)

    def EXP_MULT_L(self, noh):
        if self.__prox().getTipo() == TipoToken.MULT:
            print(self.__tabs + "SOMA(EXP-SOMA-L)")
            noh_mult = Noh(TipoToken.MULT)
            noh.add(noh_mult)
            self.MULT(noh_mult)

            print(self.__tabs + "TERMO(EXP-SOMA-L)")
            noh_termo = Noh(TipoToken.TERMO)
            noh.add(noh_termo)
            self.TERMO(noh_termo)

            print(self.__tabs + "EXP-MULT-L(EXP)")
            noh_multl = Noh(TipoToken.EXP_MULT_L)
            noh.add(noh_multl)
            self.EXP_MULT_L(noh_multl)

    
    def REL (self, noh) :
        self.__tabs += "  "

        if self.__prox().getTipo() == TipoToken.RELACIONAL :
            if   self.__prox().getValor() == ">=":
                noh.add(Noh(TipoToken.MAIOR_IGUAL))
            elif self.__prox().getValor() == "<=":
                noh.add(Noh(TipoToken.MENOR_IGUAL))
            elif self.__prox().getValor() == ">" :
                noh.add(Noh(TipoToken.MAIOR))
            elif self.__prox().getValor() == "<" :
                noh.add(Noh(TipoToken.MENOR))
            elif self.__prox().getValor() == "==":
                noh.add(Noh(TipoToken.IGUAL))
            elif self.__prox().getValor() == "!=":
                noh.add(Noh(TipoToken.DIFERENTE))

            self.match( self.__prox().getValor() )

        else :

            self.erro("5Erro Sintático! Espera-se um operador relacional em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
    

    def TERMO (self, noh) :
        self.__tabs += "  "

        if self.__prox().getTipo() in [TipoToken.ABRE_PARENTESE, TipoToken.IDENT, TipoToken.NUM, TipoToken.NUM_INT] :
        
            print(self.__tabs + "FATOR(TERMO)")
            noh_fator = Noh(TipoToken.FATOR)
            noh.add(noh_fator)
            self.FATOR(noh_fator)

            print(self.__tabs + "TERMO-LIST(TERMO)")
            noh_termol = Noh(TipoToken.TERMO_LIST)
            noh.add(noh_termol)
            self.TERMO_LIST(noh_termol)

        else:

            self.erro("6Erro Sintático! Espera-se um termo em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
    
    def TERMO_LIST (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.MULT :
            print(self.__tabs + "MULT(TERMO-LIST)")
            noh_mult = Noh(TipoToken.MULT)
            noh.add(noh_mult)
            self.MULT(noh_mult)

            print(self.__tabs + "FATOR(TERMO-LIST)")
            noh_fator = Noh(TipoToken.FATOR)
            noh.add(noh_fator)
            self.FATOR(noh_fator)

            print(self.__tabs + "TERMO-LIST(TERMO-LIST)")
            noh_termol = Noh(TipoToken.TERMO_LIST)
            noh.add(noh_termol)
            self.TERMO_LIST()
    
    def FATOR (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.ABRE_PARENTESE :
            noh.add(Noh(TipoToken.ABRE_PARENTESE))
            self.match('(')

            print(self.__tabs + "EXP(FATOR)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)
            
            noh.add(Noh(TipoToken.FECHA_PARENTESE))
            self.match(')')

        elif self.__prox().getTipo() == TipoToken.NUM: 
            print(self.__tabs + "NUM(FATOR)")
            noh_num = Noh(TipoToken.NUM)
            noh.add(noh_num)
            self.NUM(noh_num)
        
        elif self.__prox().getTipo() == TipoToken.NUM_INT:
            print(self.__tabs + "NUM-INT(FATOR)")
            noh_nint = Noh(TipoToken.NUM_INT)
            noh.add(noh_nint)
            self.NUM_INT(noh_nint)

        elif self.__prox().getTipo() == TipoToken.IDENT :
            print(self.__tabs + "IDENT(FATOR)")
            noh_ident = Noh(TipoToken.IDENT)
            noh.add(noh_ident)
            self.IDENT(noh_ident)

            if self.__prox().getTipo() == TipoToken.ABRE_PARENTESE :
                noh.add(Noh(TipoToken.ABRE_CHAVE))
                self.match("(")

                print(self.__tabs + "ARGS(FATOR)")
                noh_args = Noh(TipoToken.ARGS)
                noh.add(noh_args)
                self.ARGS(noh_args)

                noh.add(Noh(TipoToken.FECHA_PARENTESE))
                self.match(")")
            
            elif self.__prox().getTipo() == TipoToken.ABRE_COLCHETE :
                print(self.__tabs + "VAR-ATR(FATOR)")
                noh_vara = Noh(TipoToken.VAR_ATR)
                noh.add(noh_vara)
                self.VAR_ATR(noh_vara)

        else : 
            self.erro("7Erro Sintático! Espera-se um fator em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
    
    def SOMA (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.SOMA : 
            if self.__prox().getValor() == "+":
                noh.add(Noh(TipoToken.ADD))
            else:
                noh.add(Noh(TipoToken.SUB))
            self.match(self.__prox().getValor())
        else : self.erro("8Erro Sintático! Espera-se um operador de soma em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
    
    def MULT (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.MULT : 
            if self.__prox().getValor() == "*":
                noh.add(Noh(TipoToken.MUL))
            else:
                noh.add(Noh(TipoToken.DIV))
            self.match(self.__prox())
        else : self.erro("9Erro Sintático! Espera-se um operador de multiplicação em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))

    
    def ARGS (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() in [TipoToken.ABRE_PARENTESE, TipoToken.IDENT, TipoToken.NUM, TipoToken.NUM_INT] :
            print(self.__tabs + "EXP(ARGS)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)

            print(self.__tabs + "ARG-LIST(ARGS)")
            noh_argl = Noh(TipoToken.ARG_LISTA)
            noh.add(noh_argl)
            self.ARG_LIST(noh_argl)
    
    def ARG_LIST (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.VIRGULA :
            noh.add(Noh(TipoToken.VIRGULA))
            self.match(',')

            print(self.__tabs + "EXP(ARG-LIST)")
            noh_exp = Noh(TipoToken.EXPRESSAO)
            noh.add(noh_exp)
            self.EXP(noh_exp)
            self.EXP()

            print(self.__tabs + "ARG-LIST(ARG-LIST)")
            noh_argl = Noh(TipoToken.ARG_LISTA)
            noh.add(noh_argl)
            self.ARG_LIST(noh_argl)
    

    def NUM (self, noh) :   
        self.__tabs += "  "     
        if self.__prox().getTipo() == TipoToken.NUM :
            noh.add( self.__prox().getValor() ) 
            self.match( self.__prox().getValor() )
        else : self.erro("10Erro Sintático! Espera-se uma constante numérica flutuante em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))
    

    def NUM_INT (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.NUM_INT:
            noh.add( self.__prox().getValor() ) 
            self.match( self.__prox().getValor() )
        else: self.erro("11Erro Sintático! Espera-se uma constante numérica inteira em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox()))    

    def IDENT (self, noh) :
        self.__tabs += "  "
        if self.__prox().getTipo() == TipoToken.IDENT : 
            noh.add( self.__prox().getValor() )
            self.match( self.__prox().getValor() )
        else : self.erro("12Erro Sintático! Espera-se um identificador em {} mas foi encontrado {}".format(self.__coordTokenAtual(), self.__prox())) 

    
    def __str__(self):
        retorno = "\n\n\t:::::: Árvore Sintática ::::::\n"
        s = self.__arvore.getRaiz().toString()
        if s != None:
            retorno += s
        retorno += "\n\n\t:::::: Erros Sintáticos ::::::\n"
        if len(self.__erros_sintaticos) == 0:
            return retorno + "\n\tNenhum erro encontrado.\n"
        for e in self.__erros_sintaticos : retorno += e + '\n'
        return retorno