#!/usr/bin/env python3
"""
Arquivo responsável pela Análise Semântica.
Linguagem LL I
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.0'
__email__   = 'arcgabrieled@gmail.com'

import subprocess

class AnaliseSemantica:

    def __init__(self, nome_arq):
        self.__nome_arq = nome_arq
        self.__nome_dest = nome_arq + ".tm"

        print("\n\n\t:::: Gerando código de 3 endereços...")

        # faz a chamada do make da api
        try: subprocess.call("make", stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except: print("\nErro na chamada da api de geração de código de 3 endereços... \n")

        # Executa a api de geração de código de 3 endereços
        try: subprocess.call(["./main",nome_arq], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except: print("\nErro na execuçao da api de geração de código de 3 endereços... \n")

        # apaga os arquivos extras criados pela api
        try: subprocess.call(["make", "clean"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        except: print("\nErro na remoção de arquivos extras...\n")

        print("\n\t... Processo finalizado:::\n\n")


    # Imprime o código de três endereços gerado
    def __str__(self):
        try:
            arquivo = open(self.__nome_dest, "r")
            entrada = arquivo.readlines()
            retorno = "\n\n\t::: Código de 3 endereços gerado :::\n\n"
            for l in entrada:
                retorno += l
            retorno += "\n"
            arquivo.close()
            return retorno

        except IOError:
            return "\nErro na leitura do arquivo!\nTente abrir manualmente pelo diretório.\nArquivo: " + self.__nome_dest + "\n\n"

        