"""
Esse arquivo contém classes envolvendo o uso de Tokens.
Essas classes estão relacionadas ao tipo do token, aos símbolos terminais e ao próprio token.
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.0'
__email__   = 'arcgabrieled@gmail.com'




from enum import Enum

class TipoToken(Enum):
    """Enum para tipos de Token"""
    PROGRAMA=0
    DECLARACAO_LISTA=1
    DECLARACAO=2
    VAR=3
    VAR_DECLARACAO=4
    FUN_DECLARACAO=5
    ATRIBUTOS_DECLARACAO=6
    LOCAL_DECLARACOES=7
    EXPRESSAO_DECLARACOES=8
    SELECAO_DECL=9
    ITERACAO_DECL=10
    RETORNO_DECL=11
    COMPOSTO_DECL=12
    TIPO_ESPECIFICADOR=13
    PARAM=14
    PARAMS=15
    PARAMS_LISTA=16
    COMANDO=17
    COMANDO_LISTA=18
    EXPRESSAO=19
    EXPRESSAO_SOMA=20
    EXPRESSAO_SIMPLES=21
    RELACIONAL=22
    SOMA=23
    MULT=24
    FATOR=25
    TERMO=26
    ATIVACAO=27
    ARGS=28
    ARG_LISTA=29
    NUM=30
    NUM_INT=31
    LETRA=32
    DIGITO=33
    ABRE_CHAVE=34
    ABRE_COLCHETE=35
    ABRE_PARENTESE=36
    FECHA_PARENTESE=37
    FECHA_COLCHETE=38
    FECHA_CHAVE=39
    PALAVRA_RESERVADA=40
    ATRIBUICAO=41
    PONTO_VIRGULA=42
    VIRGULA=43
    PONTO=44
    LITERAL=45
    IDENT=46
    FIM_PROG = 47
    
    # Tipos de token não primitivos
    DECL_COM   =  48
    DECL_LIST  =  49
    VET_ATR    =  50
    VAR_ATR    =  51
    PARAM_VET  =  52
    PARAM_LIST =  53

    INT        = 54
    FLOAT      = 55
    CHAR       = 56
    VOID       = 57
    STRUCT     = 58

    SE         = 59
    SENAO_DECL = 60
    SENAO      = 61

    ENQUANTO   = 62

    RETORNO    = 63
    EXP_ATR    = 64

    RECEBE     = 65

    EXP_SOMA_L = 66
    EXP_MULT_L = 67
    EXP_SIMP_C = 68

    REL        = 69
    MAIOR_IGUAL= 70
    MENOR_IGUAL= 71
    MAIOR      = 72
    MENOR      = 73
    IGUAL      = 74
    DIFERENTE  = 75
    TERMO_LIST = 76

    ADD = 77
    SUB = 78
    MUL = 79
    DIV = 80

    FINAL = 81


class Terminais:
    def valores(tipoToken):
        if(tipoToken == TipoToken.LETRA):
            return ['a', 'b', 'c', 'd', 'e', 'f', 'g','h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's','t', 'u', 'v', 'w', 'x', 'y', 'z']

        elif (tipoToken == TipoToken.DIGITO):
            return ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        
        elif (tipoToken == TipoToken.TIPO_ESPECIFICADOR):
            return ["int", "float", "char", "void", "struct"]
        
        elif (tipoToken == TipoToken.RELACIONAL):
            return ["<=", ">=", '>', '<', "==", "!="]
        
        elif (tipoToken == TipoToken.SOMA):
            return ['+', '-']
        
        elif (tipoToken == TipoToken.MULT):
            return ['*', '/']
        
        elif (tipoToken == TipoToken.ABRE_CHAVE):
            return ['{']

        elif (tipoToken == TipoToken.ABRE_COLCHETE):
            return ['[']
        
        elif (tipoToken == TipoToken.ABRE_PARENTESE):
            return ['(']
        
        elif (tipoToken == TipoToken.FECHA_PARENTESE):
            return [')']
        
        elif (tipoToken == TipoToken.FECHA_COLCHETE):
            return [']']
        
        elif (tipoToken == TipoToken.FECHA_CHAVE):
            return ['}']
        
        elif (tipoToken == TipoToken.ATRIBUICAO):
            return ['=']
        
        elif (tipoToken == TipoToken.PONTO_VIRGULA):
            return [';']

        elif (tipoToken == TipoToken.VIRGULA):
            return [',']
        
        elif (tipoToken == TipoToken.PONTO):
            return ['.']

        elif (tipoToken == TipoToken.PALAVRA_RESERVADA):
            return Terminais.valores(TipoToken.TIPO_ESPECIFICADOR) + ["if", "else", "while", "return"]
        
        else:
            return None

class Token:

    def __init__(self, tipo, valor):
        self.__tipo   = tipo
        self.__valor  = valor
        self.__linha  = 0
        self.__coluna = 0
        print("<{},{}>".format(self.__tipo.name, self.__valor))
    
    def getTipo(self) : 
        return self.__tipo
    
    def getValor(self) :
        return self.__valor
    
    def setLinha(self, linha) :
        self.__linha = linha
    
    def getLinha(self) :
        return self.__linha
    
    def setColuna(self, coluna) :
        self.__coluna = coluna
    
    def getColuna(self) :
        return self.__coluna

    def getCoord(self) :
        return "Linha {} , Coluna {}".format(self.__linha, self.__coluna)
    
    def __str__(self):
        return "<{},{}> || {} ||".format(self.__tipo.name, self.__valor, self.getCoord())

