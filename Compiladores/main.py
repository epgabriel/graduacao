#input: utf-8
"""
Programa principal. Realiza a leitura do arquivo de entrada e gerencia as análises.
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.0'
__email__   = 'arcgabrieled@gmail.com'


import sys

from lexica         import AnaliseLexica    as ALe
from sintatica      import AnaliseSintatica as ASi
from semantica      import AnaliseSemantica as ASe
from tabelaSimbolos import TabelaSimbolos   as TabSimb

# Lê o arquivo de entrada e a retorna formatada
def getCodigoFonte(nome_arq):
    try:
        arquivo = open(nome_arq, "r")
        entrada = arquivo.readlines()
        arquivo.close()
        print(entrada)
        return entrada
    
    except IOError:
        print("\nErro na leitura do arquivo!\n")
        return None


def menu():
    op = 0
    print("\n\n                     ::======::\n                        MENU\n\n")
    print("\t1 - Imprimir Tabela de Símbolos")
    print("\t2 - Imprimir Análise Léxica")
    print("\t3 - Imprimir Análise Sintática")
    print("\t4 - Imprimir Análise Semântica")
    print("\t5 - Sair")
    while op < 1 or op > 5:
        op = int(input("\t\tOpção: "))
    return op
    

if __name__ == "__main__":

    if len(sys.argv) <= 1:
        print("Erro! Nenhum arquivo de entrada fornecido!")
        exit() 
        
    else:
        # Tenta realizar a leitura do código fonte pela entrada fornecida na chamada do programa
        nome_arq = sys.argv[1]
        codigo_fonte = getCodigoFonte(nome_arq)
        
        # Se ocorreu algum erro durante a leitura então encerra o programa
        if codigo_fonte == None: exit()
        
        # Tempo de definição de cada token, usado para questões de revisão.
        tempo_lex = 0 if len(sys.argv) <= 2 else sys.argv[2]

        #Senão, prossegue para as Análises:

        # Análise léxica
        analise_lex = ALe(codigo_fonte, tempo_lex)

        # Análise sintática
        analise_sin = ASi(analise_lex.getTokens())

        # Análise semântica
        analise_sem = ASe(nome_arq)


        # Loop Principal de Interação com o Usuário
        op = 0
        while op != 5:
            if op == 1:
                print(TabSimb.getInstance())
            
            elif op == 2:
                print(analise_lex)
            
            elif op == 3:
                print(analise_sin)
            
            elif op == 4:
                print(analise_sem)
            
            op = menu()
        
        print("\n\n\n\t\t:: ~ Bye ~ ::\n\n")
        
        
        
        



    
    