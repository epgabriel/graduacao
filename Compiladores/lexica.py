"""
Arquivo responsável pela Análise Léxica.
"""

__author__  = 'Gabriel Edmilson Pinto'
__version__ = '0.0'
__email__   = 'arcgabrieled@gmail.com'

import time

from tp_token import Token
from tp_token import TipoToken as Tipo
from tp_token import Terminais as Term

from tabelaSimbolos import TabelaSimbolos as TabSimb

class ErroLexico:

    def __init__(self, linha, coluna, erro):
        self.__linha  = linha
        self.__coluna = coluna
        self.__erro   = erro
        print(self)
    
    def __str__(self):
        return "\tLinha {}  Coluna {}\t->\t{}".format(self.__linha, self.__coluna, self.__erro)


class AnaliseLexica:

    def __init__(self, codigo_fonte, tempo):
        self.__fluxo_tokens  = [] # Lista de Tokens encontrados
        self.__erros_lexicos = [] # Lista de Erros Léxicos Encontrados
        self.__codigo_fonte  = codigo_fonte 
        self.__tempo         = float(tempo)
        self.__ent_formatada = self.formataEntrada(codigo_fonte) # Entrada Formatada Lista de Linhas
        
        for e in self.__ent_formatada:
           print("\t\t{}".format(e))

        # Inicia o Fluxo de Tokens
        self.__fluxoTokens()


    def addErro(self, erro):
        self.__erros_lexicos.append(erro)
    
    def getErros(self):
        return self.__erros_lexicos
    
    def getTokens(self):
        return self.__fluxo_tokens

    def getEntFormatada(self):
        return self.__ent_formatada
    
    def imprimeErro(self, ind_erro):
        if ind_erro < len(self.__erros_lexicos): print(self.__erros_lexicos[ind_erro])
        else: print("\nErro ao imprimir erro! Indice inválido.\n")

    
    def __defineToken(self, lexema):
        time.sleep(self.__tempo)
        print()
        print(lexema)
        token = None

        # Lexemas individuais
        if   lexema == '{': return Token(Tipo.ABRE_CHAVE, lexema)
        elif lexema == '[': return Token(Tipo.ABRE_COLCHETE, lexema)
        elif lexema == '(': return Token(Tipo.ABRE_PARENTESE, lexema)
        elif lexema == ')': return Token(Tipo.FECHA_PARENTESE, lexema)
        elif lexema == ']': return Token(Tipo.FECHA_COLCHETE, lexema)
        elif lexema == '}': return Token(Tipo.FECHA_CHAVE, lexema)
        elif lexema == '.': return Token(Tipo.PONTO, lexema)
        elif lexema == ';': return Token(Tipo.PONTO_VIRGULA, lexema)
        elif lexema == '=': return Token(Tipo.ATRIBUICAO, lexema)
        elif lexema == ',': return Token(Tipo.VIRGULA, lexema)
        
        # Lexemas agrupaveis
        elif lexema in Term.valores(Tipo.TIPO_ESPECIFICADOR): return Token(Tipo.TIPO_ESPECIFICADOR, lexema)
        elif lexema in Term.valores(Tipo.PALAVRA_RESERVADA):  return Token(Tipo.PALAVRA_RESERVADA, lexema)
        elif lexema in Term.valores(Tipo.RELACIONAL): return Token(Tipo.RELACIONAL, lexema)
        elif lexema in Term.valores(Tipo.SOMA): return Token(Tipo.SOMA, lexema)
        elif lexema in Term.valores(Tipo.MULT): return Token(Tipo.MULT, lexema)
        
        # Se o lexema chegou até aqui começando com uma letra, então ele é um identificador
        elif lexema[0] in Term.valores(Tipo.LETRA): return Token(Tipo.IDENT, TabSimb.getInstance().addElemento(lexema))

        # Constantes Literais - Strings
        elif lexema[0] == '"': # Lexema começa com aspas...
            # ... e termina com aspas, é uma constante literal, retorna o token
            if lexema[len(lexema) - 1] == '"': return Token(Tipo.LITERAL, TabSimb.getInstance().addElemento(lexema))
            
            #... mas não termina com aspas, retorna erro
            else: return ("Está faltando fechamento de \"", 1, 0)
        
        # Lexema não começa com aspas, mas termina com aspas
        elif lexema[len(lexema) - 1] == '\"': return ("Está faltando abertura de \"", 0, 0)
        
        # Constantes Literais - Caractere
        elif lexema[0] == '\'': # Lexema começa com apostrofo...
            # ... e termina com apostrofo, é uma constante literal, retorna o token
            if lexema[len(lexema) - 1] == '\'': return Token(Tipo.LITERAL, TabSimb.getInstance().addElemento(lexema))
            
            #... mas não termina com aspas, retorna erro
            else: return ("Está faltando fechamento de \'", 1, 1)

        # Lexema não começa com aspas, mas termina com apostrofo
        elif lexema[len(lexema) - 1] == '\'': return ("Está faltando abertura de \'",0, 1)

        # Constantes Numéricas
        elif lexema[0] in Term.valores(Tipo.DIGITO) or lexema[0] in Term.valores(Tipo.SOMA):
            isInt = True
            for l in lexema:
                if l not in Term.valores(Tipo.DIGITO):
                    isInt = False
                    break

            if isInt == True: return Token(Tipo.NUM_INT, TabSimb.getInstance().addElemento(lexema))
            else:
                # Se existe um único ponto no lexema e ele não é o último caractere do lexema, então pode ser um float.
                if (lexema.count('.') == 1 and 
                    lexema.find('.') < len(lexema) - 1 and
                    (lexema.count('E') <= 1 and lexema.find('E') < len(lexema)-2)):

                    isFloat = True
                    for l in lexema[1:lexema.find('.')]:
                        if l not in Term.valores(Tipo.DIGITO):
                            isFloat = False
                            break
                    
                    if isFloat == True:
                        aux = None
                        for l in lexema[lexema.find('.')+1:]:

                            # Se l não é um dígito, então verifica se está de acordo com as regras de exponenciação.
                            # Se estiver, então prossegue com o loop normalmente. Senão, então o encerra.
                            if l not in Term.valores(Tipo.DIGITO):

                                # l é um 'E', então aux reserva o 'E' e faz o loop ir para a próxima iteração
                                if l == 'E':
                                    aux = l
                                    continue
                                
                                # l é um '+' ou '-' e o antecessor era um E, então aux reserva o sinal e faz o loop ir para a próxima iteração
                                elif l in Term.valores(Tipo.SOMA) and aux == 'E':
                                    aux = l
                                    continue

                                # senão, há algo errado, seta isFlot para falso e encerra o loop
                                else:
                                    isFloat = False
                                    break
                    
                    # Lexema é uma constante númerica de ponto flutuante
                    if isFloat == True: return Token(Tipo.NUM, TabSimb.getInstance().addElemento(lexema))
                    
                    # Lexema não é uma constante númerica, retorna erro de identificador inválido
                    else: return ("Constante numérica inválidaFinal",1, 2)
                elif (lexema.find('E') != -1): return ("Constante numérica inválidaInicio",0, 2)
                else: return ("Identificador inválido",0, 3)

    
    def __fluxoTokens(self):

        print("\n::\tMódulo de Reconhecimento do Fluxo de Tokens iniciado...\n")
        time.sleep(self.__tempo * 3)

        

        for lista_lexema in self.__ent_formatada:
            linha = lista_lexema[0]
            for lexema in lista_lexema[1:]:
                token = self.__defineToken(lexema)
                
                # Se foi retornado um token, então ele é adicionado a lista de tokens.
                if isinstance(token, Token):
                    token.setLinha(linha)
                    token.setColuna(self.__codigo_fonte[linha-1].find(lexema))
                    self.__fluxo_tokens.append(token)
                


                # Senão, então ele é adicionado a lista de erros.
                elif token != None:                  
                    coluna = self.__codigo_fonte[linha-1][self.__codigo_fonte[linha-1].find(lexema)]  
                    if token[1] == 0:
                        coluna = self.__codigo_fonte[linha-1].find(lexema)
                        if coluna != -1: self.__erros_lexicos.append(ErroLexico(linha, coluna+1, token[0]))
                        
                        if token[2] == 0: # Falta aspas
                            self.__defineToken("\"{}".format(lexema))
                            
                        
                        elif token[2] == 1: # Falta apostrofo
                            self.__defineToken("\'{}".format(lexema))

                        elif token[2] == 2: # Erro de constante numérica
                            if lexema[len(lexema)-1] == 'E':
                                self.__defineToken("{}+0".format(lexema))

                        elif token[2] == 3: # Erro de identificador
                            if len(lexema) > 1:
                                i = 1;
                                while not isinstance(token, Token) and i < len(lexema) - 1:
                                    lexema = lexema[1:]
                                    token = self.__defineToken(lexema)
                                    i += 1
                            
                        
                    else:
                        coluna = self.__codigo_fonte[linha-1].find(lexema)
                        if coluna != -1: self.__erros_lexicos.append(ErroLexico(linha, coluna+len(lexema), token[0]))
                        
                        if token[2] == 0: # Falta aspas
                            if lexema[len(lexema)-1] == ';':
                                self.__defineToken("{}\"".format(lexema[:len(lexema)-1]))
                                self.__defineToken(";")
                            else: self.__defineToken("{}\"".format(lexema))
                            
                        
                        elif token[2] == 1: # Falta apostrofo
                            if lexema[len(lexema)-1] == ';':
                                self.__defineToken("{}\'".format(lexema[:len(lexema)-1]))
                                self.__defineToken(";")
                            else: self.__defineToken("{}\'".format(lexema))

                        elif token[2] == 2: # Erro de constante numérica
                            if lexema[len(lexema)-1] == 'E':
                                self.__defineToken("{}+0".format(lexema))

                        elif token[2] == 3: # Erro de identificador
                            pass
                    
                

        print("\n::",end="") 
        for i in range(3):
            time.sleep(self.__tempo/2)
            print(end=".")
        print("Módulo Encerrado")
        

    # Remove os comentários e espaços inúteis de uma entrada
    def formataEntrada(self, entrada):
    
        '''
        Método que formata a entrada lida do arquivo
        @param entrada str: entrada a ser formatada
        @return list: linha-lexemas encontrados
        '''

        print("\n\n::\tPercorrendo o código fonte...")
        time.sleep(self.__tempo * 3)

        # remove quebra presente no final de cada linha
        for i in range(len(entrada)):
            t = len(entrada[i]) - 1
            if entrada[i][t] == '\n':
                entrada[i] = entrada[i][:t]

        print(entrada)
        print()
        print()
        
        entradaFormatada = []
        comentario_multi_linha = False

        n_linha = 0 # Número da linha

        for linha in entrada:

            # Número da linha é incrementado
            n_linha += 1 

            inicio_comentario = linha.find("/*")
            final_comentario = linha.find("*/")

            # Se está sendo verificado um comentário multi linha, então:
            # Verifica se o identificador de fim de comentário (*/) existe.
            # Se existir então quer dizer que acabou o comentário.
            # Senão, não faz nada, deixa ir para a próxima linha
            if comentario_multi_linha == True:
                if final_comentario != -1:
                    comentario_multi_linha = False
                continue


            # Se existe comentário na linha
            if inicio_comentario != -1:
                
                # Entrada recebe formatação até antes do comentário
                aux = self.formataLinha(linha[:inicio_comentario])
                if len(aux) > 0:
                    entradaFormatada.append([n_linha]+aux)
                
                # Se o comentário foi iniciado, mas não foi terminado na mesma linha,
                # então é um comentário multi linha.
                if final_comentario == -1:
                    comentario_multi_linha = True
            else:
                aux = self.formataLinha(linha) 
                print("<<<<<<<<<<<<<<<<"+str(aux))
                if(len(aux) > 0):
                    entradaFormatada.append([n_linha]+aux)
        
        print("\n::... Código Fonte Percorrido.\n")
        return entradaFormatada

    # Remove os comentários e espaços inúteis de uma entrada
    def formataLinha(self, linha):
        print(linha)
        
        '''
        Método que formata uma linha removendo seus espaços.
        @param linha str: linha a ser formatada
        @return list: lexemas encontrados
        '''

        linha_formatada = []
        
        i = j = 0
        e = ''

        entreConjunto = False
        apostAberto   = False
        aspasAberto   = False
        
        while i < len(linha):
            print("::::::::"+e)
            if linha[i] == '\n':
                break

            # Se é string
            if linha[i] in ['\'','\"']:
                
                # Se é uma string completa
                if (linha[i] == '\'' and linha[i+1:].find('\'') != -1) or (linha[i] == '\"' and linha[i+1:].find('\"') != -1):

                    if linha[i] == '\'': apostAberto = not apostAberto
                    else: aspasAberto = not aspasAberto

                    

                    # Se está em um conjunto
                    if entreConjunto == True:

                        # Se percorreu apenas a virgula (divisor de parâmetros)
                        isVirgula = True
                        for ch in e:
                            if ch not in [' ', ',']: isVirgula = False

                        # Senão, se há alguma coisa além, salva esse lexema e avança para o tratamento da string
                        if isVirgula == False:
                            for k in range(j,i):
                                if linha[k] != ' ': e += linha[k]
                            if entreConjunto == True:
                                if e[len(e) - 1] == ',':
                                    linha_formatada.append(e[:len(e)-1])
                                if e[0] == ',':
                                    linha_formatada.append(e[1:])
                            else: linha_formatada.append(e)
                        e = ''
                        j = i+1
                            
                    e += linha[i]
                    end = linha[i]
                    i += 1
                    while i < len(linha) and linha[i] != end:                     
                        e += linha[i]
                        i += 1
                    if i == len(linha):
                        linha_formatada.append(e)
                        e = ''
                        break;
                    else:
                        linha_formatada.append(e+linha[i])
                        if i < len(linha) - 1 and linha[i+1] == ',':
                            i += 1                        
                        e = ''
                    j = i + 1

            # Se encontra um espaço fora de string, então preenche e salva o elemento até ali
            elif linha[i] == ' ':
                
                for k in range(j,i):
                    if linha[k] != ' ': e += linha[k]
                print(e)
                if e != '' and e != ',':
                    if entreConjunto == True:
                        if e[len(e) - 1] == ',':
                            linha_formatada.append(e[:len(e)-1])
                            linha_formatada.append(e[len(e)-1])
                        if e[0] == ',':
                            #linha_formatada.append(e[0])
                            linha_formatada.append(e[1:])
                        if e.find(',') == -1 :
                            linha_formatada.append(e)
                    else:
                            
                        linha_formatada.append(e)
                    
                    e = ''
                
                j = i+1
            
            # Senão, se encontra operador relacional
            elif (linha[i] == '>' or linha[i] == '<' or
                linha[i] == '=' or linha[i] == '!'):
                if (i < len(linha) - 1):
                    if(i == j):
                        if(linha[i+1] == '='):
                            linha_formatada.append(linha[i] + linha[i+1])
                            i += 1
                            
                        else:
                            linha_formatada.append(linha[i])
                        
                        j = i + 1
                    
                    else:
                        for k in range(j,i):
                            e += linha[k]
                        if e != '':
                            linha_formatada.append(e)
                            e = ''
                        if(linha[i+1] == '='):
                            linha_formatada.append(linha[i] + linha[i+1])
                            i += 1
                        else:
                            linha_formatada.append(linha[i])
                        
                        j = i + 1
                
                else:                
                    if(i != j):
                        for k in range(j,i):
                            e += linha[k]
                        if e != '':
                            linha_formatada.append(e)
                            e = ''
                        
                    linha_formatada.append(linha[i])
                    
                    j = i + 1



                

            # Senão, se encontra um delimitador ou operador aritmético
            elif (linha[i] in ['(', ')', '[', ']', '{', '}'] or
                linha[i] in ['+', '-', '*', '/', ';']):
                
                if(linha[i] in ['(', '[']): entreConjunto = True
                elif(linha[i] in [')', ']']): entreConjunto = False
                
                if i == j:
                    linha_formatada.append(linha[i])
                    j = i + 1
                    
                else:
                    
                    for k in range(j,i):
                        if linha[k] != ' ': e += linha[k]
                    
                    if e != '':
                        
                        # Se encontrou esponenciação
                        if(e[len(e)-1] == 'E' and i < len(linha) - 1):
                        
                            if linha[i] in ['+', '-']:
                                e += linha[i]
                                i += 1
                                while(i < len(linha) and linha[i] in Term.valores(Tipo.DIGITO)):
                                    e += linha[i]
                                    i += 1
                       
                        linha_formatada.append(e)
                        linha_formatada.append(linha[i])
                        e = ''
                    
                    j = i + 1
            

            
            i += 1
        
        #print(linha_formatada)
        return linha_formatada

    def __str__(self):
        s  = "\n\n\t\t:::::::::::: Análise Léxica ::::::::::::\n\n"
        
        s += "\t::Fluxo de Tokens:\n\n"
        for token in self.__fluxo_tokens: 
            s += "\t\t{}\n".format(token)

        s += "\n\n\t::Erros Léxicos:\n\n"
        if len(self.__erros_lexicos) == 0: 
            s += "\tNenhum erro encontrado nesta fase.\n\n"
        else: 
            for erro in self.__erros_lexicos: s += "{}\n".format(erro)
        
        return s