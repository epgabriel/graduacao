--Gabriel Edmilson P.
--Jorge Sassaki
--Stella Marques
--Trabalho Prático, Linguagens de Programação II, Versão Haskell


import Data.List
--import qualified Data.Map

-------------------------------------Funções demandadas-------------------------------------
cidades :: [(Int, Int, Float)]
cidades = [(1,2,10.0), (1,3,33.0), (1,4,78.0), (2,4,26.0), (2,5,7.0), (3,6,130.0),
		(4,3,17.0), (4,7,42.0), (5,4,67.0), (5,7,50.0), (7,6,34.0)]

perc_inicial :: ([Int],Float)
perc_inicial = ([],0)


encontrarPercurso :: [(Int, Int, Float)]->Float->([Int],Float)
encontrarPercurso percursos custoMax
	|(custo_p (calc_perc_inicial percursos)) <= custoMax = calcula_percurso perc_inicial (melhores_caminhos ( sort (lista_percursos percursos)) percursos) custoMax
	|otherwise = perc_inicial

calcula_percurso :: ([Int],Float)->[(Int, Int, Float)]->Float->([Int],Float)
calcula_percurso perCalc mc cmax
	|(custo_p (add_percurso perCalc mc)) > (custo_p perCalc) && (custo_p (add_percurso perCalc mc)) <= cmax = calcula_percurso (add_percurso perCalc mc) mc cmax  
	|otherwise = perCalc
	


-------------------------------------Funções Importantes-------------------------------------

--Encontra o percurso inicial
menor_cij_geral [c] = c
menor_cij_geral (i:j:x)
	|custo_ij i < custo_ij j = menor_cij_geral(i:x)
	|otherwise = menor_cij_geral(j:x)

--Encontra possiveis percursos partindo de uma dada cidade
percursos_regionais::Int->[(Int, Int, Float)]->[(Int, Int, Float)]
percursos_regionais a [] = []
percursos_regionais a (x:xs)
	|cid1 x < a = percursos_regionais a xs
	|cid1 x == a = x:percursos_regionais a xs
	|otherwise = []

--Lista todas as cidades iniciais do problema
lista_percursos::[(Int, Int, Float)]->[Int]
lista_percursos [] = []
lista_percursos (x:xs) = no_rept(cid1 x : lista_percursos xs)


--Otimiza a cidade (só os melhores caminhos)
melhores_caminhos::[Int]->[(Int, Int, Float)]->[(Int, Int, Float)]
melhores_caminhos [] (y:ys) = []
melhores_caminhos (x:xs) (y:ys)
	= (menor_cij_geral(percursos_regionais x (y:ys))) : (melhores_caminhos xs (y:ys))
	


-------------------------------------Aux Zone-------------------------------------
--Verifica se uma dada tupla é nula, isto é, percurso bloqueado
nulo :: (Int,Int,Float) -> Bool
nulo k	|cid1 k == 0 && cid2 k == 0 && custo_ij k == 0.0 = True
	|otherwise = False

--Retorna a tupla inicial de percurso
calc_perc_inicial :: [(Int,Int,Float)]->([Int],Float) 
calc_perc_inicial percursos = ([(cid1(menor_cij_geral percursos)),(cid2(menor_cij_geral percursos))] , (custo_ij(menor_cij_geral percursos)))


--Adiciona nova rota
add_percurso :: ([Int],Float)->[(Int,Int,Float)]->([Int],Float)
add_percurso ([],_) mc = calc_perc_inicial mc
add_percurso p mc
	|not(nulo(elemento (last(p_list p)) mc))  = ( (p_list p) ++ [cid2( elemento (last(p_list p)) mc )] , ( (custo_ij( elemento (last(p_list p)) mc )) + (custo_p p))  )
	|otherwise = p


--Retorna o elemento k na lista
elemento ::Int->[(Int,Int,Float)]->(Int,Int,Float)
elemento k [] = (0,0,0.0)
elemento k (x:xs)
	|k == cid1 x = x
	|otherwise = elemento k xs

--Retorna um dos valores da tupla da lista principal
cid1(i,_,_) = i
cid2(_,j,_) = j
custo_ij(_,_,c) = c

--Retorna o custo da tupla de percusos calculados
p_list (l,_) = l
custo_p(_,c) = c

--Remove um elemento da lista
remove k [] = []
remove k (x:xs)
	|k == x = xs
	|otherwise = x : remove k xs

--Remove todas as repeticoes de uma lista
no_rept::[Int]->[Int]
no_rept [] = []
no_rept (x:xs)
	|(notElem x xs) = sort(x : (no_rept xs))
	|otherwise = no_rept(remove x (x:xs))


