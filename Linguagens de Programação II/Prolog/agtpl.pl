%Jorge Sassaki Resende Silva
%Gabriel Edilmilson P.
%Stella Marques

%Representação dos caminhos e custos do grafo.

distancia(1,2,10).
distancia(1,3,33).
distancia(1,4,78).
distancia(2,4,26).
distancia(2,5,7).
distancia(3,6,130).
distancia(4,3,17).
distancia(4,6,10).
distancia(4,7,42).
distancia(5,4,67).
distancia(5,7,50).
distancia(7,6,34).

%Inicia em 0 e aumenta em 1 até achar a menor distância válida para o percurso.
acumula(A,B,P,S,V):-S is V,achaCaminho(A,B,P,V).
acumula(A,B,P,S,V):-V1 is V+1,acumula(A,B,P,S,V1).

%serve apenas para chamar acumula, iniciando a função com 0.
encontrarCaminhoMinimo(A,B,P,S):- achaCaminho(A,B,_,_),acumula(A,B,P,S,0),!.

%Quando possível, acha um caminho do ponto A ao B, retornando o o percurso percorrido e seu custo total .
achaCaminho(A,B,P,S) :- append([A],[B]	,P),distancia(A,B,S),!.
achaCaminho(A,B,[A|P],S1) :- distancia(A,X,C),achaCaminho(X,B,P,S),S1 is S+C.
